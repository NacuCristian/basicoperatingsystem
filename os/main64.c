#include "devices/vga.h"
#include "tools/gdt_tools.h"
#include "interrupts/init_interrupts.h"
#include "memory/memory_management_init.h"
#include "user_mode/um_management_init.h"
#include "memory/memory_management_def.h"
#include "multi_processor/multiprocessor_management.h"
#include "interrupts/interrupt_manager.h"
#include "user_mode/syscall_handle.h"

extern PVOID Gdt;
extern PVOID Cr3Base;

//  Home made Code and Data 16, 32, 64 ring 0 and 64 ring 3 flat memory descriptors
QWORD gGdtrDescArr[] = {
    // Code 16 ring 0
    0x8F9A000000FFFF,
    // Data 16 ring 0
    0x8F92000000FFFF,
    // Code 32 ring 0
    0xCF9A000000FFFF,
    // Data 32 ring 0
    0xCF92000000FFFF,
    // Code 64 ring 0
    0xAF9A000000FFFF,
    // Data 64 ring 0
    0xAF92000000FFFF,
    // Code 64 ring 3
    0xBFFA000000FFFF,
    // Data 64 ring 3
    0xBFF2000000FFFF
};

#define CODE_16_R0_SELCTOR 0x8
#define DATA_16_R0_SELCTOR 0x10
#define CODE_32_R0_SELCTOR 0x18
#define DATA_32_R0_SELCTOR 0x20
#define CODE_64_R0_SELCTOR 0x28
#define DATA_64_R0_SELCTOR 0x30
#define CODE_64_R3_SELCTOR 0x38
#define DATA_64_R3_SELCTOR 0x40

OS_STATUS
CreateNewGdtrStructure(

)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    GDT_INFO_64 *gdtr = NULL;

    status = CreateGdtStructure(
        &gdtr
    );
    if(!SUCCESS(status))
    {
        LOG("Error Creating GDT structure\n");
        return status;
    }

    status = RegisterGdtDescriptor(
        gdtr,
        gGdtrDescArr,
        8
    );
    if(!SUCCESS(status))
    {
        LOG("Error populating\n");
        return status;
    }

    gCpuInformation.SHARED_CPU_INFORMATION.CodeSegmentDescriptor64Ring0 = CODE_64_R0_SELCTOR;
    gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring0 = DATA_64_R0_SELCTOR;
    gCpuInformation.SHARED_CPU_INFORMATION.CodeSegmentDescriptor64Ring3 = CODE_64_R3_SELCTOR;
    gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3 = DATA_64_R3_SELCTOR;
    gCpuInformation.SHARED_CPU_INFORMATION.Gdt = gdtr;
    return status;
}
#define  IA32_EFER_MSR   0xC0000080

OS_STATUS 
main64(
    E820_MAP_STRUCTURE *E820Map
)
{
    OS_STATUS status = OS_STATUS_SUCCESS;

    // Initialize what we know up until this moment
    gCpuInformation.SHARED_CPU_INFORMATION.ExecutionStatus = EARLY_BOOT;
    gCpuInformation.SHARED_CPU_INFORMATION.Gdt = &Gdt;
    gCpuInformation.SHARED_CPU_INFORMATION.KernelCr3 = Cr3Base;

    status = InitMemoryManagement(E820Map);
    if(!SUCCESS(status))
    {
        LOG("Initializing memory management system failed. ABORTING\n");
        BREAKPOINT;
    }

    status = InitInterruptSystem();
    if(!SUCCESS(status))
    {
        LOG("Initializing interrupt system failed. ABORTING\n");
        BREAKPOINT;
    }
    status = CreateNewGdtrStructure();
    if(!SUCCESS(status))
    {
        LOG("CreateNewGdtrStructure system failed. ABORTING\n");
        BREAKPOINT;
    }

    status = InitUmManagement();
    if(!SUCCESS(status))
    {
        LOG("Initializing um management system failed. ABORTING\n");
        BREAKPOINT;
    }
    LOG(" \nSuccessfuly created threadpool queue in which contexts will be saved\n");
    QWORD registerNo = 0;
    RegisterHandleForInterruptVector(
        33,
        &HandleDispatcher,
        &registerNo
    );
    LOG(" \nRegistered handle for SYSCALLs\n");
    InitMultiProcessor();
    while(1==1);

    return 0;
    
}