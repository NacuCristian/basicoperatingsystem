#ifndef _OS_DEFS_H_
#define _OS_DEFS_H_


typedef unsigned char BYTE;
typedef unsigned char BOOL;
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef unsigned long long QWORD;
typedef void *PVOID;

#ifndef va_list
typedef BYTE *va_list;
#endif 

#ifndef va_start
#define va_start(list, first_arg) list = ((va_list)&first_arg + sizeof(first_arg)) 
#endif

#ifndef va_args
#define va_args(list, type) (*(type *)((((list)+=(sizeof(QWORD))) - sizeof(QWORD))))
#endif

#ifndef va_end
#define va_end(list) (list = 0)
#endif

#define KILO (1024)
#define MEGA (1024 * 1024)
#define GIGA (1024 * 1024 * 1024)
#define TERA (1024 * 1024 * 1024 * 1024)

#define PAGE_SIZE_4K (4096)
#define PAGE_MASK_4K ((-1) - (0xFFF))

#define BIT(n) (1 << n)

#define min(a, b) (a < b ? a : b)
#define max(a, b) (a > b ? a : b)
#define abs(a)  (((a) < 0) ? (-a) : (a))
#define TRUE    1
#define FALSE   0

#define NULL ((PVOID)0)

#define MAX_STRING_SIZE 1000

#define SCREEN_WIDTH  80
#define SCREEN_HEIGHT 25

#define MAKE_RING_SELECTOR(selector, ring) (selector | ring)

#define ADDRESS_NOT_CONFIGURED  ((PVOID)0xFFFFFFFFFFFFFFFF)
#define INVALID_SELECTOR 0xFFFF

#define QWORD_MASK  0xFFFFFFFFFFFFFFFF
#define DWORD_MASK  0xFFFFFFFF
#define WORD_MASK   0xFFFF
#define BYTE_MASK   0xFF

#define MAX_QWORD   0xFFFFFFFFFFFFFFFF
#define MAX_DWORD   0xFFFFFFFF
#define MAX_WORD    0xFFFF
#define MAX_BYTE    0xFF


#define BREAKPOINT while(1==1)


#endif