#include "kern_pagepool.h"
#include "../tools/standard_tools.h"
#include "../datastructures/synchronization/mutex.h"

#define PAGEPOOL_MAGIC 0x9A6E
#define QWORD_MAX 0xFFFFFFFFFFFFFFFF
#define LEAST_SIGNIFICANT_BIT(value) ((value) & 1)

MUTEX MtxPP = {0};

PAGEPOOL_HEADER *gPagingArea = NULL;

OS_STATUS
PagePoolInit(
    PAGEPOOL_HEADER *PagepoolArea,
    QWORD PagepoolSize
)
{
    PagepoolArea->Magic = PAGEPOOL_MAGIC;

    // The equations are:
    // PagePoolHeader + Bitmap + PagePool = PagePoolSize
    // Bitmap = PagePool / PAGE_SIZE_4K
    // => PagePool = ((PAGE_SIZE_4K + 1)/PAGE_SIZE_4K)*(PagepoolSize - sizeof(PAGEPOOL_HEADER)) 
    QWORD bmSize = (PagepoolSize - sizeof(PAGEPOOL_HEADER) - 2*PAGE_SIZE_4K) / PAGE_SIZE_4K;
    //This must be page aligned
    QWORD ppStart = (((QWORD)PagepoolArea + 2*PAGE_SIZE_4K + sizeof(PAGEPOOL_HEADER)) + PAGE_SIZE_4K - 1) & PAGE_MASK_4K;
    QWORD bitmapEnd = ((QWORD)PagepoolArea +sizeof(PAGEPOOL_HEADER) + bmSize);
    PagepoolArea->PagePoolStart = ppStart;
    PagepoolArea->PagePoolSize = PagepoolSize;
    PagepoolArea->BitmapEnd = bitmapEnd;
    //Clear area TODO
    return OS_STATUS_SUCCESS;
}

QWORD 
PagePoolSearch(
    PAGEPOOL_HEADER *PagepoolStart,
    QWORD Size
)
{
    // Again bug, we have a SIZE but nobody gives a shit
    QWORD bitmap = ((QWORD)PagepoolStart + sizeof(PAGEPOOL_HEADER));  
    QWORD offset = 0;
    for(bitmap; bitmap < PagepoolStart->BitmapEnd; bitmap++,offset+=64)
    {
        if(*(QWORD *)bitmap < QWORD_MAX)
        {
            QWORD value = *(QWORD *)bitmap;
            while(LEAST_SIGNIFICANT_BIT(value))
            {
                value >>= 1;
                offset++;
            }
            return offset;
        }
    }
    return QWORD_MAX;
}

PVOID
PagePoolAlloc(
    PAGEPOOL_HEADER *PagepoolStart,
    QWORD Size
)
{
    AcquireSpinMutex(&MtxPP);
    QWORD offset;
    QWORD * bitmap;
    PAGEPOOL_HEADER *ppstart;

    // Validity checks
    if(PagepoolStart == NULL)
    {
        if(gPagingArea == NULL)
        {
            ReleaseSpinMutex(&MtxPP);
            return NULL;
        }
        ppstart = gPagingArea;
    }
    else
    {
        if(PagepoolStart->Magic!=PAGEPOOL_MAGIC)
        {
            ReleaseSpinMutex(&MtxPP);
            return NULL;
        }
        ppstart = PagepoolStart;
    }
    
    
    bitmap = (QWORD *)((QWORD)ppstart + sizeof(PAGEPOOL_HEADER));
    
    offset = PagePoolSearch(
        ppstart,
        Size
    );

    // If there has been an error
    if(offset == QWORD_MAX)
    {
        LOG("Not found\n");
        ReleaseSpinMutex(&MtxPP);
        return NULL;
    }

    // Mark page as being in use
    // Here might be a bug, only 1 page is marked in use whilst the method accepts a SIZE so caca
    bitmap[(offset/64)] |= ((QWORD)BIT((offset % 64)));
    ReleaseSpinMutex(&MtxPP);
    return (PVOID)(((QWORD)ppstart->PagePoolStart) + offset * PAGE_SIZE_4K);
}

OS_STATUS
PagePoolFree(
    PAGEPOOL_HEADER *PagepoolArea,
    PVOID *PageAddress
)
{
    AcquireSpinMutex(&MtxPP);
    if(PagepoolArea->Magic!=PAGEPOOL_MAGIC)
    {
        ReleaseSpinMutex(&MtxPP);
        return OS_STATUS_MAGIC_NOT_CORRESPONDING;
    }

    if(PageAddress < ((PVOID *)PagepoolArea->PagePoolStart))
    {
        ReleaseSpinMutex(&MtxPP);
        return OS_STATUS_INVALID_PARAMETER_1;
    }

    QWORD * bitmap = (QWORD *)((QWORD)PagepoolArea + sizeof(PAGEPOOL_HEADER));
    QWORD offset;
    
    offset = ((QWORD)PageAddress - PagepoolArea->PagePoolStart)/PAGE_SIZE_4K;
    bitmap[(offset/64)] &= (~((QWORD)BIT((offset % 64))));
    KernFill(0, PageAddress, PAGE_SIZE_4K);
    ReleaseSpinMutex(&MtxPP);
    return OS_STATUS_SUCCESS;
}

OS_STATUS
SetPagingArea(
    PAGEPOOL_HEADER *PagepoolArea 
)
{
    gPagingArea = PagepoolArea;
}

OS_STATUS
PagePoolAllocMappingPage(
    PVOID * MemoryPageAddress
)
{
    *MemoryPageAddress = PagePoolAlloc(
        gPagingArea,
        1
    );
    if(MemoryPageAddress == NULL)
    {
        LOG("PagePoolAlloc Failed\n");
        return OS_STATUS_NULL_POINTER;
    }

    return OS_STATUS_SUCCESS;
}

OS_STATUS
PagePoolFreeMappingPage(
    PVOID MemoryPageAddress
)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    status = PagePoolFree(
        gPagingArea,
        MemoryPageAddress
    );
    if(!SUCCESS(status))
    {
        LOG("PagePoolFree Failed");
    }
    return status;
}