[bits 16]

;;  E820 map information:
;;      http://www.uruk.org/orig-grub/mem64mb.html
;;      http://www.acpi.info/DOWNLOADS/ACPIspec40a.pdf

extern main64
global CurrentPagePointer
global Cr3Base
global Gdt

KILO                equ (1024)
MEGA                equ (1024*1024)
GIGA                equ (1024*1024*1024)
TERA                equ (1024*1024*1024*1024)

%define BIT(x)  (1 << x)

CR0.PG              equ BIT(31)
CR0.PE              equ BIT(0)
CR4.PAE             equ BIT(5)
IA32_EFER_MSR.LME   equ BIT(8)


PAGE_POOL_START     equ 0x00060000

PAGE_SIZE           equ 4096
PAGE_MASK           equ (~(0xFFF))

IA32_EFER_MSR       equ 0xC0000080

; struc PML4
;     .PML4Entry  times 512  resq 1
; endstruc 

; struc PDPT
;     .PDPTEntry  times 512  resq 1
; endstruc 

; struc PD
;     .PDEntry    times 512  resq 1
; endstruc 

; struc PT
;     .PTEntry    times 512  resq 1
; endstruc 

;   Section 1:
_Init16:

;  >> Sub - section 1.1.1:
;  Retrieve E820 map in order to get memory information
;  Set up a new stack
mov     esp, 0x4900
mov     ax, 0xB800
mov     fs, ax
mov     cx, 80*25
mov     bx, 0

;  Clear es, ds registers
xor     ax, ax
mov     ds, ax
mov     es, ax

;  We will assume all entries are 24 bytes long
;  Clear ebx to point the start of the E820 map
;  Save the map at the E820MapSpace reserved space
xor     ebx, ebx
mov     edi, E820MapSpace
.newLine:
;  Create a valid ACPI 3.X entry 
mov     [edi + 20], dword 1
;  The function E820 will be called
mov     eax, 0xE820
;  Size of input buffer is of 24 bytes
mov     ecx, 24
;  'SMAP' magic
mov     edx, 0x534D4150
;  Call the BIOS in order to obtain the next line of the map
int     0x15
;  Check for errors in the execution
jc      error
;  Jump over the current entry in the buffer
add     edi, 24
;  Check if we reached the end of the E820 map
cmp     bx, 0
jnz     .newLine

;   Store the size of the E820 map
sub     di, E820MapSpace
sub     di, 24
mov     [E820MapSize], di 

;   >> Sub - section 1.1.2:
;   Load GDT
lgdt    [Gdt]

;   >> Sub - section 1.1.3:
;   Enable protection
mov     eax, cr0
or      eax, CR0.PE
mov     cr0, eax

;   >> Sub - section 1.1.4:
;   Set the segment selectors for data & code
mov     ax, 0x20
mov     ds, ax
mov     es, ax
mov     fs, ax
mov     gs, ax
mov     ss, ax

;   >> Sub - section 1.1.5:
;   Reach 32 bit mode (IA32)
jmp     0x18:_Init32


;   Section 2:
_Init32:
[bits 32]

;   >> Sub - section 2.1.1:
;   AllocatePage method

;   >> Sub - section 2.1.2:
;   PageMemoryRange method
;           and
;   MapVa method
;   >> Sub - section 2.1.2 cont:

;   Allocate the PML4 Page
call    AllocatePage
mov     [Cr3Base], eax

;   Map the first 1MB of memory identically (PA == VA)
mov     edi, [Cr3Base]
mov     ecx, 256
mov     esi, 0
mov     eax, 0
mov     edx, 0
mov     ebx, 0
call    PageMemoryRange 

;   >> Sub - section 2.1.3:
;   Enable 4 level paging

;   Move the PML4 pointer into CR3
mov     eax, [Cr3Base]
mov     cr3, eax

;   Enable PAE
mov     eax, cr4
or      eax, CR4.PAE
mov     cr4, eax

;   Enable Long Mode Enable bit in IA32_EFER msr
mov     ecx, IA32_EFER_MSR
rdmsr   
or      eax, IA32_EFER_MSR.LME
wrmsr

;   Enable Paging
mov     eax, cr0
or      eax, CR0.PG
mov     cr0, eax

;   >> Sub - section 2.1.4:
;   Set the segment selectors for data & code

mov     ax, 0x30
mov     ds, ax
mov     es, ax
mov     fs, ax
mov     gs, ax
mov     ss, ax

;   >> Sub - section 2.1.4:
;   Reach 64 bit mode (IA32e)

jmp     0x28:_Init64

;   Section 3:
_Init64:
[bits 64]

mov     rax, cr4
and     eax, (~(1<<2))
mov     cr4, rax

;  Clear the stack of any unwanted leftovers
;mov     rsp, 0x2900

;   Send E820 map data to the C code
;   We use the C x64 calling convention

mov     rcx, E820MapStructure
mov     rsp, 0x4900
call    main64
cmp     rax, 0
jne     error

cli 
hlt


mov     rdi, 0xB8000
mov     rcx, 80*25*2
mov     bx, 0
mov     rsi, 0x9000

.dostuf:
mov     [rdi + rcx], si
.next:
dec     rcx
loop .dostuf

add     si, 0x1000
and     si, 0xF000
mov     rdi, 0xB8000
mov     rcx, 80*25*2

jmp .dostuf 


[bits 32]

;;  ECX = #Pages to map
;;  EDI = CR3 address (32 bit)
;;  ESI = VA High 32b
;;  EAX = VA Low  32b
;;  EDX = PA High 32b
;;  EBX = PA Low  32b
PageMemoryRange:
push    ebp
mov     ebp, esp

.nextPage:

push    ebx
push    edx
push    eax
push    esi
push    edi

call    MapVa

add     ebx, PAGE_SIZE
adc     edx, 0

add     eax, PAGE_SIZE
adc     esi, 0

loop .nextPage
mov     esp, ebp
pop     ebp
ret


;;  Stack:
;;  CR3 Address
;;  VA High 32b
;;  VA Low  32b
;;  PA High 32b
;;  PA Low  32b
MapVa:
push    ebp
push    edi
push    esi
push    ebx
push    eax
push    ecx
push    edx

mov     ebp, esp
add     esp, 8 * 4

pop     edi     ;;EDI = CR3
pop     esi     ;;ESI:EBX = VA 
pop     ebx
mov     esp, ebp

shld    esi, ebx, 16
shl     ebx, 16

mov     ecx, 3
.nextPagingStructure:
mov     edx, esi
shr     edx, 23
cmp     edx, 0
jz      .continueAllOk
inc     byte [0xB8000]
.continueAllOk:
shl     edx, 3

cmp     [edi + edx], dword 0
jnz     .entryExists
call    .AllocateNewPage
.entryExists:
mov     edi, [edi + edx]
and     edi, PAGE_MASK

shld    esi, ebx, 9
shl     ebx, 9
dec     ecx
cmp     ecx, 0
jnz     .nextPagingStructure

mov     edx, esi
shr     edx, 23
shl     edx, 3

cmp     edx, 0



mov     esp, ebp
add     esp, 11 * 4
pop     esi
pop     ebx

and     ebx, PAGE_MASK
or      ebx, (1 << 0) | (1 << 1) | (1 << 2)
mov     [edi + edx], ebx
mov     [edi + edx + 4], esi

mov     esp, ebp
pop     edx
pop     ecx
pop     eax
pop     ebx
pop     esi
pop     edi
pop     ebp


ret

.AllocateNewPage:
call    AllocatePage
and     eax, PAGE_MASK
or      eax, (1 << 0) | (1 << 1) | (1 << 2)
mov     [edi + edx], eax
ret


;   >> Sub - section 2.1.1:
;   Create a page pool alocator

;;  Desc:   Rudimental page alocator
;;  In:     nothing
;;  Eax:    address of the newly allocated page
AllocatePage:
push    ecx
mov     eax, [CurrentPagePointer]
add     [CurrentPagePointer],dword PAGE_SIZE
mov     ecx, PAGE_SIZE
.clearPage:
mov     byte [eax + ecx], 0
loop    .clearPage
mov     [eax], byte 0
pop     ecx
ret

;;  Si = Dump start
;;  Cx = Dump size
dumpMem:

push    ax
push    di
push    cx
push    si

mov     ax, 0xB800
mov     fs, ax
mov     di, 0

.loopPoint:
cld
lodsb
mov     ah, al
and     al, 0xF0
shr     al, 4
cmp     al, 9
ja      .addLetter1
add     al, '0'
jmp     .print1
.addLetter1:
add     al, 'A'-10
.print1:
mov     [fs:di], al
mov     [fs:di+1], byte 0x1f
add     di, 2

mov     al, ah
and     al, 0x0F
cmp     al, 9
ja      .addLetter2
add     al, '0'
jmp     .print2
.addLetter2:
add     al, 'A'-10
.print2:
mov     [fs:di], al
mov     [fs:di+1], byte 0x1f
add     di, 2
cli
hlt
loop .loopPoint

pop     si
pop     cx
pop     di
pop     si
ret

error:
mov     ax, 0xB800
mov     es, ax
xor     bx, bx
mov     [es:bx],byte 'e'
mov     [es:bx + 1],byte 0x56
inc     bx
inc     bx
mov     [es:bx],byte 'r'
mov     [es:bx + 1],byte 0x56
cli
hlt

[bits 64]

;; RCX = address
dumpAddr:
push rax
push rbx
mov rax, 0

.loopPrint:
mov rbx, rcx

and bl, 0x0F
cmp bl, 9
jbe .addDigit
add bl, 'A' - 10
jmp .printDigit
.addDigit:
add bl, '0'
.printDigit:
mov [0xB8000 + rax], bl
mov [0xB8000 + rax + 1], BYTE 0x0A

shr rcx, 4
add rax, 2
cmp rax, 32
jb .loopPrint
pop rbx
pop rax
ret

dumpMem64:

push    rax
push    rdi
push    rcx
push    rsi

mov     rdi, 0xB8000

.loopPoint:
cld
lodsb
mov     ah, al
and     al, 0xF0
shr     al, 4
cmp     al, 9
ja      .addLetter1
add     al, '0'
jmp     .print1
.addLetter1:
add     al, 'A'-10
.print1:
mov     [rdi], al
mov     [rdi + 1], byte 0x0A
add     rdi, 2

mov     al, ah
and     al, 0x0F
cmp     al, 9
ja      .addLetter2
add     al, '0'
jmp     .print2
.addLetter2:
add     al, 'A'-10
.print2:
mov     [rdi], al
mov     [rdi+1], byte 0x0A
add     rdi, 2
loop .loopPoint

pop     rsi
pop     rcx
pop     rdi
pop     rax
ret


[bits 32]

Cr3Base: dq 0
CurrentPagePointer: dq PAGE_POOL_START
Gdt:
    .Limit:     dw  TableEnd - TableStart
    .Base:      dd  TableStart
TableStart:
    .Zero:      dq  0x0
    .Code16:    dq  0x8F9A000000FFFF
    .Data16:    dq  0x8F92000000FFFF
    .Code32:    dq  0xCF9A000000FFFF
    .Data32:    dq  0xCF92000000FFFF
    .Code64:    dq  0xAF9A000000FFFF
    .Data64:    dq  0xAF92000000FFFF
TableEnd:

E820MapStructure:
E820MapSize:    dd 0
E820MapSpace:   times (24 * 30) db 0 ; We will support 30 E820 Entries

[bits 64]