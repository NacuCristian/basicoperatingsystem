#ifndef _KERN_HEAP_
#define _KERN_HEAP_

#include "../utils/os_status.h"
#include "../utils/os_defs.h"
#include "../devices/vga.h"

#pragma pack(1)
typedef struct _HEAP_AREA_HEADER
{
    DWORD Magic;
    QWORD Size;
}HEAP_AREA_HEADER;
#pragma pack()

OS_STATUS
HeapInit(
    HEAP_AREA_HEADER *HeapArea,
    QWORD HeapSize
);

PVOID
HeapAlloc(
    HEAP_AREA_HEADER *HeapStart,
    QWORD Size
);

OS_STATUS
HeapSetDefault(
    HEAP_AREA_HEADER *HeapArea
);

OS_STATUS
HeapFree(
    PVOID ObjectAddress
);

#endif