#include "vga_driver.h"
#include "../tools/standard_tools.h"
#include "../devices/vga.h"
#include "../utils/os_intrinsics.h"
// #define SCREEN_WIDTH  80
// #define SCREEN_HEIGHT 25
#define SCREEN_PTR ((WORD *)0xB8000)
MUTEX gVgaWriteMutex = {0}; // Ensure the vga writing is serialized

#pragma pack(1)
typedef struct _VGA_PIXEL_DATA{
    union 
    {
        struct 
        {
            BYTE AsciiCharacter;
            struct COLOR
            {
                BIOS_COLOR Foreground : 4;
                BIOS_COLOR Background : 4;
            };
        };
        WORD Raw;
    };
} VGA_PIXEL_DATA;
#pragma pack()

#define WRITE_POSITION(Line, Column, Data) (SCREEN_PTR[(((BYTE)Line) * SCREEN_WIDTH + ((BYTE)Column))] = (Data.Raw))

BYTE newBorderUp = 1;
BYTE newBorderLeft = 1;
BYTE newBorderRight = 1;
BYTE newBorderDown = 1;
BYTE newWritingAreaWidth = 1;
BOOL newSeparateWritingArea = TRUE;
BOOL newSeparateWritingAreaWithLine = TRUE;

BYTE newBorderTexture = 219;
BYTE newBorderFgColor = LIGHT_BLUE;
BYTE newBorderBgColor = BLACK;

BYTE newWritingTexture = ' ';
BYTE newWritingFgColor = BLUE;
BYTE newWritingBgColor = BLACK;

#define TOP_LEFT_CORNER     201
#define TOP                 205
#define TOP_RIGHT_CORNER    187
#define RIGHT               186
#define BOTTOM_RIGHT_CORNER 188
#define BOTTOM              205
#define BOTTOM_LEFT_CORNER  200
#define LEFT                186
#define CONNECTOR_LEFT      204
#define CONNECTOR_RIGHT     185

#define DIRECTION_UP 1
#define DIRECTION_DOWN 2

DWORD newgScreenLineEnd = 0;

///// Cursor
DWORD newgCursorLine = 0;
DWORD newgCursorColumn = 0;

//// Writing
BYTE newWritingAreaStartLine = 0;
BYTE newWritingAreaStartColumn = 0;

BYTE newWritingAreaEndLine = 0;
BYTE newWritingAreaEndColumn = 0;

// Buffer structure
VGA_PIXEL_DATA *newgVgaBuffer;
QWORD newgVgaBufferSize;

// Buffer management
QWORD crtBufferFillIndex;

// Vga management
QWORD vgaStartPosition;
QWORD vgaEndPosition;
DWORD lineSize = 0;

QWORD nrCoulumns = 0;
QWORD nrLines = 0;

BOOL Sticky = TRUE;
BOOL VgaFull = FALSE;
QWORD crtCharPos;

void
VgaPrinBsod(
)
{
    VGA_PIXEL_DATA bsod;
    for(QWORD  i = vgaStartPosition, pos = 0; i < vgaEndPosition; i++, pos ++)
    {
        bsod = newgVgaBuffer[i];
        bsod.Background = RED;
        bsod.Foreground = WHITE;
        WRITE_POSITION(
            (newWritingAreaStartLine + ((int)pos / nrCoulumns)),
            (newWritingAreaStartColumn + (pos % nrCoulumns)), 
            bsod
        );
    }
}

void
RefreshScreen(
)
{
    for(QWORD  i = vgaStartPosition, pos = 0; i < vgaEndPosition; i++, pos ++)
    {
        WRITE_POSITION(
            (newWritingAreaStartLine + ((int)pos / nrCoulumns)),
            (newWritingAreaStartColumn + (pos % nrCoulumns)), 
            newgVgaBuffer[i]
        );
    }
}

void
VgaWrite(
    BYTE *Text,
    COLOR_VALUES Bg,
    COLOR_VALUES Fg
)
{
    while(*Text != 0)
    {
        if(crtBufferFillIndex >= newgVgaBufferSize)
        {
            crtBufferFillIndex = 0;
            vgaStartPosition = 0;
            vgaEndPosition = nrCoulumns * nrLines;
        }
        if((crtBufferFillIndex % nrCoulumns == 0) && Sticky)
        {
            if(crtBufferFillIndex > vgaEndPosition)
            {
                vgaStartPosition = crtBufferFillIndex - nrCoulumns * nrLines;
                vgaEndPosition = crtBufferFillIndex;
            }
            RefreshScreen();
        }

        if(*Text == '\n')
        {
            while(crtBufferFillIndex % nrCoulumns != 0)
            {
                 if(crtBufferFillIndex >= newgVgaBufferSize)
                {
                    crtBufferFillIndex = 0;
                    vgaStartPosition = 0;
                    vgaEndPosition = nrCoulumns * nrLines;
                }

                newgVgaBuffer[crtBufferFillIndex].AsciiCharacter = ' '; 
                newgVgaBuffer[crtBufferFillIndex].Background = Bg; 
                newgVgaBuffer[crtBufferFillIndex].Foreground = Fg;
                crtBufferFillIndex++;
            }
            *Text ++;
            continue;
        }

        newgVgaBuffer[crtBufferFillIndex].AsciiCharacter = *Text; 
        newgVgaBuffer[crtBufferFillIndex].Background = Bg; 
        newgVgaBuffer[crtBufferFillIndex].Foreground = Fg;
        
        crtBufferFillIndex++;
        *Text++;
    }

    if(Sticky)
    {
        DWORD val = 0;
        if(crtBufferFillIndex % nrCoulumns != 0)
        {
            val = (nrCoulumns - crtBufferFillIndex % nrCoulumns);
        }
        vgaStartPosition = crtBufferFillIndex - nrCoulumns * nrLines + val;
        vgaEndPosition = crtBufferFillIndex + val;
    }
    
    RefreshScreen();

}


void
KeyboardWriteCharacter(
    BYTE Value,
    COLOR_VALUES Bg,
    COLOR_VALUES Fg
)
{
    VGA_PIXEL_DATA data;
    if(crtCharPos == newWritingAreaEndColumn)
    {
        return;
    }
    data.AsciiCharacter = Value;
    data.Background = Bg,
    data.Foreground = Fg;
    WRITE_POSITION((newWritingAreaEndLine + 2), crtCharPos, data);
    crtCharPos++;
}


void
KeyboardDeleteCharacter(
    COLOR_VALUES Bg,
    COLOR_VALUES Fg
)
{
    VGA_PIXEL_DATA data;
    if(crtCharPos == newWritingAreaStartLine)
    {
        return;
    }
    data.AsciiCharacter = ' ';
    data.Background = Bg,
    data.Foreground = Fg;
    WRITE_POSITION((newWritingAreaEndLine + 2), crtCharPos, data);
    crtCharPos--;
}

void
KeyboardReset(
    COLOR_VALUES Bg,
    COLOR_VALUES Fg
)
{
    VGA_PIXEL_DATA data;
    if(crtCharPos == newWritingAreaStartLine)
    {
        return;
    }
    while(crtCharPos != newWritingAreaStartLine - 1)
    {
        data.AsciiCharacter = ' ';
        data.Background = Bg,
        data.Foreground = Fg;
        WRITE_POSITION((newWritingAreaEndLine + 2), crtCharPos, data);
        crtCharPos--;
    }
    crtCharPos++;
}


void 
VgaMoveWindow(
    BYTE Direction
)
{
    if(Direction == DIRECTION_UP)
    {
        if(vgaStartPosition >= nrCoulumns)
        {
            Sticky = FALSE;
            vgaStartPosition -= nrCoulumns;
            vgaEndPosition -= nrCoulumns;
            RefreshScreen();
        }
    }
    else
    {
        QWORD aux = 0; 
        if(vgaEndPosition <= crtBufferFillIndex)
        {
            if(vgaEndPosition == crtBufferFillIndex)
            {
                Sticky = TRUE;
            }
            else
            {
                vgaStartPosition += nrCoulumns;
                vgaEndPosition += nrCoulumns;
            }
            RefreshScreen();
        }
    }
    
}

void
DrawBorders(
)
{   
    newgScreenLineEnd = SCREEN_HEIGHT - newBorderRight - 1;
    
    //
    newWritingAreaStartLine = newBorderLeft;
    newWritingAreaStartColumn = newBorderUp;
    
    newWritingAreaEndLine = SCREEN_HEIGHT - newWritingAreaWidth - newBorderDown - 1;
    newWritingAreaEndColumn = SCREEN_WIDTH - newBorderDown - 1;

    nrCoulumns =  newWritingAreaEndColumn - newWritingAreaStartColumn;
    nrLines = newWritingAreaEndLine - newWritingAreaStartLine;
    //

    VGA_PIXEL_DATA border;
    VGA_PIXEL_DATA wriringarea;

    border.AsciiCharacter = newBorderTexture;
    border.Background = newBorderBgColor;
    border.Foreground = newBorderFgColor;
    
    wriringarea.AsciiCharacter = newWritingTexture;
    wriringarea.Background = newWritingBgColor;
    wriringarea.Foreground = newWritingFgColor;


    for(BYTE Line = 0; Line < SCREEN_HEIGHT; Line++)
        for(BYTE Column = 0; Column < SCREEN_WIDTH; Column++)
        {
            if (newSeparateWritingAreaWithLine && Line == newWritingAreaEndLine && Column < newWritingAreaStartColumn)
            {
                border.AsciiCharacter = CONNECTOR_LEFT;
                WRITE_POSITION(Line, Column, border);
            }
            else if (newSeparateWritingAreaWithLine && Line == newWritingAreaEndLine  && Column > newWritingAreaEndColumn)
            {    
               border.AsciiCharacter = CONNECTOR_RIGHT;
                WRITE_POSITION(Line, Column, border);
            }
            else if (newSeparateWritingAreaWithLine && Line == newWritingAreaEndLine)
            {
                border.AsciiCharacter = TOP;
                WRITE_POSITION(Line, Column, border);
            }
            else if (Line < newWritingAreaStartLine && Column < newWritingAreaStartColumn)
            {
                border.AsciiCharacter = TOP_LEFT_CORNER;
                WRITE_POSITION(Line, Column, border);
            }
            else if (Line < newWritingAreaStartLine && Column > newWritingAreaEndColumn)
            {
                border.AsciiCharacter = TOP_RIGHT_CORNER;
                WRITE_POSITION(Line, Column, border);
            }
            else if (Line > newgScreenLineEnd && Column < newWritingAreaStartColumn)
            {
                border.AsciiCharacter = BOTTOM_LEFT_CORNER;
                WRITE_POSITION(Line, Column, border);
            }
            else if (Line > newgScreenLineEnd && Column > newWritingAreaEndColumn)
            {
                border.AsciiCharacter = BOTTOM_RIGHT_CORNER;
                WRITE_POSITION(Line, Column, border);
            }
            else if (Line < newWritingAreaStartLine)
            {
                border.AsciiCharacter = TOP;
                WRITE_POSITION(Line, Column, border);
            }
            else if (Line > newgScreenLineEnd)
            {
                border.AsciiCharacter = BOTTOM;
                WRITE_POSITION(Line, Column, border);
            }
            else if (Column < newWritingAreaStartColumn)
            {
                border.AsciiCharacter = LEFT;
                WRITE_POSITION(Line, Column, border);
            }
            else if (Column > newWritingAreaEndColumn)
            {
                border.AsciiCharacter = RIGHT;
                WRITE_POSITION(Line, Column, border);
            }
            else
                WRITE_POSITION(Line, Column, wriringarea);
        }
    newWritingAreaEndLine--;
}

OS_STATUS
VgaInit(
    DWORD *VgaBuffer,
    QWORD Size
)
{
    VGA_PIXEL_DATA fill;

	_OutByte(0x0A, 0x3D4);
	_OutByte(0x20, 0x3D5);

    newgVgaBuffer = VgaBuffer;
    newgVgaBufferSize = Size / 2;
    DrawBorders();

    crtCharPos = newWritingAreaStartColumn;

    fill.AsciiCharacter = ' ';
    fill.Background = newWritingBgColor;
    fill.Foreground = newWritingFgColor;

    for(QWORD pos = 0; pos < newgVgaBufferSize; pos++)
    {
        newgVgaBuffer[pos] = fill;
    }    
    
    crtBufferFillIndex = 0;

    vgaStartPosition = 0;
    vgaEndPosition = nrCoulumns * nrLines;
    
    return OS_STATUS_SUCCESS;
}
