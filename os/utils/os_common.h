#ifndef _OS_COMMON_H_
#define _OS_COMMON_H_

#include "os_defs.h"
#include "os_intrinsics.h"
#include "os_status.h"
#include "../datastructures/synchronization/mutex.h"
#include "../acpi/acpi.h"

#define ACPI_MAX_INIT_TABLES 20
static ACPI_TABLE_DESC gAcpiTableArray[ACPI_MAX_INIT_TABLES] = {0};

typedef enum _OS_PROGRESS_STATUS{

    EARLY_BOOT = 0,
    DEVICE_INIT,
    MULTI_PROCESSOR_INIT,
    USERMODE_STARTED
    
} OS_PROGRESS_STATUS;

// MAKE SURE THIS HAS THE EXACT SAME STRUCUTRE AS ASM_PER_CPU_INFORMATION in iniAp.asm
typedef struct _DEVICE_DESCRIPTOR
{
    struct DEVICE_DESCRIPTOR *NextDevice;
    PVOID DeviceConfigSpaceAddress;
    BYTE Bus;
    BYTE Device;
    BYTE Function;
    BYTE DeviceClass;
    BYTE DeviceSubclass;
    BYTE DeviceVendor;
} DEVICE_DESCRIPTOR;

#pragma pack(1)
// MAKE SURE THIS HAS THE EXACT SAME STRUCUTRE AS ASM_PER_CPU_INFORMATION in iniAp.asm
typedef struct _PER_CPU_INFORMATION
{
    BYTE CpuId;
    PVOID StackAddress;
    PVOID TssAddress;
    WORD TssSelector;
    PVOID ContextSwitchSpace;
    BYTE *ApicBase;
    QWORD LapicFrequency;
    BYTE  InKernel;
    BYTE  SwitchedTask;
} PER_CPU_INFORMATION;
#pragma pack()

#pragma pack(1)
typedef struct _CPU_INFORMATION
{
    struct _SHARED_CPU_INFORMATION
    {
        // Progress status
        BYTE ExecutionStatus;

        // Pointer to a GDT_MANAGEMENT structure
        PVOID Gdt;
        
        // Segment selectors, used for switching in and out of userland:
        // Segment descriptor for Kernel
        WORD DataSegmentDescriptor64Ring0;
        WORD CodeSegmentDescriptor64Ring0;
        // Segment descriptor for Userland
        WORD DataSegmentDescriptor64Ring3;
        WORD CodeSegmentDescriptor64Ring3; 

        // Scheaduling data
        PVOID *ThreadpoolGlobaQueue; 
        MUTEX ThreadpoolGlobalMutex;

        // Multi processor data
        DWORD TotalCpuNumber;
        DWORD InitializedCpus;

        QWORD KernelSpaceStart;

        // The IDT will be shared by all processors
        PVOID Idt;

        // The kernel CR3 will be shared
        PVOID KernelCr3;
        DEVICE_DESCRIPTOR *SystemDevices;
    } SHARED_CPU_INFORMATION;
    
    PER_CPU_INFORMATION *PerCpuData;
} CPU_INFORMATION;
#pragma pack()


CPU_INFORMATION gCpuInformation;

#endif