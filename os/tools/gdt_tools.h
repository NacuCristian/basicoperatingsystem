#ifndef _GDT_TOOLS_H_
#define _GDT_TOOLS_H_

#include "../utils/os_defs.h"
#include "../utils/os_status.h"

#pragma pack(1)
typedef struct _GDT_INFO_32{
    WORD GdtLimit;
    DWORD TableStart;
}GDT_INFO_32;
#pragma pack()

#pragma pack(1)
typedef struct _GDT_INFO_64{
    WORD GdtLimit;
    QWORD TableStart;
}GDT_INFO_64;
#pragma pack()

#pragma pack(1)
    typedef struct _GDT_TABLE_64{
        QWORD Zero;
        QWORD Descriptors[1];
    }GDT_TABLE_64;
#pragma pack()

#pragma pack(1)
typedef struct _SEGMENT_DESCRIPTOR {
     union LOW_DWORD {
        struct LOW_DWORD_FIELDS{
            WORD SegmentLimit_0_15;
            WORD BaseAddress_0_15;
        };
        DWORD RawLowDword;
    };

    union HIGH_DWORD{
        struct HIGH_DWORD_FIELDS{
            DWORD BaseAddress_16_23: 8;
            DWORD Type: 4;
            DWORD S: 1;
            DWORD Dpl: 2;
            DWORD P: 1;
            DWORD SegmentLimit_16_19: 4;
            DWORD Avl: 1;
            DWORD L: 1;
            DWORD Db: 1;
            DWORD G: 1;
            DWORD BaseAddress_24_31: 8;
        };
        DWORD RawHighDword;
    };
}SEGMENT_DESCRIPTOR;
#pragma pack()

/// TSS Stuff
#pragma pack(1)
    typedef struct _TSS_DESCRIPTOR_64{

        union LOW_QWORD
        {
            SEGMENT_DESCRIPTOR FIELDS;
            QWORD RawLowQword;
        };
        
        union HIGH_QWORD
        {
            struct HIGH_QWORD_FIELDS 
            {
                DWORD BaseAddress_32_64;
                DWORD Reserved;
            };
            QWORD RawHighQword;
        };
        
    }TSS_DESCRIPTOR_64;
#pragma pack()

#pragma pack(1)
typedef struct _TASK_STATE_SEGMENT_64{
    DWORD Reserved1;
    QWORD Rsp0;
    QWORD Rsp1;
    QWORD Rsp2;
    QWORD Reserved2;
    QWORD Ist1;
    QWORD Ist2;
    QWORD Ist3;
    QWORD Ist4;
    QWORD Ist5;
    QWORD Ist6;
    QWORD Ist7;
    QWORD Reserved3;
    BYTE Reserved4;
    BYTE IOMapBaseAddress;
}TASK_STATE_SEGMENT_64;
#pragma pack()


OS_STATUS
CreateGdtStructure(
    GDT_INFO_64 **Gdtr
);

WORD
GetCurrentGdtOffset(
    GDT_INFO_64 *Gdtr
);

OS_STATUS
RegisterGdtDescriptor(
    GDT_INFO_64 *Gdtr,
    QWORD *Descriptor,
    DWORD DescriptorSizeInQwords  
);

#endif