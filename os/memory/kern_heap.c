#include "kern_heap.h"
#include "../datastructures/synchronization/mutex.h"
#include "../utils/os_common.h"
#define HEAP_MAGIC 0xD50A
#define OBJECT_MAGIC 0x7A
MUTEX MtxH = {0};

#pragma pack(1)
typedef struct _HEAP_OBJECT_HEADER
{
    BYTE Magic;
    PVOID *NextObject; 
    BOOL Free;
}HEAP_OBJECT_HEADER;
#pragma pack()

HEAP_AREA_HEADER *DefaultHeap = NULL;

OS_STATUS
HeapInit(
    HEAP_AREA_HEADER *HeapArea,
    QWORD HeapSize
)
{
    // Identify with a valid heap
    HeapArea->Magic = HEAP_MAGIC;
    // The last address of the heap. Used as stop condition for search.
    HeapArea->Size = (QWORD)HeapArea + HeapSize;
    // Creat one entry that will contain all heap memory and will later be fragmented
    ((HEAP_OBJECT_HEADER *)(((QWORD)HeapArea)+sizeof(HEAP_AREA_HEADER)))->Free = TRUE;
    ((HEAP_OBJECT_HEADER *)(((QWORD)HeapArea)+sizeof(HEAP_AREA_HEADER)))->NextObject = (PVOID)(((QWORD)HeapArea) + HeapSize);
}

OS_STATUS
HeapSetDefault(
    HEAP_AREA_HEADER *HeapArea
)
{
    DefaultHeap = HeapArea;
    return OS_STATUS_SUCCESS;
}

HEAP_OBJECT_HEADER *
HeapSearch(
    HEAP_AREA_HEADER *HeapStart,
    QWORD Size
)
{
    PVOID heapMaxAddress = (PVOID)HeapStart->Size; 
    HEAP_OBJECT_HEADER *objectIterator = (HEAP_OBJECT_HEADER *)((QWORD)HeapStart + sizeof(HEAP_AREA_HEADER));
    while((PVOID)objectIterator < heapMaxAddress)
    {
        if(objectIterator->Free == TRUE &&
            ((((QWORD)objectIterator->NextObject) - ((QWORD)objectIterator))) >= (Size + sizeof(HEAP_OBJECT_HEADER)))
        {
            return objectIterator;
        }
        if(((HEAP_OBJECT_HEADER *)objectIterator->NextObject)->Free == TRUE &&
            objectIterator->Free)
        {
            objectIterator->NextObject = ((HEAP_OBJECT_HEADER *)objectIterator->NextObject)->NextObject;
        }
        else{
            objectIterator = (HEAP_OBJECT_HEADER *)objectIterator->NextObject;
        }
    }
    return NULL;
}

PVOID
HeapAlloc(
    HEAP_AREA_HEADER *HeapStart,
    QWORD Size
)
{
    AcquireSpinMutex(&MtxH);
    QWORD curentObjectSize;
    QWORD secondSplit;
    HEAP_OBJECT_HEADER *insertPosition;
    HEAP_AREA_HEADER *heap;

    if (HeapStart == NULL)
    {
        if (DefaultHeap == NULL)
        {
            LOG("Error finding Heap Area\n");
            ReleaseSpinMutex(&MtxH);
            return NULL;
        }
        heap = DefaultHeap;
    }
    else
    {
        heap = HeapStart;
    }

    // If this is not an initialized heap area, error
    if(heap->Magic!=HEAP_MAGIC)
    {
        ReleaseSpinMutex(&MtxH);
        return NULL;        
    }

    insertPosition = HeapSearch(heap, Size);
    
    // If there's no suitable area for the new object return error
    if(insertPosition == NULL)
    {
        ReleaseSpinMutex(&MtxH);
        return NULL;
    }

    curentObjectSize = (QWORD)insertPosition->NextObject - (QWORD)insertPosition;
    // If we can split the curent object into two objects of smaller size, do it
    if( curentObjectSize > Size + 2 * sizeof(HEAP_OBJECT_HEADER))
    {
        secondSplit = ((QWORD)insertPosition) + Size + sizeof(HEAP_OBJECT_HEADER);

        ((HEAP_OBJECT_HEADER *)secondSplit)->Free = insertPosition->Free;
        ((HEAP_OBJECT_HEADER *)secondSplit)->NextObject = insertPosition->NextObject;
    
        insertPosition->Free = FALSE;
        insertPosition->NextObject = (PVOID)secondSplit;
    }
    // Else just use the whole object
    else
    {
        insertPosition->Free = FALSE;
    }

    insertPosition->Magic = OBJECT_MAGIC;
    ReleaseSpinMutex(&MtxH);
    return (PVOID)(((QWORD)insertPosition)+sizeof(HEAP_OBJECT_HEADER));
}

OS_STATUS
HeapFree(
    PVOID ObjectAddress
)
{
    AcquireSpinMutex(&MtxH);
    HEAP_OBJECT_HEADER *object = (HEAP_OBJECT_HEADER *)(((QWORD)ObjectAddress) - sizeof(HEAP_OBJECT_HEADER));
    HEAP_OBJECT_HEADER *nextObject = ((HEAP_OBJECT_HEADER *)object->NextObject);
    if(object->Magic != OBJECT_MAGIC)
    {
        LOG("ERR/FREE ADDRESS %llX\n", object);
        ReleaseSpinMutex(&MtxH);
        return OS_STATUS_MAGIC_NOT_CORRESPONDING;
    }
    if(nextObject->Magic == OBJECT_MAGIC && nextObject->Free == TRUE)
    {
        object->NextObject = nextObject->NextObject;
    }

    object->Free = TRUE;
    ReleaseSpinMutex(&MtxH); 
    return OS_STATUS_SUCCESS;
}


