[bits 64]
extern HandleSyscall 
global _SyscallEntryPoint

_SyscallEntryPoint:
    ; push    rcx
    ; push    rdx
    ; ;call    HandleSyscall

    mov     ax, 0x43
    mov     ds, ax
    mov     es, ax
    mov     fs, ax
    mov     gs, ax
    sysexit
    ;db      0b01001000, 0x0f, 0x35 ;; SYSEXIT FFS
