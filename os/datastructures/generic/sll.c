#include "sll.h"
#include  "../../memory/kern_heap.h"

typedef struct _SLL_NOT_SYNCHRONIZED{
    // has to be the first filed, do not change
    // it's the interface exposed to the user
    SLL_HEADER Header;
    CompareElements Comparator;
    GENERIC_SINGLE_LINKED_NODE *Tail;
} SLL_NOT_SYNCHRONIZED;

typedef struct _SLL_SYNCHRONIZED{
    SLL_NOT_SYNCHRONIZED Base;
    
    // Mutex for the list, could be done lockless but i'm too lazy
    MUTEX ListMutex;
} SLL_SYNCHRONIZED;


OS_STATUS
SllInit(
    LIST_TYPE SllType,
    CompareElements Comparator,
    SLL_HEADER **SllHead
)
{
    SLL_HEADER *sllShadow;
    SLL_NOT_SYNCHRONIZED *sllNotSync;
    OS_STATUS status;

    // Allocate Sll
    if (SllType == LIST_SLL_SYNCHRONIZED)
        sllShadow = HeapAlloc(NULL, sizeof(SLL_SYNCHRONIZED));
    else if (SllType == LIST_SLL_NOT_SYNCHRONIZED)
        sllShadow = HeapAlloc(NULL, sizeof(SLL_NOT_SYNCHRONIZED));
    else
        return OS_STATUS_INVALID_PARAMETER_1;
    if(sllShadow == NULL)
    {
        LOG("Error allocating SLL Head\n");
        return OS_STATUS_NULL_POINTER;
    }

    // Initialize Sll
    sllShadow->Element = NULL;
    sllShadow->ListType = SllType;

    sllNotSync = sllShadow;
    sllNotSync->Comparator = Comparator;
    sllNotSync->Tail = NULL;

    if (SllType == LIST_SLL_SYNCHRONIZED)
    {
        SLL_SYNCHRONIZED *sllSync = sllShadow;
        InitSpinMutex(&sllSync->ListMutex);
    }

    *SllHead = sllShadow;
    return OS_STATUS_SUCCESS;
}

BOOL
SllIsEmpty(
    SLL_HEADER *SllHead
)
{
    return (SllHead->Element == NULL);
}

OS_STATUS
SllSearchElement(
    SLL_HEADER *SllHead,
    PVOID SearchedData,
    GENERIC_SINGLE_LINKED_NODE **PreviousNode
)
{
    CompareElements compareFunction;
    GENERIC_SINGLE_LINKED_NODE *currentElement;
    
    compareFunction = ((SLL_NOT_SYNCHRONIZED *)SllHead)->Comparator;

    if(compareFunction == NULL)
    {
        LOG("Error no comparing method\n");
        return OS_STATUS_NULL_POINTER;
    }
    
    if(SllIsEmpty(SllHead))
    {
        if(PreviousNode != NULL)
            *PreviousNode = NULL;
        return OS_STATUS_NOT_FOUND;
    }

    currentElement = SllHead;
    while(currentElement->Next != NULL)
    {
        if(compareFunction(currentElement->Next->Data, SearchedData) 
                == COMPARE_FUNCTION_STATUS_EQUAL)
        {
            if(PreviousNode != NULL)
                *PreviousNode = currentElement;
            return OS_STATUS_SUCCESS;
        }
        currentElement = currentElement->Next;
    }

    // The case for the last element in the list
    if(compareFunction(currentElement->Data, SearchedData) 
                == COMPARE_FUNCTION_STATUS_EQUAL)
    {
        if(PreviousNode != NULL)
            *PreviousNode = currentElement;
        return OS_STATUS_SUCCESS;
    }

    // No such element exists
    if(PreviousNode != NULL)
        *PreviousNode = NULL;
    return OS_STATUS_NOT_FOUND;
}

OS_STATUS
SllInsertElementFront(
    SLL_HEADER *SllHead,
    PVOID *Element
)
{
    GENERIC_SINGLE_LINKED_NODE *newNode;
    MUTEX *mtx = NULL;

    if(SllHead->ListType == LIST_SLL_SYNCHRONIZED)
    {
        mtx = &((SLL_SYNCHRONIZED *)SllHead)->ListMutex;
    }

    newNode = HeapAlloc(NULL, sizeof(GENERIC_SINGLE_LINKED_NODE));
    if(newNode == NULL)
    {
        LOG("HeapAlloc Failed\n");
        return OS_STATUS_NULL_POINTER;
    }

    newNode->Data = Element;
    if(mtx)
    {
        AcquireSpinMutex(mtx);
    }

    newNode->Next = SllHead->Element;
    SllHead->Element = newNode;
    if(((SLL_NOT_SYNCHRONIZED *)SllHead)->Tail == NULL)
    {
        ((SLL_NOT_SYNCHRONIZED *)SllHead)->Tail = newNode;
    }

    if(mtx)
    {
        ReleaseSpinMutex(mtx);
    }

    return OS_STATUS_SUCCESS;
}

OS_STATUS
SllInsertElementEnd(
    SLL_HEADER *SllHead,
    PVOID *Element
)
{
    GENERIC_SINGLE_LINKED_NODE *newNode;
    MUTEX *mtx = NULL;

    if(SllHead->ListType == LIST_SLL_SYNCHRONIZED)
    {
        mtx = &((SLL_SYNCHRONIZED *)SllHead)->ListMutex;
    }

    newNode = HeapAlloc(NULL, sizeof(GENERIC_SINGLE_LINKED_NODE));
    if(newNode == NULL)
    {
        LOG("HeapAlloc Failed\n");
        return OS_STATUS_NULL_POINTER;
    }

    newNode->Data = Element;
    newNode->Next = NULL;

    if(mtx)
    {
        AcquireSpinMutex(mtx);
    }
        if(((SLL_NOT_SYNCHRONIZED *)SllHead)->Tail != NULL)
        {
            ((SLL_NOT_SYNCHRONIZED *)SllHead)->Tail->Next = newNode;
        }
        ((SLL_NOT_SYNCHRONIZED *)SllHead)->Tail = newNode;
        if(SllHead->Element == NULL)
        {
            SllHead->Element = newNode;
        }
    if(mtx)
    {
        ReleaseSpinMutex(mtx);
    }
    return OS_STATUS_SUCCESS;
}

PVOID
SllDeleteElement(
    SLL_HEADER *SllHead,
    PVOID *Element
)
{
    GENERIC_SINGLE_LINKED_NODE *node;
    GENERIC_SINGLE_LINKED_NODE *oldNode;
    OS_STATUS status;
    MUTEX *mtx = NULL;
    PVOID returnValue = NULL;

    if(SllHead->ListType == LIST_SLL_SYNCHRONIZED)
    {
        mtx = &((SLL_SYNCHRONIZED *)SllHead)->ListMutex;
    }
    status = SllSearchElement(
        SllHead,
        Element,
        &node
    );
    if(!SUCCESS(status))
    {
        LOG("Element was not found\n");
        // Maybe there could be a warning here?
        return OS_STATUS_SUCCESS;
    }

    oldNode = node->Next;
    returnValue = oldNode->Data;

    if(mtx)
    {
        AcquireSpinMutex(mtx);
    }
        // node is the previous node of the one thaat needs to be deleted
        node->Next = node->Next->Next;
        if(node->Next->Next == NULL)
        {
            if(node == SllHead)
            {
                ((SLL_NOT_SYNCHRONIZED *)SllHead)->Tail = NULL;
            }
            else
            {
                ((SLL_NOT_SYNCHRONIZED *)SllHead)->Tail = node;
            }
        }

    if(mtx)
    {
        ReleaseSpinMutex(mtx);
    }

    // Memory management, don't overflow the heap
    HeapFree(oldNode);

    return returnValue;

}

PVOID
SllDeleteFirst(
    SLL_HEADER *SllHead
)
{
    GENERIC_SINGLE_LINKED_NODE *oldNode;
    OS_STATUS status;
    MUTEX *mtx = NULL;
    PVOID returnValue = NULL;

    if(SllHead->ListType == LIST_SLL_SYNCHRONIZED)
    {
        mtx = &((SLL_SYNCHRONIZED *)SllHead)->ListMutex;
    }

    if(SllIsEmpty(SllHead))
    {
        return OS_STATUS_EMPTY;
    }

    oldNode = SllHead->Element;
    returnValue = oldNode->Data;
    if(mtx)
    {
        AcquireSpinMutex(mtx);
    }
        // node is the previous node of the one thaat needs to be deleted
        SllHead->Element = SllHead->Element->Next;
        if(SllHead->Element == NULL)
            ((SLL_NOT_SYNCHRONIZED *)SllHead)->Tail = NULL;
    if(mtx)
    {
        ReleaseSpinMutex(mtx);
    }

    // Memory management, don't overflow the heap
    HeapFree(oldNode);

    return returnValue;

}

OS_STATUS
SllDestroy(
    SLL_HEADER *SllHead
)
{
    if(SllIsEmpty(SllHead))
    {
        HeapFree(SllHead);
        return OS_STATUS_SUCCESS;
    }
    return OS_STATUS_NOT_EMPTY;
}
