#include "syscall_handle.h"
#include "syscall_defs.h"
#include "../driver/keyboard.h"
#include "../devices/lapic.h"
#include "../devices/pic.h"
#include "task_loader.h"
#include "task_scheaduler.h"
#include "../memory/kern_pagepool.h"
#include "../memory/kern_heap.h"
typedef OS_STATUS (*SyscallHandler)(PVOID *Args);

OS_STATUS
HandleReadReq(PVOID *Args);

OS_STATUS
HandleWriteReq(PVOID *Args);

OS_STATUS
HandleForkReq(INTERRUPT_SERVICE_ROUTINE_DATA *Data);

OS_STATUS
HandleGetPid(INTERRUPT_SERVICE_ROUTINE_DATA *Data);

OS_STATUS
HandleExit(INTERRUPT_SERVICE_ROUTINE_DATA *Data);

SyscallHandler Handlers[MAX_REQ] = {
    HandleReadReq,
    HandleWriteReq,
    HandleForkReq,
    HandleGetPid,
    HandleExit
};

OS_STATUS
HandleDispatcher(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
)
{
    QWORD Command = 0;
    PVOID Args = NULL;
    DWORD intTime = ReadApicTimer();
    StopApicTimer();
    
    
    Command = Data->Context->COMMON_REGS.Rcx;
    Args = Data->Context->COMMON_REGS.Rdx;

    if(Command < MAX_REQ)
    {
        if(Command == FORK_REQ || Command == GETPID_REQ || Command == EXIT_REQ)
        {
            Handlers[Command](Data);
        }
        else
        {
            Handlers[Command](Args);
        }
    }

    SetApicTimer(intTime, ONE_SHOT, 0x46, 1, NOT_MASKED, TRUE);
    return OS_STATUS_SUCCESS;
}

OS_STATUS
HandleReadReq(PVOID *Args){
    
    OS_STATUS status = OS_STATUS_SUCCESS;
    KERNEL_BUFFER_COMMAND *readRequestBuffer;
    volatile BYTE *signal;
    
    if(Args == NULL)
        return OS_STATUS_NULL_POINTER;
    
    readRequestBuffer = Args;
    LOG("Read request data\n");
    status = KeyboardRegisterBuffer
    (
        readRequestBuffer->Buffer,
        readRequestBuffer->Count - 1,
        &signal
    );
    SendEoiForLapic();
    while(*signal == 0)
    {
        _CpuPause();
    }
    
    readRequestBuffer->Buffer[readRequestBuffer->Count - 1] = 0;
    return OS_STATUS_SUCCESS;
}

OS_STATUS
HandleWriteReq(PVOID *Args){
    
    OS_STATUS status = OS_STATUS_SUCCESS;
    KERNEL_BUFFER_COMMAND *writeRequestBuffer;
    
    if(Args == NULL)
        return OS_STATUS_NULL_POINTER;
    
    writeRequestBuffer = Args;
    PRINT_FROM_UM("%s\n", writeRequestBuffer->Buffer);
    return OS_STATUS_SUCCESS;

}

OS_STATUS
HandleForkReq(INTERRUPT_SERVICE_ROUTINE_DATA *Data){

    OS_STATUS status = OS_STATUS_SUCCESS;
    PVOID stack = PagePoolAlloc(NULL, PAGE_SIZE_4K);
    
    QWORD newPid = 0;
    CloneTask(
        Data->Context,
        stack,
        gCpuInformation.SHARED_CPU_INFORMATION.KernelCr3,
        &newPid
    );

    Data->Context->COMMON_REGS.Rax = newPid;

    return OS_STATUS_SUCCESS;
}

OS_STATUS
HandleGetPid(INTERRUPT_SERVICE_ROUTINE_DATA *Data)
{
    Data->Context->COMMON_REGS.Rax = *(QWORD *)(((Data->Context->Rsp & PAGE_MASK_4K) + PAGE_SIZE_4K) - sizeof(QWORD));
}

OS_STATUS
HandleExit(INTERRUPT_SERVICE_ROUTINE_DATA *Data)
{
    return KernelMainCycle(Data->Context);
}