#include "um_management_init.h"
#include "syscall.h"
#include "task_scheaduler.h"

OS_STATUS
InitUmManagement()
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    
    InitThreadpool();
    if(!SUCCESS(status))
    {
        LOG("Initializing Threadpool Failed!\n");
        return status;
    }

    return status;

}