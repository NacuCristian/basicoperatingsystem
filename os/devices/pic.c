#include "pic.h"
#include "vga.h"
// Init configuration sequence
// ICW1 signals the reset phase
// ICW2 sets the base interrupt vector
// ICW3 
// ICW4 Bit 0 must be set in order to indicate the controoler are operating in INTEL AChitecture

//  PCI1 and PCI2 I/O Ports Base
#define PIC1            0x20
#define PIC2            0xA0

//  PCI1 and PCI2 Control Registers
#define PIC1_COMMAND    0x20
#define PIC1_DATA       0x21
#define PIC2_COMMAND    0xA0
#define PIC2_DATA       0xA1

//  Eoi value
#define PIC_EOI         0x20

//  Configuration values
#define ICW3_MASK 0x7
#define ICW1_INIT	                0x10	    // Initiate configuration
#define ICW1_ICW4	                0x01        // Determines the configuration will be MASTER SLAVE next ICW MASTER SLAVE next ICW
#define ICW3_MASTER_IRQ2            0x4         // Communicate with slave PIC via IRQ2
#define ICW3_SLAVE_CASCADE          0x2         // The master will cascade-communicate via IRQ2
#define ICW4_INTEL_ARCHITECTURE	    0x01		// Bit 0 must be set in order to indicate the controoler are operating in INTEL AChitecture

BYTE    gRemappedBaseForPic1;
BYTE    gRemappedBaseForPic2;

OS_STATUS
SendEoiForPic(
    BYTE Irq
)
{
    BOOL validIrq = FALSE;
    if (Irq < 8)
    {
        _OutByte(PIC_EOI, PIC1_COMMAND);
        validIrq = TRUE;
    }
    if (Irq >= 8)
    {
        _OutByte(PIC_EOI, PIC1_COMMAND);
        _OutByte(PIC_EOI, PIC2_COMMAND);
        validIrq = TRUE;
    }

    if(!validIrq)
    {
        return OS_STATUS_INVALID_VALUE;
    }

    return OS_STATUS_SUCCESS;
}

OS_STATUS
InitPic(
    DWORD OffsetInt_0_7,
    DWORD OffsetInt_8_15
)
{
    BYTE pic1InterruptMask;
    BYTE pic2InterruptMask;
    
    if(OffsetInt_0_7 + 7 >= OffsetInt_8_15)
    {
        LOG("[WARNING] Overlapping IRQs\n");
    }

    gRemappedBaseForPic1 = OffsetInt_0_7;
    gRemappedBaseForPic2 = OffsetInt_8_15;

    pic1InterruptMask = _InByte(PIC1_DATA);
    pic2InterruptMask = _InByte(PIC2_DATA);

    // Send ICW1 in COMMAND register
    _OutByte((ICW1_INIT | ICW1_ICW4), PIC1_COMMAND);
    _IoWait();
    _OutByte((ICW1_INIT | ICW1_ICW4), PIC2_COMMAND);
    _IoWait();

    // Send ICW2 in DATA register
    LOG("Offset 0_7 at : %X Offset 8_15 at : %X\n",OffsetInt_0_7,OffsetInt_8_15);
    _OutByte(OffsetInt_0_7, PIC1_DATA);
    _IoWait();
    _OutByte(OffsetInt_8_15, PIC2_DATA);
    _IoWait();

    // Send ICW3 in DATA register
    _OutByte(ICW3_MASTER_IRQ2, PIC1_DATA);
    _IoWait();
    _OutByte(ICW3_SLAVE_CASCADE, PIC2_DATA);
    _IoWait();

    // Send ICW4 in DATA register
    _OutByte(ICW4_INTEL_ARCHITECTURE, PIC1_DATA);
    _IoWait();
    _OutByte(ICW4_INTEL_ARCHITECTURE, PIC2_DATA);
    _IoWait();

    // Reload masks
    _OutByte(pic1InterruptMask, PIC1_DATA);
    _OutByte(pic2InterruptMask, PIC2_DATA);
    return OS_STATUS_SUCCESS;
}

OS_STATUS
GetInterruptVectorFromIrq(
    BYTE Irq,
    BYTE *InterruptVector
)
{
    if(Irq > 15)
    {
        return OS_STATUS_INVALID_PARAMETER_1;
    }

    // Irq is sent from the slave PIC 
    if(Irq > 7)
    {
        *InterruptVector = (Irq - 8) + gRemappedBaseForPic2;
    }
    else
    {
        *InterruptVector = Irq + gRemappedBaseForPic1;
    }

    return OS_STATUS_SUCCESS;
}

OS_STATUS
GetIrqFromInterruptVector(
    BYTE InterruptVector,
    BYTE *Irq
)
{
    // Irq is sent from the slave PIC 
    if(InterruptVector >= gRemappedBaseForPic1 && 
        InterruptVector < gRemappedBaseForPic1 + 8)
    {
        if(Irq != NULL)
            *Irq = InterruptVector - gRemappedBaseForPic1;
    }
    else if(InterruptVector >= gRemappedBaseForPic2 && 
        InterruptVector < gRemappedBaseForPic2 + 8
    )
    {
        if(Irq != NULL)
            *Irq = InterruptVector - gRemappedBaseForPic2 + 8;
    }
    else 
    {
        if(Irq != NULL)
            *Irq = INVALID_IRQ;
        return OS_STATUS_INVALID_VALUE;
    }
    return OS_STATUS_SUCCESS;

}