#include "syscall.h"
#include "../utils/os_intrinsics.h"

#define IA32_EFER_MSR       0xC0000080
#define IA32_LSTAR_MSR      0xC0000082 // RIP
#define IA32_STAR_MSR       0xC0000081 // CS[47:32]
#define IA32_FMASK_MSR      0xC0000084 // RFLAGS MASK

#define IA32_SYSENTER_CS    0x174
#define IA32_SYSENTER_EIP   0x176
#define IA32_SYSENTER_ESP   0x175

#define IA32_EFER_SCE       BIT(0)

extern void _SyscallEntryPoint();

OS_STATUS
InitSyscall(
    QWORD SyscallRsp
)
{

    // Enable System Call Extension
    _WriteMsr(IA32_EFER_MSR, _ReadMsr(IA32_EFER_MSR) | IA32_EFER_SCE);

    _WriteMsr(IA32_LSTAR_MSR, ((QWORD)_SyscallEntryPoint));

    _WriteMsr(IA32_STAR_MSR, (0 | (((QWORD)gCpuInformation.SHARED_CPU_INFORMATION.CodeSegmentDescriptor64Ring0) << 32)
                                | (((QWORD)gCpuInformation.SHARED_CPU_INFORMATION.CodeSegmentDescriptor64Ring3) << 48)));

    _WriteMsr(IA32_FMASK_MSR, ((QWORD)0));

    // Set the Syscall RIP
    _WriteMsr(IA32_SYSENTER_EIP, ((QWORD)_SyscallEntryPoint));

    // Set the Syscall RIP
    _WriteMsr(IA32_SYSENTER_CS, gCpuInformation.SHARED_CPU_INFORMATION.CodeSegmentDescriptor64Ring0);
    
    // Set the Syscall RSP
    _WriteMsr(IA32_SYSENTER_ESP, (QWORD)gCpuInformation.PerCpuData[_GetCpuId()].StackAddress);

    // Don't clear flags
    _WriteMsr(IA32_FMASK_MSR, 0);
    
    return OS_STATUS_SUCCESS;
}

void
HandleSyscall(
)
{
    gCpuInformation.PerCpuData[_GetCpuId()].InKernel = TRUE; 
    for(int i=0;i<4;i++) LOG("Woop woop\n");
    // StopApicTimer();
    // LOG("Stopped timer\n");    
    // //LOG("Return rip = %llX and rsp = %llX\n", Rip, Rsp);
}
