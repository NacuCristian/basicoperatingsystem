#ifndef _INTRRUPT_MANAGER_H_
#define _INTRRUPT_MANAGER_H_

#include "../utils/os_common.h"
#include "idt.h"

typedef struct _INTERRUPT_SERVICE_ROUTINE_DATA
{
    BYTE InterruptVector;
    INTERRUPT_REGISTER_STUCTURE *Context;

} INTERRUPT_SERVICE_ROUTINE_DATA;

typedef OS_STATUS (*InterruptServiceRoutine)(INTERRUPT_SERVICE_ROUTINE_DATA *Data);

OS_STATUS
RegisterHandleForInterruptVector(
    BYTE InterruptVector,
    InterruptServiceRoutine Isr,
    QWORD *RegistrationNumber
);

OS_STATUS
RegisterHandleForIrq(
    BYTE IrqNumber,
    InterruptServiceRoutine Isr,
    QWORD *RegistrationNumber
);

OS_STATUS
UnregisterHandle(
    QWORD RegistrationNumber
);

OS_STATUS
HandleUserInterrupt(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
);

OS_STATUS
HandleException(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
);

#endif