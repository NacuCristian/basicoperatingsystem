#include "task_loader.h"
#include "../memory/kern_heap.h"
#include "../datastructures/generic/queue.h"
#include "../memory/virtual_memory_manager.h"

#define RING_3 3
#define INITIAL_FLAGS 0x202 // BIT 1 should be set amd we need interrupts so bit 9 enabled

OS_STATUS
LoadTaskExtendedArgs(
    INTERRUPT_REGISTER_STUCTURE *Regs    
)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    INTERRUPT_REGISTER_STUCTURE *context;
    status = context = HeapAlloc(
        NULL,
        sizeof(INTERRUPT_REGISTER_STUCTURE)
    ); 
    if(context == NULL)
    {
        return OS_STATUS_NULL_POINTER;
    }

    status = KernMemcpy(
        Regs,
        context,
        sizeof(INTERRUPT_REGISTER_STUCTURE)
    );
    if(!SUCCESS(status))
    {
        LOG("Kern memecpy failed? Rly?\n");
        HeapFree(context);
        return status;
    }

    AcquireSpinMutex(&gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobalMutex);
    status = QueueEnqueue(
        gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobaQueue,
        (PVOID)context
    );
    ReleaseSpinMutex(&gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobalMutex);

    if(!SUCCESS(status))
    {
        LOG("Enqueue failed\n");
        HeapFree(context);
        return status;
    }

    // The context is deleted in the scheaduler, when is allocated to the cpu
    return status;
}
QWORD lastPid = 1;
OS_STATUS
LoadTask(
    PVOID Cr3,
    PVOID Stack,
    PVOID TaskStartAddress
)
{
    INTERRUPT_REGISTER_STUCTURE *context;
    OS_STATUS status;

    context = HeapAlloc(
        NULL,
        sizeof(INTERRUPT_REGISTER_STUCTURE)
    );
    
    // All registers will be initialized with 0
    // If you would, however, to send some data to the new task 
    // you might want to initialize some registers with specific values
    KernFill(
        0,
        context,
        sizeof(INTERRUPT_REGISTER_STUCTURE)
    );

    context->Ss = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->Cs = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.CodeSegmentDescriptor64Ring3, RING_3);
    context->COMMON_REGS.Ds = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->COMMON_REGS.Es = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->COMMON_REGS.Fs = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->COMMON_REGS.Gs = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->Rflags = INITIAL_FLAGS;
    context->Rsp = Stack;
    context->Rip = TaskStartAddress;
    context->COMMON_REGS.Cr3 = Cr3;
    context->COMMON_REGS.Cr0 = _GetCr0(); // Omit this for a beautiful GP
    *(QWORD *)(((QWORD)Stack) + PAGE_SIZE_4K - sizeof(QWORD)) = lastPid++;

    status = LoadTaskExtendedArgs(
        context
    );
    if (!SUCCESS(status))
    {
        LOG("Error at creating the task\n");
    }

    HeapFree(context);

    return status;
}

OS_STATUS
CloneTask(
    INTERRUPT_REGISTER_STUCTURE *Ctx,
    PVOID Stack,
    PVOID Cr3,
    QWORD *ChildPid
)
{
    INTERRUPT_REGISTER_STUCTURE *context;
    OS_STATUS status;

    context = HeapAlloc(
        NULL,
        sizeof(INTERRUPT_REGISTER_STUCTURE)
    );
    
    // All registers will be initialized with 0
    // If you would, however, to send some data to the new task 
    // you might want to initialize some registers with specific values
    KernMemcpy(
        Ctx,
        context,
        sizeof(INTERRUPT_REGISTER_STUCTURE)
    );

    KernMemcpy(
        Ctx->Rsp & PAGE_MASK_4K,
        Stack,
        PAGE_SIZE_4K
    );

    context->Ss = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->Cs = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.CodeSegmentDescriptor64Ring3, RING_3);
    context->COMMON_REGS.Ds = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->COMMON_REGS.Es = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->COMMON_REGS.Fs = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->COMMON_REGS.Gs = MAKE_RING_SELECTOR(gCpuInformation.SHARED_CPU_INFORMATION.DataSegmentDescriptor64Ring3, RING_3);
    context->Rsp = ((QWORD)Stack) + ((Ctx->Rsp & (~PAGE_MASK_4K)));
    context->COMMON_REGS.Rax = 0;
    *(QWORD *)(((QWORD)Stack) + PAGE_SIZE_4K - sizeof(QWORD)) = lastPid++;
    *ChildPid = *(QWORD *)(((QWORD)Stack) +PAGE_SIZE_4K - sizeof(QWORD));

    status = LoadTaskExtendedArgs(
        context
    );
    if (!SUCCESS(status))
    {
        LOG("Error at creating the task\n");
    }

    HeapFree(context);

    return status;
}