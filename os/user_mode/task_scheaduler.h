#ifndef _THREADPOOL_H_
#define _THREADPOOL_H_

#include "../utils/os_common.h"
#include "../interrupts/idt.h"

OS_STATUS
InitThreadpool(

);

OS_STATUS
ContextSave(
    INTERRUPT_REGISTER_STUCTURE *CurrentContext
);

OS_STATUS
KernelMainCycle(
    INTERRUPT_REGISTER_STUCTURE *Registers
);

#endif