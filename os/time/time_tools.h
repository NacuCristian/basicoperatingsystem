#ifndef _TIME_H_
#define _TIME_H_

#include "../utils/os_common.h"
#include "../interrupts/interrupt_manager.h"
#include "../devices/lapic.h"

#define SECOND                           ((QWORD)1)
#define ONE_SECOND_IN_MILI_SECONDS       (SECOND * ((QWORD)1000))
#define ONE_SECOND_IN_MICRO_SECONDS      (ONE_SECOND_IN_MILI_SECONDS * ((QWORD)1000))
#define ONE_SECOND_IN_NANO_SECONDS       (ONE_SECOND_IN_MICRO_SECONDS * ((QWORD)1000))
#define ONE_SECOND_IN_PICO_SECONDS       (ONE_SECOND_IN_NANO_SECONDS * ((QWORD)1000))

OS_STATUS
InitTimer(
    BYTE InterruptVectorForLapicTimer
);

void
PitStartTimer(
    WORD TimeInMicroseconds
);

OS_STATUS
SetCallbacksForPit(
    InterruptServiceRoutine Isr,
    QWORD *RegistrationNumber
);

OS_STATUS
TimeLapic(
    QWORD TimeInMicroseconds,
    TIMER_MODE TimerMode
);

OS_STATUS
SetCallbacksForLapicTimer(
    InterruptServiceRoutine Isr,
    QWORD *RegistrationNumber
);

OS_STATUS
StopLapicTimer(

);

#endif