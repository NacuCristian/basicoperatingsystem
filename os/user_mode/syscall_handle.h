#ifndef _SYSCALL_HANDLE_H_
#define _SYSCALL_HANDLE_H_
#include "../utils/os_common.h"
#include "../interrupts/interrupt_manager.h"

OS_STATUS
HandleDispatcher(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
);

#endif