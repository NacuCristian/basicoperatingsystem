#ifndef _TASK_LOADER_H_
#define _TASK_LOADER_H_

#include "../utils/os_common.h"
#include "../interrupts/idt.h"

OS_STATUS
CreateNewCr3ForProcess(
    PVOID CodeAddress,
    QWORD CodeSize,
    PVOID StackAddress,
    QWORD StackSize,
    PVOID DllAddress,
    QWORD DllSize,
    PVOID *NewCr3
);

OS_STATUS
LoadTask(
    PVOID Cr3,
    PVOID Stack,
    PVOID Rip
);

OS_STATUS
CloneTask(
    INTERRUPT_REGISTER_STUCTURE *Ctx,
    PVOID Stack,
    PVOID Cr3,
    QWORD *ChildPid
);

#endif
