#include "string.h"

DWORD
KernStrlen(
    char *String
)
{
    DWORD len = 0;
    DWORD k=0;
    char *aux = String;
    while (*aux != 0)
    {
        aux++;
        k++;
    }
    return k;
}

void
KernItoa(
    QWORD Value,
    char *String,
    DWORD Base
)
{
    BYTE digit = 0;

    QWORD auxValue = Value;
    DWORD length = 0;
    DWORD crtPosition = 0;

    while (auxValue >= Base)
    {
        auxValue = auxValue / Base;
        length++;
    }

    do
    {
        digit = (BYTE)(Value % Base);
        Value /= Base;

        String[length - crtPosition] = (digit > 9 ? digit - 10 + 'A' : digit + '0');
        crtPosition++;
    } while (Value);
    String[length + 1] = 0;
}

void
KernStrcpy(
    char *Source,
    char *Destination
)
{
    while (*Source != 0)
    {
        *Destination = *Source;
        Destination++;
        Source++;
    }
}

void
KernStrcat(
    char *Source,
    char *Destination
)
{
    while (*(Destination++) != 0);
    while ((*Source) != 0)
    {
        *Destination = *Source;
        Destination++;
        Source++;
    }
}

BOOL
KernIsDigit(
    char Character
)
{
    return (Character >= '0' && Character <= '9') ? TRUE : FALSE;
}

BOOL
KernIsLetter(
    char Character
)
{
    return ((Character >= 'a' && Character <= 'z') || (Character >= 'A' && Character <= 'Z')) ? TRUE : FALSE;
}