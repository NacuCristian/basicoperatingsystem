#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include "../utils/os_common.h"
#include "../devices/vga.h"

OS_STATUS
InitSyscall(
    QWORD SyscallRsp
);

void
HandleSyscall(
);

#endif