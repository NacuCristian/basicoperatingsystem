#include "um_initial_process.h"
#include "../../devices/vga.h"
#include "../../utils/os_intrinsics.h"

#include "um_dll.h"

QWORD UspaceStart = 0;

int Cmp(char *s1, char *s2)
{
    while(*s1 == *s2 && *s1 != 0 && *s2 != 0)
    {
        s1++;
        s2++;
    }
    return (*s1 - *s2);
}

int GetNumber(char *s1, int base)
{
    int nr = 0;
    while(*s1 != 0 && ((*s1 >= '0' && *s1 <= '9') || (*s1 >= 'A' || s1 <= 'F')))
    {
        nr *= base;
        if(*s1 >= '0' && *s1 <= '9')
            nr += *s1 - '0';
        else
            nr += *s1 - 'A' + 10;
        s1++;
    }
    return nr;
}

void Itoa(int x, char * buff)
{
    int k = 0;
    int aux = x;

    while(aux > 0)
    {
        k++;
        aux/=10;
    }
    buff[aux] = 0;
    aux --;
    while(x > 0)
    {
        buff[aux] = x % 10;
        x/=10;
        aux--;
    }

}

MUTEX gTaskMtx = {0};
int gSignal = 0;

void InitialProcess()
{
    char *buff[10];
    int pid = 0;
    int ok = 0;
    while(1==1)
    {
        AcquireSpinMutex(&gTaskMtx);
        if(gSignal == 0)
        {
            gSignal = 1;
            ok = 1;
        }
        ReleaseSpinMutex(&gTaskMtx);
        if(ok==1)
        {
            Read(&buff, 10);
            AcquireSpinMutex(&gTaskMtx);
                gSignal = 0;
                ok = 0;
            ReleaseSpinMutex(&gTaskMtx);
            if(Cmp(buff, "HELP") == 0)
            {
                Write("Help:\n");
                Write("type PF for Page Fault\n");
                Write("type GP for General Protection Fault\n");
                Write("type FRK for Fork\n");
                Write("type GPID for Get PID\n");
                Write("type EXIT to close this thread\n");
            }
            if(Cmp(buff, "PF") == 0)
            {
                *((BYTE *)0x711121);
            }
            else if(Cmp(buff, "GP") == 0)
            {
                _Break(); // CLI + HLT
            }
            else if(Cmp(buff, "FRK") == 0)
            {
                pid = Fork();
                LOG("Pid = %X\n", pid);
                if(pid == 0)
                {
                    Write("Child\n");
                }
                else
                {
                    Write("Parent\n");
                }
            }
            else if(Cmp(buff, "GPID") == 0)
            {
                int x = GetPid();
                LOG("Pid = %lld", x);
                char buf[24] = "PID = ";
                Itoa(pid, (char *)(buf+6));
                Write(buf);
            }
             else if(Cmp(buff, "EXIT") == 0)
            {
                Exit();
            }
        }
        else
        {
            Write("Process just hanging around\n");
        }
        
    for(int i=0;i<10;i++)
        buff[i] = 0;
    for(long long unsigned i=0;i<0xFFFFFFFF;i++);

    }
    while(1==1);
}
