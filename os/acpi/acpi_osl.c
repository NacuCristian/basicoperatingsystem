
#include "acpi.h"
#include "../memory/virtual_memory_manager.h"
#include "../memory/kern_heap.h"
#include "../tools/standard_tools.h"
#include "../utils/os_common.h"

#ifndef ACPI_USE_ALTERNATE_PROTOTYPE_AcpiOsMapMemory

void *
AcpiOsMapMemory(
    ACPI_PHYSICAL_ADDRESS   Where,
    ACPI_SIZE               Length
)
{

    OS_STATUS status;
    status = VirtMemMngrMapVaToPa(
       (PVOID)&gCpuInformation.SHARED_CPU_INFORMATION.KernelCr3,
       Where,
       Where,
       Length,
       STRONGLY_UNCACHABLE,
       FALSE
    );
    if(!SUCCESS(status))
    {
        LOG("VirtMemMngrMapVaToPa failed with status %llX", status);
        BREAKPOINT;
    }

    return ((PVOID) Where);
}
#endif

#ifndef ACPI_USE_ALTERNATE_PROTOTYPE_AcpiOsUnmapMemory
void
AcpiOsUnmapMemory(
    void                    *LogicalAddress,
    ACPI_SIZE               Size)
{
    return;
}
#endif

#ifndef ACPI_USE_ALTERNATE_PROTOTYPE_AcpiOsGetPhysicalAddress
ACPI_STATUS
AcpiOsGetPhysicalAddress(
    void                    *LogicalAddress,
    ACPI_PHYSICAL_ADDRESS   *PhysicalAddress)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    QWORD address = 0;
    status = VirtMemMngrGetPaForVa(
          (PVOID)&gCpuInformation.SHARED_CPU_INFORMATION.KernelCr3,
          (QWORD)LogicalAddress,
          &address
      );
    if(!SUCCESS(status))
    {
        //LOG("ERROR!!\n");
        return AE_ERROR;
    }
    *PhysicalAddress = address;
}
#endif

#ifndef ACPI_USE_NATIVE_RSDP_POINTER
ACPI_PHYSICAL_ADDRESS
AcpiOsGetRootPointer(
    void)
{
    ACPI_PHYSICAL_ADDRESS  Ret;
    Ret = 0;
    AcpiFindRootPointer(&Ret);
    return Ret;
}
#endif
void
AcpiOsFree(
    void  *Mem
)
{
    return;
}

void *
AcpiOsAllocate(
    ACPI_SIZE               Size)
{
    return HeapAlloc(
        NULL,
        Size
    );
}

void *
AcpiOsAllocateZeroed (
    ACPI_SIZE               Size)
{
    PVOID address = HeapAlloc(
        NULL,
        Size
    );
    if(address == NULL)
    {
        return NULL;
    }
    KernFill(
        0,
        address,
        Size
    );
    return address;
    
}

ACPI_STATUS
AcpiOsCreateSemaphore(
    UINT32              MaxUnits,
    UINT32              InitialUnits,
    ACPI_HANDLE         *OutHandle)
{
    *OutHandle = (ACPI_HANDLE)1;
    return (AE_OK);
}

ACPI_STATUS
AcpiOsDeleteSemaphore(
    ACPI_HANDLE         Handle)
{
    return (AE_OK);
}

ACPI_STATUS
AcpiOsWaitSemaphore(
    ACPI_HANDLE         Handle,
    UINT32              Units,
    UINT16              Timeout)
{
    return (AE_OK);
}

ACPI_STATUS
AcpiOsSignalSemaphore(
    ACPI_HANDLE         Handle,
    UINT32              Units)
{
    return (AE_OK);
}

ACPI_THREAD_ID
AcpiOsGetThreadId(
    void)
{
    return (ACPI_THREAD_ID)0;
}

ACPI_STATUS
AcpiOsCreateLock(
    ACPI_SPINLOCK           *OutHandle)
{
    return (ACPI_STATUS)0;
}

void
AcpiOsDeleteLock(
    ACPI_SPINLOCK           Handle)
{
}

ACPI_CPU_FLAGS
AcpiOsAcquireLock(
    ACPI_SPINLOCK           Handle)
{
    return (0);
}

void
AcpiOsReleaseLock(
    ACPI_SPINLOCK           Handle,
    ACPI_CPU_FLAGS          Flags)
{

}

void ACPI_INTERNAL_VAR_XFACE
AcpiOsPrintf(
    const char              *Fmt,
    ...)
{
    return;
}

ACPI_STATUS
AcpiOsTableOverride(
    ACPI_TABLE_HEADER       *ExistingTable,
    ACPI_TABLE_HEADER       **NewTable)
{
    *NewTable = NULL;
    return (ACPI_STATUS)0;
}

void
AcpiOsStall(
    UINT32                  Microseconds)
{
    return;
}

ACPI_STATUS
AcpiOsPhysicalTableOverride(
    ACPI_TABLE_HEADER       *ExistingTable,
    ACPI_PHYSICAL_ADDRESS   *NewAddress,
    UINT32                  *NewTableLength)
{
    return (AE_SUPPORT);
}


void
AcpiOsSleep(
    UINT64                  Milliseconds)
{
    return;
}

UINT32
AcpiOsInstallInterruptHandler(
    UINT32                  InterruptNumber,
    ACPI_OSD_HANDLER        ServiceRoutine,
    void                    *Context)
{
    return (AE_OK);
}

ACPI_STATUS
AcpiOsRemoveInterruptHandler(
    UINT32                  InterruptNumber,
    ACPI_OSD_HANDLER        ServiceRoutine)
{
    return (AE_OK);
}


ACPI_STATUS
AcpiOsReadMemory(
    ACPI_PHYSICAL_ADDRESS   Address,
    UINT64                  *Value,
    UINT32                  Width)
{
    return (AE_OK);
}

ACPI_STATUS
AcpiOsWriteMemory(
    ACPI_PHYSICAL_ADDRESS   Address,
    UINT64                  Value,
    UINT32                  Width)
{
    return (AE_OK);
}

ACPI_STATUS
AcpiOsWritePort(
    ACPI_IO_ADDRESS         Address,
    UINT32                  Value,
    UINT32                  Width)
{
    return (AE_OK);

}

ACPI_STATUS
AcpiOsReadPort(
    ACPI_IO_ADDRESS         Address,
    UINT32                  *Value,
    UINT32                  Width)
{
    return (AE_OK);
}


ACPI_STATUS
AcpiOsExecute(
    ACPI_EXECUTE_TYPE       Type,
    ACPI_OSD_EXEC_CALLBACK  Function,
    void                    *Context)
{
    return (0);
}

void
AcpiOsWaitEventsComplete(
    void)
{
    return;
}

ACPI_STATUS
AcpiOsSignal(
    UINT32                  Function,
    void                    *Info)
{
    return (AE_OK);
}

UINT64
AcpiOsGetTimer(
    void)
{
    return (UINT64)0;
}

ACPI_STATUS
AcpiOsReadPciConfiguration(
    ACPI_PCI_ID             *PciId,
    UINT32                  Register,
    UINT64                  *Value,
    UINT32                  Width)
{
    *Value = 0;
    return (AE_OK);
}


ACPI_STATUS
AcpiOsWritePciConfiguration(
    ACPI_PCI_ID             *PciId,
    UINT32                  Register,
    UINT64                  Value,
    UINT32                  Width)
{
    return (AE_OK);
}


ACPI_STATUS
AcpiOsPredefinedOverride(
    const ACPI_PREDEFINED_NAMES *InitVal,
    ACPI_STRING                 *NewVal)
{
    if (!InitVal || !NewVal)
    {
        return (AE_BAD_PARAMETER);
    }

    *NewVal = NULL;
    return (AE_OK);
}

ACPI_STATUS
AcpiOsInitialize (
    void)
{
    return (AE_OK);
}

ACPI_STATUS
AcpiOsEnterSleep (
    UINT8                   SleepState,
    UINT32                  RegaValue,
    UINT32                  RegbValue)
{
    return (AE_OK);
}

ACPI_STATUS
AcpiOsTerminate (
    void)
{
    return(AE_OK);
}