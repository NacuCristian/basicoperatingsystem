#ifndef _LOGS_H_
#define _LOGS_H_

#include "../tools/vprintf.h"
#include "../utils/os_defs.h"
#include "../driver/vga_driver.h"

#define LOG(...) VgaPrint(BLACK, GREEN, __VA_ARGS__)
#define PRINT_FROM_UM(...) VgaPrint(BLACK, LIGHT_GRAY, __VA_ARGS__)

void 
VgaPrint(
    COLOR_VALUES Background,
    COLOR_VALUES Foreground,
    char * Format,
    ...
);

void 
VgaSetCursor(

);

void 
VgaGetCursor(

);

void 
VgaSetColor(

);

void 
VgaClearColor(
    void
);

void
VgaClearScreen(

);

#endif