#include "gdt_tools.h"
#include "../memory/kern_heap.h"
#include "../memory/kern_pagepool.h"


OS_STATUS
CreateGdtStructure(
    GDT_INFO_64 **Gdtr
)
{
    GDT_INFO_64 *shadowGdt = NULL;
    GDT_TABLE_64 *gdtTable = NULL;

    shadowGdt = HeapAlloc(NULL, sizeof(GDT_INFO_64));
    if(shadowGdt == NULL)
    {
        LOG("HeapAlloc Failed\n");
        return OS_STATUS_NULL_POINTER;
    }

    gdtTable = PagePoolAlloc(NULL, PAGE_SIZE_4K);
    if(gdtTable == NULL)
    {
        LOG("PagePoolAlloc Failed\n");
        return OS_STATUS_NULL_POINTER;
    }

    gdtTable->Zero = 0;

    shadowGdt->TableStart = (QWORD)gdtTable;
    shadowGdt->GdtLimit = 8;
    *Gdtr = shadowGdt;
    return OS_STATUS_SUCCESS;
}

WORD
GetCurrentGdtOffset(
    GDT_INFO_64 *Gdtr
)
{
    return Gdtr->GdtLimit;
}

OS_STATUS
RegisterGdtDescriptor(
    GDT_INFO_64 *Gdtr,
    QWORD *Descriptor,
    DWORD DescriptorSizeInQwords  
)
{
    WORD currentOffset;
    GDT_TABLE_64 *gdtTable;

    if(Gdtr == NULL)
    {
        return OS_STATUS_INVALID_PARAMETER_1;
    }

    gdtTable = (GDT_TABLE_64 *)Gdtr->TableStart;
    currentOffset = (Gdtr->GdtLimit - 8) / 8;

    for(DWORD crtDescriptor = 0; crtDescriptor < DescriptorSizeInQwords; crtDescriptor++)
    {
        gdtTable->Descriptors[currentOffset++] = Descriptor[crtDescriptor];
    }

    Gdtr->GdtLimit = (currentOffset + 1) * 8;
    return OS_STATUS_SUCCESS;
}
