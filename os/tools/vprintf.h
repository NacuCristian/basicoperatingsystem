#ifndef _VPRINTF_H_
#define _VPRINTF_H_

#include "string.h"

int
Vprintf(
    char *Outbuffer, 
    char *Format, 
    ...
);

int
StrFormat(
    char **Format, 
    va_list Args, 
    char *Outbuffer
);

#endif