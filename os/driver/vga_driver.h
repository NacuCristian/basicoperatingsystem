#ifndef _VGA_DRIVER_H_
#define _VGA_DRIVER_H_

#include "../utils/os_common.h"

#define BIOS_COLOR BYTE

typedef enum _COLOR_VALUES{
    BLACK = 0,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    BROWN,
    LIGHT_GRAY,
    DARK_GRAY,
    LIGHT_BLUE,
    LIGHT_GREEN,
    LIGHT_CYAN,
    LIGHT_RED,
    LIGHT_MAGENTA,
    YELLOW,
    WHITE
} COLOR_VALUES;

void
RefreshScreen(
);

void
VgaPrinBsod(
);

void
VgaWrite(
    BYTE *Text,
    COLOR_VALUES Bg,
    COLOR_VALUES Fg
);

void
KeyboardWriteCharacter(
    BYTE Value,
    COLOR_VALUES Bg,
    COLOR_VALUES Fg
);

void
KeyboardDeleteCharacter(
    COLOR_VALUES Bg,
    COLOR_VALUES Fg
);

void
KeyboardReset(
    COLOR_VALUES Bg,
    COLOR_VALUES Fg
);

void 
VgaMoveWindow(
    BYTE Direction
);

OS_STATUS
VgaInit(
    DWORD *VgaBuffer,
    QWORD Size
);

#endif