#ifndef _UM_DLL_
#define _UM_DLL_
#include "../../devices/vga.h"

int
Fork();

int
GetPid();

int
Exit();

int
Read(char *Buffer, unsigned int Count);

int
Write(char *Buffer);

#endif