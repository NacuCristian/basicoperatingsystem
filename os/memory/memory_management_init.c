#include "memory_management_init.h"
#include "virtual_memory_manager.h"
#include "kern_heap.h"
#include "kern_pagepool.h"
#include "../tools/standard_tools.h"
#include "../driver/vga_driver.h"

extern QWORD CurrentPagePointer;
extern PVOID Cr3Base;

OS_STATUS
InitMemoryManagement(
    E820_MAP_STRUCTURE *E820Map
)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    DWORD noEntries;
    PVOID KernelSpace;
    PVOID VgaSpace;
    PVOID HeapSpace;
    PVOID PagePoolSpace;

    noEntries = E820Map->MapSize / sizeof(E820_MAP_ENTRY);

    status = VirtMemMngrInit(
        ((PVOID)CurrentPagePointer)
    );
    BOOL  Found = FALSE;
    for (DWORD position = 0; position <= noEntries; position++)
    {
        if(E820Map->Map[position].BaseAddress > 200*MEGA &&
            E820Map->Map[position].Length > 100*MEGA &&
            E820Map->Map[position].Type ==  1 &&
            !Found)
        {
            KernelSpace = (PVOID)(E820Map->Map[position].BaseAddress);
            Found = TRUE;
        }
    }

    status = VirtMemMngrMapVaToPa(
        (PVOID)&Cr3Base,    // REPLACE THIS WITH GLOBAL STRUCTURE
        (QWORD)0xB800,
        (QWORD)0xB800,
        PAGE_SIZE_4K,
        STRONGLY_UNCACHABLE,
        FALSE
    );

    status = VirtMemMngrMapVaToPa(
        (PVOID)&Cr3Base,
        (QWORD)KernelSpace,
        (QWORD)KernelSpace,
        100*MEGA,
        WRITEBACK_CACHABLE,
        FALSE
    );

    KernFill
    (
        0,
        KernelSpace,
        100*MEGA
    );

    VgaSpace = (PVOID)((QWORD)KernelSpace + 91 * MEGA);
    HeapSpace = (PVOID)((QWORD)KernelSpace + 60 * MEGA);
    PagePoolSpace = (PVOID)((QWORD)KernelSpace);
    gCpuInformation.SHARED_CPU_INFORMATION.KernelSpaceStart = KernelSpace;
    VgaInit(
        VgaSpace,
        8 * MEGA
    );

    HeapInit(
        HeapSpace,
        30*MEGA
    );

    HeapSetDefault(
        HeapSpace
    );

    PagePoolInit(
        PagePoolSpace,
        40*MEGA
    );

    SetPagingArea(
        PagePoolSpace
    );

    VirtMemMngrRegisterAllocatonMethod(
        PagePoolAllocMappingPage    
    );

    VirtMemMngrRegisterFreeMethod(
        PagePoolFreeMappingPage    
    );

    LOG("Memory space initialized as such:\nAt %llX - 60MB - PagePoolSpace\nAt %llX - 30MB - HeapSpace\nAt %llX - 9MB - VgaMemory\n",
    PagePoolSpace, HeapSpace, VgaSpace);
    return status;
}