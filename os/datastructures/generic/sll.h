#ifndef _SLL_DATASTRUCT_H_
#define _SLL_DATASTRUCT_H_

#include "../../utils/os_common.h"
#include "../datastructures_defs.h"
#include "../synchronization/mutex.h"

#define LIST_TYPE                   BYTE
#define LIST_SLL_SYNCHRONIZED       ((LIST_TYPE) 1)
#define LIST_SLL_NOT_SYNCHRONIZED   ((LIST_TYPE) 2)

typedef struct _SLL_HEADER{
    
    // Head ofthe list, Muts be the first structure so the sll header can be conisdered a GENERIC_SINGLE_LINKED_NODE
    GENERIC_SINGLE_LINKED_NODE *Element;

    // Used for marking the type of list
    LIST_TYPE ListType;

} SLL_HEADER, *PSLL_HEADER;

OS_STATUS
SllInit(
    LIST_TYPE SllType,
    CompareElements Comparator,
    SLL_HEADER **SllHead
);

OS_STATUS
SllInsertElementFront(
    SLL_HEADER *SllHead,
    PVOID *Element
);

OS_STATUS
SllInsertElementEnd(
    SLL_HEADER *SllHead,
    PVOID *Element
);

PVOID
SllDeleteElement(
    SLL_HEADER *SllHead,
    PVOID *Element
);

PVOID
SllDeleteFirst(
    SLL_HEADER *SllHead
);

OS_STATUS
SllSearchElement(
    SLL_HEADER *SllHead,
    PVOID SearchedData,
    GENERIC_SINGLE_LINKED_NODE **PreviousNode
);

BOOL
SllIsEmpty(
    SLL_HEADER *SllHead
);

OS_STATUS
SllDestroy(
    SLL_HEADER *SllHead
);



#endif