#ifndef _KERN_PAGEPOOL_
#define _KERN_PAGEPOOL_

#include "../utils/os_status.h"
#include "../utils/os_defs.h"
#include "../devices/vga.h"

typedef struct _PAGEPOOL_HEADER
{
    DWORD Magic;
    QWORD PagePoolSize;
    QWORD PagePoolStart;
    QWORD BitmapEnd;
}PAGEPOOL_HEADER;

OS_STATUS
PagePoolInit(
    PAGEPOOL_HEADER *PagepoolArea,
    QWORD HeapSize
);

PVOID
PagePoolAlloc(
    PAGEPOOL_HEADER *PagepoolStart,
    QWORD Size
);

OS_STATUS
PagePoolFree(
    PAGEPOOL_HEADER *PagepoolArea,
    PVOID *PageAddress
);

OS_STATUS
SetPagingArea(
    PAGEPOOL_HEADER *PagepoolArea 
);

OS_STATUS
PagePoolAllocMappingPage(
    PVOID * MemoryPageAddress
);

OS_STATUS
PagePoolFreeMappingPage(
    PVOID MemoryPageAddress
);

#endif
