#include "vprintf.h"

int
StrArgument(
    char **Format, 
    va_list *Args, 
    char **Outbuffer
);

int
StrFormat(
    char **Format, 
    va_list Args, 
    char *Outbuffer
);

int
Vprintf(
    char *Outbuffer, 
    char *Format, 
    ...
)
{
    va_list args;
    int status = 0;

    va_start(args, Format);
    status = StrFormat(&Format, args, Outbuffer);
    va_end(args);
    return 0;
}

int
StrFormat(
    char **Format, 
    va_list Args, 
    char *Outbuffer
)
{   
    int i=0;
    while (**Format != 0) {
        if ((**Format) != '%')
        {
            (*Outbuffer) = (**Format);
            Outbuffer++;
            (*Format)++;
        }
        else
        {
            StrArgument(
                Format,
                &Args,
                &Outbuffer
            );
        }
        i++;
    }
    (*Outbuffer) = 0;
    return 0;
}

int
StrArgument(
    char **Format, 
    va_list *Args, 
    char **Outbuffer
)
{
(*Format)++;  //skip the '%' character
    int precisionIntegralPart = 0;
    char fillCharacter = '0';

    char string[MAX_STRING_SIZE];
    char sign = 0; //Replace with NULL
    int base = 0;
    int len;

    while (KernIsDigit(**Format))
    {
        precisionIntegralPart = precisionIntegralPart * 10 + ((**Format) - '0');
        (*Format)++;
    }

    if ((**Format) == 'd' || (**Format) == 'i')
    {
        int value;

        base = 10;
        value = va_args(*Args, int);
        if (value < 0)
        {
            sign = '-';
            value = -value;
        }

        KernItoa(value, string, base);
        (*Format)++;
    }
    else if ((**Format) == 'u')
    {
        unsigned int value;

        base = 10;
        value = va_args(*Args, unsigned int);
        KernItoa(value, string, base);
        (*Format)++;
    }
    else if ((**Format) == 'o')
    {
        unsigned int value;

        base = 8;
        value = va_args(*Args, unsigned int);
        KernItoa(value, string, base);
        (*Format)++;
    }
    else if ((**Format) == 'x' || (**Format) == 'X')
    {
        unsigned int value;

        base = 16;
        value = va_args(*Args, unsigned int);
        KernItoa(value, string, base);
        (*Format)++;
    }
    else if ((**Format) == 'p')
    {
        QWORD value;

        base = 16;
        value = va_args(*Args, unsigned int);
        KernItoa(value, string, base);
        (*Format)++;
    }
    else if ((**Format) == 'l')
    {
        if ((*(*Format + 1)) == 'l')
        {
            if ((*(*Format + 2)) == 'd')
            {
                long long int value;
            
                base = 10;
                value = va_args(*Args, long long int);
                if (value < 0)
                {
                    sign = '-';
                    value = -value;
                }

                KernItoa(value, string, base);
                (*Format)+=3;
            }
            else if ((*(*Format + 2)) == 'x' || (*(*Format + 2)) == 'X')
            {
                unsigned long long int value;

                base = 16;
                value = va_args(*Args, unsigned long long int);
                KernItoa(value, string, base);
                (*Format) += 3;
            }
             else if ((*(*Format + 2)) == 'u')
            {
                unsigned long long int value;

                base = 10;
                value = va_args(*Args, long long int);

                KernItoa(value, string, base);
                (*Format)+=3;
            }
        }
        else if ((**Format + 1) == 'd')
        {
            long int value;

            base = 10;
            value = va_args(*Args, long int);
            if (value < 0)
            {
                sign = '-';
                value = -value;
            }

            KernItoa(value, string, base);
            (*Format) += 2;
        }
        else if ((**Format + 1) == 'x' || (**Format + 1) == 'X')
        {
            unsigned long int value;

            base = 10;
            value = va_args(*Args, unsigned long int);
            KernItoa(value, string, base);
            (*Format) += 2;
        }
    }
    else if ((**Format) == 'c')
    {
        char value;

        precisionIntegralPart = 0;
        value = va_args(*Args, char);
        string[0] = value;
        string[1] = 0;
        (*Format)++;
    }
    else if ((**Format) == 's')
    {
        char *value;
        int position = 0;

        precisionIntegralPart = 0;
        value = va_args(*Args, char *);
        while (position < MAX_STRING_SIZE && ((*value) != 0))
        {
            string[position] = *value;
            value++;
            position++;
        }
        string[position] = 0;
        (*Format)++;
    }

    len = KernStrlen(string);

    if (sign != 0)
    {
        precisionIntegralPart = max(precisionIntegralPart - 1, 0);
    }

    if (base == 16)
    {
        precisionIntegralPart = max(precisionIntegralPart - 2, 0);
    }
    else if (base == 8)
    {
        precisionIntegralPart = max(precisionIntegralPart - 1, 0);
    }

    if (sign != 0)
    {
        **Outbuffer = sign;
        (*Outbuffer)++;
    }

    if (base == 16)
    {
        **Outbuffer = '0';
        (*Outbuffer)++;
        **Outbuffer = 'x';
        (*Outbuffer)++;
    }
    else if (base == 8)
    {
        **Outbuffer = '0';
        (*Outbuffer)++;
    }

    if (len < precisionIntegralPart)
    {
        for (int distance = 0; distance < precisionIntegralPart - len; distance++)
        {
            **Outbuffer = fillCharacter;
            (*Outbuffer)++;
        }
    }

    for (int position = 0; position < len; position++)
    {
        **Outbuffer = string[position];
        (*Outbuffer)++;
    }
    return 0;
}