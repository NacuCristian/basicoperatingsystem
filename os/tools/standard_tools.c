#include "standard_tools.h"

OS_STATUS
KernMemcpy(
    PVOID * Source,
    PVOID * Destination,
    QWORD Length
)
{
    for(QWORD i=0; i<Length; i++)
    {
        ((BYTE *)Destination)[i] = ((BYTE *)Source)[i];
    }

    return OS_STATUS_SUCCESS;
}

OS_STATUS
KernFill(
    BYTE Value,
    PVOID * Destination,
    QWORD Length
)
{
    for(QWORD i=0; i<Length; i++)
    {
        ((BYTE *)Destination)[i] = Value;
    }

    return OS_STATUS_SUCCESS;
}

// Returns TRUE if buffers are equal
BOOL
KernMemCompare(
    BYTE *Buffer1,
    BYTE *Buffer2,
    QWORD Size
)
{
    for(QWORD i=0;i<Size;i++)
    {
        if(Buffer1[i] != Buffer2[i])
        {
            return FALSE;
        }
    }
    return TRUE;
}