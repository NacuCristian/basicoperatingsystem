
typedef unsigned char BYTE;
typedef unsigned char BOOL;
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef unsigned long long QWORD;
typedef void *PVOID;
#define FALSE 0
#define TRUE 1

#ifndef _STRINGs_H_
#define _STRINGs_H_

#include "../utils/os_defs.h"

DWORD
KernStrlen(
    char *String
);

void
KernItoa(
    QWORD Value,
    char *String,
    DWORD Base
);

void
KernStrcpy(
    char *Source,
    char *Destination
);

void
KernStrcat(
    char *Source,
    char *Destination
);

BOOL
KernIsDigit(
    char Character
);

BOOL
KernIsLetter(
    char Character
);

#endif