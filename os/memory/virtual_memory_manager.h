#ifndef _VIRTUAL_MEMORY_MANAGER_
#define _VIRTUAL_MEMORY_MANAGER_

#include "../utils/os_defs.h"
#include "../utils/os_status.h"

typedef OS_STATUS (*GET_MEMORY_PAGE)(PVOID * MemoryPageAddress);
typedef OS_STATUS (*FREE_MEMORY_PAGE)(PVOID MemoryPageAddress);

typedef enum _CACHING_TYPE{
    WRITEBACK_CACHABLE = 0,
    STRONGLY_UNCACHABLE = 3
} CACHING_TYPE;

OS_STATUS 
VirtMemMngrInit(
    PVOID LastPage
);

OS_STATUS
VirtMemMngrRegisterAllocatonMethod(
    GET_MEMORY_PAGE GetMemoryPage 
);

OS_STATUS
VirtMemMngrRegisterFreeMethod(
    FREE_MEMORY_PAGE FreeMemoryPage
);

OS_STATUS 
VirtMemMngrMapVaToPa(
    PVOID* Cr3,
    QWORD PhysicalAddress,
    QWORD VirtualAddress,
    QWORD Size,
    CACHING_TYPE CacheType,
    BOOL MapIn2MegaPages
);

OS_STATUS
VirtMemMngrGetPaForVa(
    PVOID* Cr3,
    QWORD VirtualAddress,
    QWORD *PhysicalAddress
);

OS_STATUS 
VirtMemMngrUnmapVaToPa(
    PVOID Cr3,
    QWORD VirtualAddress
);

#endif