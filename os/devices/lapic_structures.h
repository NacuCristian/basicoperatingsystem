#ifndef _LAPIC_STRUCTURES_H_
#define _LAPIC_STRUCTURES_H_

#include "../utils/os_defs.h"

#define IA32_APIC_BASE_MSR 0x1B
#define APIC_BASE_MASK 0xFFFFFF000
#define X2APIC_BASE 0x800
#define LAPIC_TIMER_STOP_VALUE 0
// Apic register map used for controlling the device
typedef enum _APIC_FIELDS
{
   OFFSET_LAPIC_ID = 0x20,
   OFFSET_LAPIC_VERSION = 0x30,
   OFFSET_TASK_PRIORITY_REGISTER = 0x80,
   OFFSET_ARBITRATION_PRIORITY_REGISTER = 0x90,
   OFFSET_PROCESSOR_PRIORITY_REGISTER = 0xA0,
   OFFSET_EOI_REGISTER = 0xB0,
   OFFSET_REMOTE_READ_REGISTER = 0xC0,
   OFFSET_LOGICAL_DESTINATION_REGISTER = 0xD0,
   OFFSET_DESTINATION_FORMAT_REGISTER = 0XE0,
   OFFSET_SPURIOUS_INTERRUPT_VECTOR_REGISTER = 0XF0,
   OFFSET_IN_SERVICE_REGISTER_32 = 0x100,
   OFFSET_IN_SERVICE_REGISTER_64 = 0x110,
   OFFSET_IN_SERVICE_REGISTER_96 = 0x120,
   OFFSET_IN_SERVICE_REGISTER_128 = 0x130,
   OFFSET_IN_SERVICE_REGISTER_160 = 0x140,
   OFFSET_IN_SERVICE_REGISTER_192 = 0x150,
   OFFSET_IN_SERVICE_REGISTER_224 = 0x160,
   OFFSET_IN_SERVICE_REGISTER_256 = 0x170,
   OFFSET_TRIGGER_MODE_REGISTER_32 = 0x180, 
   OFFSET_TRIGGER_MODE_REGISTER_64 = 0x190, 
   OFFSET_TRIGGER_MODE_REGISTER_96 = 0x1A0, 
   OFFSET_TRIGGER_MODE_REGISTER_128 = 0x1B0, 
   OFFSET_TRIGGER_MODE_REGISTER_160 = 0x1C0, 
   OFFSET_TRIGGER_MODE_REGISTER_192 = 0x1D0, 
   OFFSET_TRIGGER_MODE_REGISTER_224 = 0x1E0, 
   OFFSET_TRIGGER_MODE_REGISTER_256 = 0x1F0,
   OFFSET_INTERRUPT_REQUEST_REGISTER_32 = 0x200, 
   OFFSET_INTERRUPT_REQUEST_REGISTER_64 = 0x210, 
   OFFSET_INTERRUPT_REQUEST_REGISTER_96 = 0x220, 
   OFFSET_INTERRUPT_REQUEST_REGISTER_128 = 0x230, 
   OFFSET_INTERRUPT_REQUEST_REGISTER_160 = 0x240, 
   OFFSET_INTERRUPT_REQUEST_REGISTER_192 = 0x250, 
   OFFSET_INTERRUPT_REQUEST_REGISTER_224 = 0x260, 
   OFFSET_INTERRUPT_REQUEST_REGISTER_256 = 0x270, 
   OFFSET_ERROR_STATUS_REGISTER = 0x280,
   OFFSET_LVT_CORRECTED_MACHINE_CHECK_INTERRUPT = 0x2F0,
   OFFSET_INTERRUPT_COMMAND_REGISTER_32 = 0x300, 
   OFFSET_INTERRUPT_COMMAND_REGISTER_64 = 0x300, 
   OFFSET_LVTR_TIMER_REGISTER = 0x320, 
   OFFSET_LVT_THERMAL_SENSOR_REGISTER = 0x330, 
   OFFSET_LVT_PERFORMANCE_MONITOR_COUNTER_REGISTER = 0x340, 
   OFFSET_LVT_INT0 = 0x350, 
   OFFSET_LVT_INT1 = 0x360,
   OFFSET_LVT_ERROR_REGISTER = 0x370,
   OFFSET_INITIAL_COUNT_REGISTER = 0x380,
   OFFSET_CURRENT_COUNT_REGISTER = 0x390,
   OFFSET_DIVIDE_CONFIGURATION_REGISTER = 0x3E0
} APIC_FIELDS;

#pragma pack(1)
typedef struct _INTERRUPT_COMMAND_REGISTER_STRUCTURE_32
{
    union
    {
        struct 
        {
            DWORD Vector : 8;
            DWORD DeliveryMode : 3;
            DWORD DestinationMode : 1;
            DWORD DeliveryStatus : 1;
            DWORD Reserved1 : 1;
            DWORD Level : 1;
            DWORD TriggerMode : 1;
            DWORD Reserved2 : 2;
            DWORD DestinationShorthand : 2;
            DWORD Reserved3 : 12;
        } FIELDS; 
        DWORD Raw;
    };
} INTERRUPT_COMMAND_REGISTER_STRUCTURE_32;
#pragma pack()

#pragma pack(1)
typedef struct _INTERRUPT_COMMAND_REGISTER_STRUCTURE_64
{
    union
    {
        struct 
        {
            DWORD Reserved3 : 24;
            DWORD DestinationField : 8;
        } FIELDS; 
        DWORD Raw;
    };
} INTERRUPT_COMMAND_REGISTER_STRUCTURE_64;
#pragma pack()

#pragma pack(1)

/// TIMER CONTROL REGISTERS ////
typedef struct _DIVIDE_CONFIGURATION_REGISTER
{
    union
    {
        struct{
            DWORD DivideValue : 4;
            DWORD Reserved : 32 - 4;
        } FIELDS;
        DWORD Raw;
    };
} DIVIDE_CONFIGURATION_REGISTER;
#pragma pack()

#pragma pack(1)

typedef struct _INITIAL_COUNT_REGISTER
{
    volatile DWORD Count;
} INITIAL_COUNT_REGISTER;
#pragma pack()

#pragma pack(1)
typedef struct _CURRENT_COUNT_REGISTER
{
    volatile DWORD Count;
} CURRENT_COUNT_REGISTER;
#pragma pack()

#pragma pack(1)
typedef struct _LVT_TIMER_REGISTER
{
    union
    {
        struct{
            DWORD Vector : 8;
            DWORD Reserved : 4;
            DWORD DeliveryStatus : 1;
            DWORD Reserved2 : 3;
            DWORD Mask : 1;
            DWORD TimerMode : 2;
            DWORD Reserved3 : 13;
        }FIELDS;
        volatile DWORD Raw;
    };
} LVT_TIMER_REGISTER;
#pragma pack()

#pragma pack(1)
typedef struct _LAPIC_EOI_REGISTER
{
    volatile DWORD Value;
} LAPIC_EOI_REGISTER;
#pragma pack()


#define DELIVERY_STATUS_IDLE 0
#define DELIVERY_STATUS_SEND_PENDING 1

#define LEVEL_DEASSERT 0
#define LEVEL_ASSERT 1

#define TRIGGER_MODE_EDGE 0
#define TRIGGER_MODE_LEVEL 1

#endif