#include "lapic.h"
#include "lapic_structures.h"
#include "vga.h"
#include "../memory/virtual_memory_manager.h"

BOOL isX2Apic = TRUE;

void
LapicWrite(QWORD Field, QWORD Data)
{
    if(isX2Apic)
    {   
        QWORD Msr = X2APIC_BASE + ((Field >> 4)&0xFF);
        if(Field != OFFSET_INTERRUPT_COMMAND_REGISTER_32)
        {
            Data &= DWORD_MASK;
        }
        _WriteMsr(Msr, Data);
    }
}

QWORD
LapicRead(QWORD Field)
{
    if(isX2Apic)
    {   
        QWORD Msr = X2APIC_BASE + ((Field >> 4)&0xFF);
        return _ReadMsr(Msr);
    }
}

OS_STATUS
IsApicInitialized(
)
{
    return OS_STATUS_SUCCESS;
}

OS_STATUS
InitApicSystem(
    PVOID RemapAddress
)
{
    QWORD ia32apicbassemsr;
    BYTE* apicbase; 
    OS_STATUS status = OS_STATUS_SUCCESS;

    ia32apicbassemsr = _ReadMsr(IA32_APIC_BASE_MSR);

    // If APIC isn't globally enabled, enable it
    if(!(ia32apicbassemsr & BIT(11)))
    {
        ia32apicbassemsr |= BIT(11); 
    }
    if(1 == 1) // x2APIC Condition
    {
        ia32apicbassemsr |= BIT(10);
    }
    _WriteMsr(IA32_APIC_BASE_MSR, ia32apicbassemsr);

    apicbase = (BYTE *)(ia32apicbassemsr & APIC_BASE_MASK);

    // Make this beautiful
    LapicWrite(
        OFFSET_SPURIOUS_INTERRUPT_VECTOR_REGISTER,
        (((DWORD)(0)) | ((DWORD)(39)) | ((DWORD)(1 << 8)))
    );

    LapicWrite(
        OFFSET_LVT_ERROR_REGISTER,
        (((DWORD)(0)) | ((DWORD)(0x50)))
    );

    LapicWrite(
        OFFSET_LVT_PERFORMANCE_MONITOR_COUNTER_REGISTER,
        0x2
    );

    LapicWrite(
        OFFSET_TASK_PRIORITY_REGISTER,
        0x0
    );

    // Set the apic base in the global information structure
    gCpuInformation.PerCpuData[_GetCpuId()].ApicBase = apicbase;
    return status;
}

OS_STATUS
SendIpi(
    BYTE Vector,
    DELIVERY_MODE IpiType,
    DESTINATION_MODE DestinationMode,
    DESTINATION_SHORTHAND DestinationShorthand,
    BYTE DestinationId,
    BOOL Synchronize
)
{
    INTERRUPT_COMMAND_REGISTER_STRUCTURE_32 icr32;
    INTERRUPT_COMMAND_REGISTER_STRUCTURE_64 icr64;
    QWORD interruptCommandRegisterx2apic;
    OS_STATUS status = OS_STATUS_SUCCESS;

    if(!SUCCESS(IsApicInitialized()))
    {
        LOG("Apic System Was Not Initialized On Cpu = %d\n", _GetCpuId());
        return OS_STATUS_NOT_INITIALIZED;
    }

    // Ensure the message has been sent before leaving the method
    icr32.Raw = LapicRead(OFFSET_INTERRUPT_COMMAND_REGISTER_32);
    while(Synchronize && ( icr32.FIELDS.DeliveryStatus != DELIVERY_STATUS_IDLE))
    {
        icr32.Raw = LapicRead(OFFSET_INTERRUPT_COMMAND_REGISTER_32);
        _CpuPause();
    };

    // It is possible to write the high 32 bits of the register directly
    // Because only writing the low 32 bits determines the ipi to be sent
    // As a result a icr32 shadow variable is nedded to prepare the message

    icr64.Raw = 0;
    if(DestinationShorthand == NO_SHORTHAND)
    {
        icr64.FIELDS.DestinationField = DestinationId;
    }

    icr32.Raw = 0;
    icr32.FIELDS.Vector = Vector;
    icr32.FIELDS.DeliveryMode = IpiType;
    icr32.FIELDS.DestinationMode = DestinationMode;
    icr32.FIELDS.Level = LEVEL_DEASSERT;
    icr32.FIELDS.TriggerMode = TRIGGER_MODE_EDGE;
    icr32.FIELDS.DestinationShorthand = DestinationShorthand;

    interruptCommandRegisterx2apic = ((QWORD)icr64.Raw << 32) | ((QWORD) icr32.Raw);
    LapicWrite(OFFSET_INTERRUPT_COMMAND_REGISTER_32, interruptCommandRegisterx2apic);

    // Ensure the message has been sent before leaving the method
    icr32.Raw = LapicRead(OFFSET_INTERRUPT_COMMAND_REGISTER_32);
    while(Synchronize && ( icr32.FIELDS.DeliveryStatus != DELIVERY_STATUS_IDLE))
    {
        icr32.Raw = LapicRead(OFFSET_INTERRUPT_COMMAND_REGISTER_32);
        _CpuPause();
    };

    return OS_STATUS_SUCCESS;
}

OS_STATUS
SetApicTimer(
    DWORD Time,
    TIMER_MODE TimerMode,
    BYTE Vector,
    DIVIDE_VALUE_OPTIONS DivideValue,
    BOOL Masked,
    BOOL Synchronize
)
{
    DIVIDE_CONFIGURATION_REGISTER dcr;
    INITIAL_COUNT_REGISTER icr;
    CURRENT_COUNT_REGISTER ccr;
    LVT_TIMER_REGISTER ltr;
    LVT_TIMER_REGISTER ltrShadow;
    OS_STATUS status = OS_STATUS_SUCCESS;

    if(!SUCCESS(IsApicInitialized()))
    {
        LOG("Apic System Was Not Initialized On Cpu = %d\n", _GetCpuId());
        return OS_STATUS_NOT_INITIALIZED;
    }

    if(TimerMode == TSC_DEADLINE)
    {
        return OS_STATUS_NOT_IMPLEMENTED;
    }  

    dcr.FIELDS.DivideValue = DivideValue;
    LapicWrite(OFFSET_DIVIDE_CONFIGURATION_REGISTER, dcr.Raw);

    ltr.Raw = LapicRead(OFFSET_LVTR_TIMER_REGISTER);
    while(Synchronize && ( ltr.FIELDS.DeliveryStatus != DELIVERY_STATUS_IDLE))
    {
        ltr.Raw = LapicRead(OFFSET_LVTR_TIMER_REGISTER);
        _CpuPause();
    };

    ltr.Raw = 0;
    ltr.FIELDS.Vector = Vector;
    ltr.FIELDS.TimerMode = TimerMode;
    ltr.FIELDS.Mask = Masked;
    LapicWrite(OFFSET_LVTR_TIMER_REGISTER, ltr.Raw);
    LapicWrite(OFFSET_INITIAL_COUNT_REGISTER, Time);

    return  OS_STATUS_SUCCESS;
}

OS_STATUS
StopApicTimer(
)
{
    // A write of 0 to the initial-count register effectively stops the local APIC timer, in both one-shot and periodic mode.
    LapicWrite(OFFSET_INITIAL_COUNT_REGISTER, LAPIC_TIMER_STOP_VALUE);
    return OS_STATUS_SUCCESS;
}

DWORD
ReadApicTimer(
)
{
    return LapicRead(OFFSET_CURRENT_COUNT_REGISTER);
}

DWORD
ReadApicError(
)
{
    LapicWrite(OFFSET_ERROR_STATUS_REGISTER, 0);
    return LapicRead(OFFSET_ERROR_STATUS_REGISTER);
}

OS_STATUS
SendEoiForLapic(
)
{
    LapicWrite(OFFSET_EOI_REGISTER, 0);
    return OS_STATUS_SUCCESS;
}