#include "vga.h"
#include "../tools/vprintf.h"
#include "../datastructures/synchronization/mutex.h"
DWORD gPositionLine = 0;
DWORD gPositionColumn = 0;

#define SCREEN_PTR ((WORD *)0xB8000)
#define MAKE_WRITING_VALUE(value, font_color, bg_color) (((bg_color << 4 | font_color )<<8) | ((char)value))

#define STD_BG_COLOR 0x0
#define STD_FG_COLOR 0x3

MUTEX gVgaMutex = {0}; // Ensure the vga writing is serialized
char outbuffer[MAX_STRING_SIZE];

void
IncreaseWritingPositionLine()
{
    gPositionLine++;
    if(gPositionLine >= 80)
    {
        gPositionLine = 0;
        gPositionColumn ++;
        if(gPositionColumn >= 25)
        {
            gPositionColumn = 0;
        }
    }
}

void
IncreaseWritingPositionColumn()
{
    gPositionLine = 0;
    gPositionColumn ++;
    if(gPositionColumn >= 25)
    {
        gPositionColumn = 0;
    }
}
void 
VgaPrint(
    COLOR_VALUES Background,
    COLOR_VALUES Foreground,
    char * Format,
    ...
)
{

    AcquireSpinMutex(&gVgaMutex);
    va_list args;
    int status = 0;
 
    va_start(args, Format);
    args = ((QWORD)args) & (~0x7);
    status = StrFormat(&Format, args, outbuffer);
    va_end(args);

    int len = KernStrlen(outbuffer);
    
    VgaWrite(
        outbuffer,
        Background,
        Foreground
    );
    ReleaseSpinMutex(&gVgaMutex);
    return;
}

void 
VgaClearScreen(

)
{
        AcquireSpinMutex(&gVgaMutex);

    gPositionColumn = 0;
    gPositionLine = 0;
    for(DWORD i=0; i < SCREEN_HEIGHT * SCREEN_WIDTH ; i++)
    {
        SCREEN_PTR[gPositionColumn * SCREEN_WIDTH + gPositionLine] =
            MAKE_WRITING_VALUE(0, STD_FG_COLOR, STD_BG_COLOR);
        IncreaseWritingPositionLine();
    }
    gPositionLine = 0;
    gPositionColumn = 0;
        ReleaseSpinMutex(&gVgaMutex);

}