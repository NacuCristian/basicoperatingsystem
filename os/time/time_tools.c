
#include "time_tools.h"
#include "../datastructures/synchronization/mutex.h"
#include "../devices/vga.h"

#define PIT_FREQUENCY 1193182 // Hz -> the smallest unit of time we can measure is 1/1193182 seconds ~= 1 microisecond 

#define PIT_CHANNEL0_PORT   0x40
#define PIT_CHANNEL1_PORT   0x41 // Useless but for the sake of it
#define PIT_CHANNEL2_PORT   0x42 // Useless but for the sake of it
#define PIT_COMMAND_PORT    0x43

#define PIT_CHANNEL0_SELECTOR 0x0
#define PIT_CHANNEL1_SELECTOR 0x1
#define PIT_CHANNEL2_SELECTOR 0x2
#define PIT_READBACK_SELECTOR 0x3

#define ACCESS_LATCH_COUNT        0x0
#define ACCESS_LOW_BYTE_ONLY      0x1
#define ACCESS_HIGH_BYTE_ONLY     0x2
#define ACCESS_LOW_BYTE_HIGH_BYTE 0x3

#define OPERATING_MODE_INTERRUPT_ON_TERMINAL    0x0
#define OPERATING_MODE_RETRIGGER_ONE_SHOT       0x1
#define OPERATING_MODE_RATE_GENERATOR           0x2
#define OPERATING_MODE_SQUARE_WAVE_GENERATOR    0x3
#define OPERATING_MODE_SOFT_STROBE              0x4
#define OPERATING_MODE_HARD_STROBE              0x5
#define OPERATING_MODE_RATE_GENERATOR2          0x6
#define OPERATING_MODE_SQUARE_WAVE_GENERATOR2   0x7

#define BCD_MODE 1
#define BINARY_MODE 1

MUTEX gInitTimerMutex = {0};
volatile BOOL gIsTimerFinished = FALSE;
BYTE gInterruptVectorForLapic;

#define MAKE_PIT_COMMAND(BcdOrBin, OperatingMode, AccessMode, Channel)\
    (((BYTE)0) | ((BYTE)BcdOrBin) | (((BYTE)OperatingMode) << 1) | (((BYTE)AccessMode) << 4) | (((BYTE)AccessMode) << 6))

OS_STATUS
LapicTimingCallback(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
)
{
    gIsTimerFinished = TRUE;
    return OS_STATUS_SUCCESS;
}

OS_STATUS
ConfigurePitTimer(
)
{
    _OutByte(
        // MAKE_PIT_COMMAND(BINARY_MODE, OPERATING_MODE_INTERRUPT_ON_TERMINAL,
        //     ACCESS_LOW_BYTE_HIGH_BYTE, PIT_CHANNEL0_SELECTOR), 
        0b00110000,
        PIT_COMMAND_PORT
        );
    return OS_STATUS_SUCCESS;
}

void
PitStartTimer(
    WORD TimeInMicroseconds
)
{
    _OutByte((1194 & 0xFF), PIT_CHANNEL0_PORT);
    _OutByte(((1194 >> 8) & 0xFF), PIT_CHANNEL0_PORT);
}

OS_STATUS
SetCallbacksForPit(
    InterruptServiceRoutine Isr,
    QWORD *RegistrationNumber
)
{
    return RegisterHandleForIrq(
        0,
        Isr,
        RegistrationNumber
    );
}


OS_STATUS
InitTimer(
    BYTE InterruptVectorForLapicTimer
)
{   
    QWORD crtApicTimer = 0;
    QWORD pitTime = 1000; // to reach seconds
    QWORD frequency = 0;
    QWORD regNumber = 0;

    AcquireSpinMutex(&gInitTimerMutex);

    gInterruptVectorForLapic = InterruptVectorForLapicTimer;
    gIsTimerFinished == FALSE;

    ConfigurePitTimer();
    
    SetCallbacksForPit(
        LapicTimingCallback,
        &regNumber
    );


    _Sti();

    SetApicTimer(
      MAX_DWORD,
      ONE_SHOT,
      gInterruptVectorForLapic,
      DIVIDE_1,
      FALSE,
      TRUE
    );
    PitStartTimer(0);

    
    while(gIsTimerFinished == FALSE);
    crtApicTimer = ReadApicTimer();
    StopApicTimer();    

    frequency = (MAX_DWORD - (QWORD)crtApicTimer) * (QWORD)pitTime;

    LOG(" \nFreq. of LAPIC timer on CPU %d is %llu Hz => %lld %s precision\n",  
            _GetCpuId(), 
            frequency,
            (frequency > ONE_SECOND_IN_NANO_SECONDS ? (((QWORD)ONE_SECOND_IN_PICO_SECONDS) / frequency) : (ONE_SECOND_IN_NANO_SECONDS / frequency)),  
            (frequency > ONE_SECOND_IN_NANO_SECONDS ? ("Pico Sec") : ("Nano Sec"))
        );

    UnregisterHandle(
        regNumber
    );

    ReleaseSpinMutex(&gInitTimerMutex);

    gCpuInformation.PerCpuData[_GetCpuId()].LapicFrequency = frequency;
    
    return OS_STATUS_SUCCESS;
    
}

OS_STATUS
TimeLapic(
    QWORD TimeInMicroseconds,
    TIMER_MODE TimerMode
)
{
    DIVIDE_VALUE_OPTIONS divider;
    BYTE prescalar;
    QWORD time;
    QWORD tics;
    QWORD frequency;
    OS_STATUS status = OS_STATUS_SUCCESS;

    if(gCpuInformation.PerCpuData[_GetCpuId()].LapicFrequency == NULL)
    {
        return OS_STATUS_NOT_INITIALIZED;
    }

    time = TimeInMicroseconds;
    prescalar = 0;
    
    while(time > MAX_DWORD && prescalar < 7)
    {
        time >> 2;
        prescalar ++;
    }

    if(time > MAX_DWORD)
    {
        return OS_STATUS_VALUE_TOO_LARGE;
    }

    if (prescalar == 0)
        divider = DIVIDE_1;
    else if (prescalar == 1)
        divider = DIVIDE_2;
    else if (prescalar == 2)
        divider = DIVIDE_4;
    else if (prescalar == 3)
        divider = DIVIDE_8;
    else if (prescalar == 4)
        divider = DIVIDE_16;
    else if (prescalar == 5)
        divider = DIVIDE_32;
    else if (prescalar == 6)
        divider = DIVIDE_64;
    else if (prescalar == 7)
        divider = DIVIDE_128;
    else
        return OS_STATUS_INVALID_VALUE;
    
    frequency = gCpuInformation.PerCpuData[_GetCpuId()].LapicFrequency;
    tics = ((time * frequency) / ONE_SECOND_IN_MILI_SECONDS);

    status = SetApicTimer(
        tics,
        TimerMode,
        gInterruptVectorForLapic,
        divider,
        NOT_MASKED,
        TRUE
    );

    return status;
}

OS_STATUS
SetCallbacksForLapicTimer(
    InterruptServiceRoutine Isr,
    QWORD *RegistrationNumber
)
{
    return RegisterHandleForInterruptVector(
        gInterruptVectorForLapic,
        Isr,
        RegistrationNumber
    );
}
