#include "interrupt_manager.h"
#include "../datastructures/generic/sll.h"
#include "../datastructures/synchronization/mutex.h"
#include "../memory/kern_heap.h"
#include "../devices/pic.h"
#include "../user_mode/task_scheaduler.h"

QWORD gIsrId = 0;
MUTEX mtxIsr = {0};
PSLL_HEADER gInterruptHandlers [255] = {0};

typedef struct _ISR_HANLDE_STRUCTURE
{
    InterruptServiceRoutine Isr;
    QWORD IsrHandleId;
} ISR_HANLDE_STRUCTURE;

COMPARE_FUNCTION_STATUS
CompareIsrHandleStructure(
    PVOID * Elem1,
    PVOID * Elem2
)
{
    ISR_HANLDE_STRUCTURE *handleStruct1;
    ISR_HANLDE_STRUCTURE *handleStruct2;

    handleStruct1 = Elem1;
    handleStruct2 = Elem2;

    if(handleStruct1->IsrHandleId == handleStruct2->IsrHandleId)
    {
        return COMPARE_FUNCTION_STATUS_EQUAL;
    }

    return COMPARE_FUNCTION_STATUS_GREATER;
}

#define ID_NUMBER_MASK 0xFFFFFFFFFFFFFF
#define GET_VECTOR_FROM_ID(Id) ((BYTE)(((QWORD)Id >> 56) & BYTE_MASK))
QWORD 
MakeId(
    BYTE InterruptVector
)
{
    QWORD id;
    AcquireSpinMutex(&mtxIsr);
        id = ((gIsrId & ID_NUMBER_MASK) | ((QWORD)(((QWORD)InterruptVector) << ((QWORD)56))));
        gIsrId++;
    ReleaseSpinMutex(&mtxIsr);
    return id;
}

OS_STATUS
RegisterHandleForInterruptVector(
    BYTE InterruptVector,
    InterruptServiceRoutine Isr,
    QWORD *RegistrationNumber
)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    ISR_HANLDE_STRUCTURE *newIsrHandler;

    if (gInterruptHandlers[InterruptVector] == NULL)
    {
        SLL_HEADER *sllHandler = NULL;
        status = SllInit(
            LIST_SLL_SYNCHRONIZED,
            CompareIsrHandleStructure,
            &sllHandler
        );
        if(!SUCCESS(status) || sllHandler == NULL)
        {
            LOG("Error at Init Sll\n");
            return status;
        }

        gInterruptHandlers[InterruptVector] = sllHandler;
    }

    newIsrHandler = HeapAlloc(NULL, sizeof(ISR_HANLDE_STRUCTURE));
    if(newIsrHandler == NULL)
    {
        LOG("Error HeapAlloc\n");
        return OS_STATUS_NULL_POINTER;
    }

    newIsrHandler->Isr = Isr;
    newIsrHandler->IsrHandleId = MakeId(InterruptVector);
    *RegistrationNumber = newIsrHandler->IsrHandleId;
    

        status = SllInsertElementFront(
            gInterruptHandlers[InterruptVector],
            newIsrHandler
        );
        if(!SUCCESS(status))
        {
            LOG("SllInsertElementFront failed\n");
        }

    return status;
}

OS_STATUS
RegisterHandleForIrq(
    BYTE IrqNumber,
    InterruptServiceRoutine Isr,
    QWORD *RegistrationNumber
)
{
    BYTE interruptVectorNuber = INVALID_IRQ;
    GetInterruptVectorFromIrq(
        IrqNumber,
        &interruptVectorNuber
    );
    return RegisterHandleForInterruptVector(
        interruptVectorNuber,
        Isr,
        RegistrationNumber
    );

}

OS_STATUS
UnregisterHandle(
    QWORD RegistrationNumber
)
{
    ISR_HANLDE_STRUCTURE isrHandler;
    ISR_HANLDE_STRUCTURE *outputIsrHandler;
    BYTE vector;
    OS_STATUS status = OS_STATUS_SUCCESS;

    vector = GET_VECTOR_FROM_ID(RegistrationNumber);
    isrHandler.IsrHandleId = RegistrationNumber;
    
    _Cli();

    outputIsrHandler = SllDeleteElement(
        gInterruptHandlers[vector], 
        &isrHandler
        );
    if(outputIsrHandler == NULL || outputIsrHandler->IsrHandleId != RegistrationNumber)
    {
        LOG("Something is not ok with the SLL\n");
        status = OS_STATUS_INVALID_VALUE;
        goto cleanup;
    }

    // TODO: delete the SLL too?

    status = HeapFree(outputIsrHandler);
    if(!SUCCESS(status))
    {
        LOG("ERROR At Heap Free\n");
    }

cleanup:
    _Sti();
    return status;
}


OS_STATUS
HandleUserInterrupt(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
)
{
    SLL_HEADER *isrs;
    GENERIC_SINGLE_LINKED_NODE *crtNode;
    INTERRUPT_SERVICE_ROUTINE_DATA isrRoutineData;
    ISR_HANLDE_STRUCTURE *isrData;
    OS_STATUS status;

    isrs = gInterruptHandlers[Data->InterruptVector];
    if(isrs == NULL)
    {
        return OS_STATUS_SUCCESS;
    }
    if(SllIsEmpty(isrs))
    {
        return OS_STATUS_SUCCESS;
    }
    
    // While handling interrupts switch the context
    // if(Data->InterruptVector == 0x29)
    // ContextSave(
    //     Data->Context
    // );

    crtNode = isrs->Element;

    while (crtNode != NULL)
    {
        isrData = crtNode->Data;
        status = isrData->Isr(Data);
        if(!SUCCESS(status))
        {
            LOG("ISR Id %lld for vector %d failed with status %d\n", isrData->IsrHandleId, Data->InterruptVector, status);
        }

        crtNode = crtNode->Next;
    }

    // // If the task on the current kernel is saved, go to main cycle
    // if(Data->InterruptVector == 0x29)
    // if(gCpuInformation.PerCpuData[_GetCpuId()].SwitchedTask == TRUE)
    // {
    //     return KernelMainCycle(Data->Context);
    // }
    return OS_STATUS_SUCCESS;
}

OS_STATUS
HandleException(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
)
{
    // TODO TODO TODOTODOTODOTODOTODOOOOO
    return OS_STATUS_NOT_IMPLEMENTED;
}
