;; Contains asm tools needed at OS runtime

[bits 64]

global _GetRip
global _OutByte
global _OutWord
global _InByte
global _InWord
global _InvalidateTlb
global _Break
global _ReadMsr
global _WriteMsr
global _Lidt
global _Lgdt
global _Ltr
global _GetCr0
global _GetCr3
global _SetCr3
global _GetCpuId
global _GetRsp
global _SetCodeSelector
global _CpuPause
global _Sti

global _Cli
;; PVOID _GetRip()
;; Returns curent Rip
_GetRip:
    pop     rax
    push    rax
    ret

;; void _OutByte(BYTE Data, WORD Port)
;; Writes a byte of data to the specified port
_OutByte:
    mov     rax, rcx
    out     dx, al
    ret

;; void _OutWord(WORD Data, WORD Port)
;; Writes a word of data to the specified port
_OutWord:
    mov     rax, rcx
    out     dx, ax
    ret

;; BYTE _InByte(WORD Port)
;; Reads a byte from the specified port
_InByte:
    mov     rax, 0
    mov     rdx, rcx
    in      al, dx
    ret

;; WORD _InWord(WORD Port)
;; Reads a word from the specified port
_InWord:
    mov     rax, 0
    mov     dx, cx
    in      ax, dx
    ret

;; void _InvalidateTlb()
;; Invalidates the TLB
_InvalidateTlb:
    mov     rax, cr3
    mov     cr3, rax
    mov     rax, 0
    ret

;; void _Break()
;; Breaks the code execution
_Break:
    cli
    hlt

;; QWORD _ReadMsr(QWORD MsrAddress)
;; Returns the value of the specified MSR
_ReadMsr:
    push    rdx
    rdmsr
    shl     rdx, 32
    or      rax, rdx
    pop     rdx
    ret

;; void _WriteMsr(QWORD MsrAddress, QWORD Value)
;; Writes the value to the specified MSR
_WriteMsr:
    push    rax
    mov     rax, 0
    mov     eax, edx
    mov     edx, 0
    wrmsr
    pop     rax
    ret

;; void _Lidt(PVOID IdtAddress)
;; Loads the IDT address
_Lidt:
    lidt    [rcx]
    ret

;; void    _Lgdt(PVOID GdtAddress)
;; Sets the GDT
_Lgdt:
    lgdt    [rcx]
    ret

;; void    _Ltr(WORD SegmentOffset)
;; Sets the GDT
_Ltr:
    ltr    cx
    ret
     
;; QWORD _GetCr3()
;; Returns CR3 value on the current cpu
_GetCr3:
    mov     rax, cr3
    ret

;; QWORD _GetCr0()
;; Returns CR0 value on the current cpu
_GetCr0:
    mov     rax, cr0
    ret
;; QWORD _GetCS()
;; Returns CR0 value on the current cpu
global _GetCs 
_GetCs:
    xor     rax, rax
    mov     ax, cs
    ret
;; QWORD _GetSS()
;; Returns CR0 value on the current cpu
global _GetSs
_GetSs:
    xor     rax, rax
    mov     ax, ss
    ret

;; void _SetCr3(QWORD Cr3)
;; Sets the CR3 value of the current cpu
_SetCr3:
    mov     cr3, rcx
    ret

;; QWORD _GetCpuId()
;; Returns the current cpu ID
_GetCpuId:
    push    rbx
    mov     rbx, 0
    mov     rax, 1
    cpuid
    shr     rbx, 24
    and     rbx, 0xFF
    mov     rax, rbx
    pop     rbx
    ret

;; QWORD _GetRsp()
;; Returns the current Rsp
_GetRsp:
    mov     rax, rsp
    ret

extern _SetRsp
;; void _SetRsp(QWORD Rsp)
;; Sets the current Rsp
_SetRsp:
    mov     rsp, rcx
    ret

;; void _SetCodeSelector(WORD Selector)
_SetCodeSelector:
    push    cx
    push    .changeCs
    retf
    .changeCs:
    ret

;; void _SetCodeSelector(WORD Selector)
_Sti:
    sti
    ret

;; void _SetCodeSelector(WORD Selector)
_Cli:
    cli
    ret

;; void _CpuPause()
;; Hints processor we're spinning
_CpuPause:
    pause
    ret

global _Rdtsc
;;  QWORD _Rdtsc()
_Rdtsc:
    rdtsc
    shl     rdx, 32
    add     rax, rdx
    ret

global _IoWait
;;  void _IoWait()
_IoWait:
    jmp .et1
    .et1:
    jmp .et2
    .et2:
    ret

global _CpuId
_CpuId:
    mov     rax, 1
    cpuid
    mov     rax, rcx
    ret

;;  QWORD _GetRflags()
;;  Returns rflags
global _GetRflags
_GetRflags:
    pushf
    pop     rax
    ret

;;  int _PerformSyscall(DWORD Command, PVOID * Args);
global _PerformSyscall
_PerformSyscall:
    int     33
    ret