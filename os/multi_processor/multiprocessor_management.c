#include "multiprocessor_management.h"
#include "../acpi/acpi.h"
#include "../memory/kern_heap.h"
#include "../memory/kern_pagepool.h"
#include "../memory/virtual_memory_manager.h"
#include "../devices/lapic.h"
#include "../tools/gdt_tools.h"
#include "../user_mode/syscall.h"
#include "../devices/pic.h"
#include "../interrupts/interrupt_manager.h"
#include "../time/time_tools.h"
#include "../user_mode/task_scheaduler.h"
#include "../user_mode/task_loader.h"
#include "../driver/vga_driver.h"
#include "../devices/pcie.h"
#include "../user_mode/demo_processes/um_initial_process.h"
#include "../user_mode/demo_processes/um_dll.h"

extern void _Init16Ap();

// Find the number of APs on the current machine
OS_STATUS
MultiprocessorFindNumberOfAps(
)
{
    ACPI_STATUS acpiStatus = AE_OK;
    ACPI_TABLE_MADT *madt;
    DWORD apNumber = 0;
    
    // Initialize Acpi Tables
    acpiStatus = AcpiInitializeTables(
        gAcpiTableArray,
        ACPI_MAX_INIT_TABLES,
        FALSE
    );
    if(acpiStatus != AE_OK)
    {
        const char * statusString = AcpiFormatException(acpiStatus);
        LOG("Error AcpiInitializeTables %s\n", statusString);
    }

    // Read MADT Tables in order to obtain the number of CPUs
    acpiStatus = AcpiGetTable(
        ACPI_SIG_MADT,
        0,
        (ACPI_TABLE_HEADER **)&madt
    );
    if(acpiStatus != AE_OK)
    {
        const char * statusString = AcpiFormatException(acpiStatus);
        //LOG("Error AcpiInitializeTables %s\n", statusString);
    }

    // Find number of CPUs
    for(QWORD addr = (QWORD)madt + sizeof(ACPI_TABLE_MADT); addr < (((QWORD)madt) + madt->Header.Length);)
    {
        ACPI_SUBTABLE_HEADER *madtObject = (ACPI_SUBTABLE_HEADER *)addr;
        if(madtObject->Type == ACPI_MADT_TYPE_LOCAL_APIC)
        {
            ACPI_MADT_LOCAL_APIC *lapicObject = (ACPI_MADT_LOCAL_APIC *)addr;    
            //LOG("Found LAPIC with proc ID = %d\n", lapicObject->Id);
            apNumber++;
        }
        addr += madtObject->Length;
    }
    gCpuInformation.SHARED_CPU_INFORMATION.TotalCpuNumber = apNumber;
    return OS_STATUS_SUCCESS;
}


OS_STATUS
TimerDriver(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
)
{
    SendEoiForLapic();
    ContextSave(Data->Context);
    return KernelMainCycle(Data->Context);
}

OS_STATUS
MultiprocessorInitiStructures(

)
{
    OS_STATUS status;

    // Alocate PerCpuData
    gCpuInformation.PerCpuData = HeapAlloc(NULL, sizeof(CPU_INFORMATION) * 
        gCpuInformation.SHARED_CPU_INFORMATION.TotalCpuNumber);
    if(gCpuInformation.PerCpuData == NULL)
    {
        LOG("HeapAlloc Failed\n");
        BREAKPOINT;
    }

    for(int i=0; i<=gCpuInformation.SHARED_CPU_INFORMATION.TotalCpuNumber; i++)
    {
        // Each stack is 1 Page long because of the page pool error, but it will be fixed no worries
        // Remember the stack decreases, so we have to round up the address
        gCpuInformation.PerCpuData[i].StackAddress = (PVOID)((QWORD)PagePoolAlloc(NULL, PAGE_SIZE_4K) + PAGE_SIZE_4K - sizeof(QWORD));
        if(gCpuInformation.PerCpuData[i].StackAddress == NULL)
        {
            LOG("PagePool Alloc Error\n");
            BREAKPOINT;
        }
        gCpuInformation.PerCpuData[i].StackAddress = (PVOID)((QWORD)gCpuInformation.PerCpuData[i].StackAddress + PAGE_SIZE_4K - sizeof(QWORD));
        
        // Create a TSS page for each CPU, 1 page is more than enough maybe define on heap?
        gCpuInformation.PerCpuData[i].TssAddress = PagePoolAlloc(NULL, PAGE_SIZE_4K);
         if(gCpuInformation.PerCpuData[i].TssAddress == NULL)
        {
            LOG("PagePool Alloc Error\n");
            BREAKPOINT;
        }

        // Alocate the context switch area
        // Again maybe it should be less than a page but w/ever
        gCpuInformation.PerCpuData[i].ContextSwitchSpace = PagePoolAlloc(NULL, PAGE_SIZE_4K);
         if(gCpuInformation.PerCpuData[i].ContextSwitchSpace == NULL)
        {
            LOG("PagePool Alloc Error\n");
            BREAKPOINT;
        }

        // Set in the TSS the alocated stack as ring 0 stack 
        TASK_STATE_SEGMENT_64 * tssdesc = gCpuInformation.PerCpuData[i].TssAddress;
        tssdesc->Rsp0 = (QWORD)gCpuInformation.PerCpuData[i].StackAddress;

        // Mark that the apic system is not initialized
        gCpuInformation.PerCpuData[i].ApicBase = ADDRESS_NOT_CONFIGURED;

        // Mark that the TSS selector is not set
        gCpuInformation.PerCpuData[i].TssSelector = INVALID_SELECTOR;

        // Mark that the CPU is in kernel
        gCpuInformation.PerCpuData[i].InKernel = TRUE;
    
        // Mark that the CPU curent task has been switched (has no task to save)
        gCpuInformation.PerCpuData[i].SwitchedTask = TRUE;

        // Load the newly created TSS structure's descriptor in the GDT

            // Step 1, set the selecor offset to the begining of the new TSS descriptor
        gCpuInformation.PerCpuData[i].TssSelector = 
                GetCurrentGdtOffset(gCpuInformation.SHARED_CPU_INFORMATION.Gdt);

            // Step 2, create the 16 byte tss descriptor
        QWORD tss = (QWORD)gCpuInformation.PerCpuData[i].TssAddress;
        QWORD tssDescriptor[2];
        // LOG("Tss = %llX\n", tss);
        tssDescriptor[0] =  0x90890000000067 | ((tss & 0x0FFFF) << 16) |  (((tss & 0x0FF0000) >> 16) << 32) | (((tss & 0x0FF000000) >> 24) << 56);
        tssDescriptor[1] = ((tss >> 32) & DWORD_MASK);

            // Step 3, load the tss descriptor in the gdt
        status = RegisterGdtDescriptor(
            gCpuInformation.SHARED_CPU_INFORMATION.Gdt,
            tssDescriptor,
            sizeof(tssDescriptor) / sizeof(QWORD)
        );

        // QWORD * desc = (QWORD *)((QWORD)(((GDT_INFO_64 *)gCpuInformation.SHARED_CPU_INFORMATION.Gdt)->TableStart) + gCpuInformation.PerCpuData[i].TssSelector);
        // LOG("desc %x = low %llX high %llX\n", gCpuInformation.PerCpuData[i].TssSelector, desc[0], desc[1] );
        
        if(!SUCCESS(status))
        {
            LOG("Error populating\n");
            return status;
        }

        // Set the CpuId to signal the completion of the data field
        gCpuInformation.PerCpuData[i].CpuId = i;
    }
    return OS_STATUS_SUCCESS;
}

BOOL gNext = FALSE;

OS_STATUS 
NextHandle(INTERRUPT_SERVICE_ROUTINE_DATA *Data)
{
    gNext = TRUE;
    return OS_STATUS_SUCCESS;
}

OS_STATUS
InitMultiProcessor(

)
{
    MultiprocessorFindNumberOfAps();
    MultiprocessorInitiStructures();
    LOG(" \nSuccesfully created per cpu structures that hold context informations\n");
    //!! DO NOT LOAD GDT BEFORE ADDING ALL TSS DESCRIPTORS!!!
    _Lgdt(gCpuInformation.SHARED_CPU_INFORMATION.Gdt);
    _Ltr(gCpuInformation.PerCpuData[_GetCpuId()].TssSelector);
    
    InitSyscall(
        gCpuInformation.PerCpuData[_GetCpuId()].StackAddress
    );

    //InitPcieDevices();
    LOG(" \nLoaded new GDT and TR for BSP\n");

    QWORD RegNumber;
    SetCallbacksForLapicTimer(
       TimerDriver,
       &RegNumber
    );  
    
    InitTimer(0x46);
    LOG(" \nPreparing to start APs\n");

    LOG("-- Start APs at %llX --\n", _Init16Ap);   
    BYTE sipiRoutineAddress = ((((QWORD)_Init16Ap) >> 12) & 0xFF );
    // Send to the APs the INIT, SIPI, SIPI sequence in order to start them
    SendIpi(0, INIT, PHYSICAL, ALL_EXCLUDING_SELF, 0, TRUE);
    SendIpi(sipiRoutineAddress, START_UP, PHYSICAL, ALL_EXCLUDING_SELF, 0, TRUE);
    SendIpi(sipiRoutineAddress, START_UP, PHYSICAL, ALL_EXCLUDING_SELF, 0, TRUE);
    gCpuInformation.SHARED_CPU_INFORMATION.InitializedCpus++;
    // Should have some kind of timer here ...
    while(gCpuInformation.SHARED_CPU_INFORMATION.InitializedCpus 
            != gCpuInformation.SHARED_CPU_INFORMATION.TotalCpuNumber)
    {
        _CpuPause();
    }

    LOG(" \nAll APs finished configuration.\nInserting the first process in queue and start processing it\n");

    PVOID newStack = PagePoolAlloc(NULL, PAGE_SIZE_4K);
    PagePoolInit(gCpuInformation.SHARED_CPU_INFORMATION.KernelSpaceStart + 40*MEGA, 20 * MEGA);
    SetPagingArea(gCpuInformation.SHARED_CPU_INFORMATION.KernelSpaceStart + 40*MEGA);

    LOG(" \nBSP Loading Initial Task\n");
    LoadTask(
       gCpuInformation.SHARED_CPU_INFORMATION.KernelCr3,
       newStack,
       InitialProcess
    );
    
    gCpuInformation.SHARED_CPU_INFORMATION.ExecutionStatus = USERMODE_STARTED;
    TimeLapic(ONE_SECOND_IN_MILI_SECONDS * 10, ONE_SHOT);
    BREAKPOINT;

    LOG("Shouldnt reach this part, EVER\n");
}
