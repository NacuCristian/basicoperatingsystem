#include "um_dll.h"
#include "../../utils/os_intrinsics.h"
#include "../../devices/lapic.h"
#include "../syscall_defs.h"

extern int _PerformSyscall(DWORD Command, PVOID * Args);


int
GetPid()
{
    return _PerformSyscall(GETPID_REQ, NULL);
}

int
Exit()
{
    return _PerformSyscall(EXIT_REQ, NULL);
}


int
Fork()
{
    return _PerformSyscall(FORK_REQ, NULL);
}

int
Read(char *Buffer, unsigned int Count)
{
    int returnStatus;
    KERNEL_BUFFER_COMMAND args;
    args.Buffer = (BYTE *)Buffer;
    args.Count = (DWORD)Count;
    returnStatus = _PerformSyscall(READ_REQ, &args);
    return returnStatus;
}

int
Write(char *Buffer)
{
    int returnStatus;
    KERNEL_BUFFER_COMMAND args;
    args.Buffer = (BYTE *)Buffer;
    args.Count = 0;
    returnStatus = _PerformSyscall(WRITE_REQ, &args);
    return returnStatus;
}