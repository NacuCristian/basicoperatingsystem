#include "virtual_memory_manager.h"
#include "../devices/vga.h"

#define INDEX_MASK 0xFF80000000000000
#pragma pack(1)
typedef struct _PAGING_TABLE_ 
{
    PVOID * PagnigTableEntry[1];
}PAGING_TABLE;
#pragma pack()

GET_MEMORY_PAGE gGetMemoryPage;  
FREE_MEMORY_PAGE gFreeMemoryPage;
PVOID gLastPage;

// Declare the placeholder function for allocating memory pages
OS_STATUS
VirtMemMngrMapOnePage(
    PVOID* Cr3,
    QWORD PhysicalAddress,
    QWORD VirtualAddress,
    CACHING_TYPE CachingType,
    BOOL MapIn2MegaPages
);

OS_STATUS
PlaceholderAllocator(
    PVOID * MemoryPageAddress
)
{
    *MemoryPageAddress = gLastPage;
    gLastPage = (PVOID)(((QWORD)gLastPage) + PAGE_SIZE_4K);
    return OS_STATUS_SUCCESS;
}


OS_STATUS 
VirtMemMngrInit(
    PVOID LastPage
)
{
    gLastPage = LastPage;
    gGetMemoryPage = &PlaceholderAllocator;
    gFreeMemoryPage = NULL;
    return OS_STATUS_SUCCESS;
}

OS_STATUS
VirtMemMngrRegisterAllocatonMethod(
    GET_MEMORY_PAGE GetMemoryPage 
)
{
    gGetMemoryPage = GetMemoryPage;
}

OS_STATUS
VirtMemMngrRegisterFreeMethod(
    FREE_MEMORY_PAGE FreeMemoryPage
)
{
    gFreeMemoryPage = FreeMemoryPage;
}

OS_STATUS 
VirtMemMngrMapVaToPa(
    PVOID* Cr3,
    QWORD PhysicalAddress,
    QWORD VirtualAddress,
    QWORD Size,
    CACHING_TYPE CacheType,
    BOOL MapIn2MegaPages
)
{      

    OS_STATUS status = OS_STATUS_SUCCESS;

    if (Cr3 == NULL)
    {
        return OS_STATUS_INVALID_PARAMETER_1;
    }

    if (Size == 0)
    {
        return OS_STATUS_INVALID_PARAMETER_4;
    }

    if (*Cr3 == NULL)
    {
        PVOID newCr3 = NULL;
        status = gGetMemoryPage(
            &newCr3
        );
        if (!SUCCESS(status) )
        {
            LOG("Error gGetMemoryPage");
            return status;
        }
        
        if (newCr3 == NULL)
        {
            LOG("Error gGetMemoryPage Null Ptr");
            return OS_STATUS_NULL_POINTER;
        }

        *Cr3 = newCr3;
    }

    QWORD newPhysicalAddress = PhysicalAddress;
    QWORD newVirtualAddress = VirtualAddress;
    QWORD quantum = PAGE_SIZE_4K;

    if(!MapIn2MegaPages)
    {
        if(newPhysicalAddress | PAGE_MASK_4K != 0)
        {
            Size += PAGE_SIZE_4K;
        }
        newPhysicalAddress &= PAGE_MASK_4K;
        newVirtualAddress &= PAGE_MASK_4K;
    }
    else
    {
        quantum = 2*MEGA;
        if(newPhysicalAddress | (2*MEGA) != 0)
        {
            Size += 2*MEGA;
        }
        newPhysicalAddress &= (~(2*MEGA - 1));
        newVirtualAddress &= (~(2*MEGA - 1));
    }

    for (QWORD range = 0; range <= Size; range += quantum)
    {
        VirtMemMngrMapOnePage(
            Cr3,
            newPhysicalAddress,
            newVirtualAddress,
            CacheType,
            MapIn2MegaPages
        );
        newPhysicalAddress += quantum;
        newVirtualAddress += quantum;
    }
return status;
}

OS_STATUS
VirtMemMngrGetPaForVa(
    PVOID* Cr3,
    QWORD VirtualAddress,
    QWORD *PhysicalAddress
)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    QWORD index = 0;
    PAGING_TABLE *auxCrtPagingTable;
    PAGING_TABLE *crtPagingTable = (PAGING_TABLE *)(*Cr3);
    
    //remove unused 16 bits
    QWORD processedAddress = VirtualAddress & PAGE_MASK_4K;
    processedAddress <<= 16;
    for (int crtLevel = 0; crtLevel < 3; crtLevel++)
    {

        //get the 9 bits of the index
        index = processedAddress;
        index &= INDEX_MASK; 
        index >>= 55;

        //if there's no mapped page, map one
            
        if (crtPagingTable->PagnigTableEntry[index] == NULL)
        {            
           return OS_STATUS_NULL_POINTER;
        }

        crtPagingTable = (PAGING_TABLE *)(((QWORD)(crtPagingTable->PagnigTableEntry[index])) & PAGE_MASK_4K);   
        processedAddress <<= 9;
       
    }

    // in the PTE page, add new memory area
    index = processedAddress;
    index &= INDEX_MASK; 
    index >>= 55;
    PhysicalAddress = (QWORD *)(((QWORD)crtPagingTable->PagnigTableEntry[index]) & PAGE_MASK_4K);
    return OS_STATUS_SUCCESS;
}

OS_STATUS
VirtMemMngrMapOnePage(
    PVOID* Cr3,
    QWORD PhysicalAddress,
    QWORD VirtualAddress,
    CACHING_TYPE CachingType,
    BOOL MapIn2MegaPages
)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    QWORD index = 0;
    PAGING_TABLE *auxCrtPagingTable;
    PAGING_TABLE *crtPagingTable = (PAGING_TABLE *)(*Cr3);

    //remove unused 16 bits
    QWORD processedAddress = VirtualAddress & PAGE_MASK_4K;
    processedAddress <<= 16;
    for (int crtLevel = 0; crtLevel < 3; crtLevel++)
    {
        //get the 9 bits of the index
        index = processedAddress;
        index &= INDEX_MASK; 
        index >>= 55;
        
        //if there's no mapped page, map one
        if (crtPagingTable->PagnigTableEntry[index] == NULL)
        {            
            PVOID newAddress = NULL;
            status = gGetMemoryPage(
                &newAddress
            );
            if (!SUCCESS(status))
            {
                LOG("Error gGetMemoryPage\n");
                return status;
            }
            if (newAddress == NULL)
            {
                LOG("Error gGetMemoryPage Null Ptr\n");
                return OS_STATUS_NULL_POINTER;
            }

            crtPagingTable->PagnigTableEntry[index] = (PVOID)((((QWORD)newAddress) & PAGE_MASK_4K) | 0x7);
        }

        crtPagingTable = (PAGING_TABLE *)(((QWORD)(crtPagingTable->PagnigTableEntry[index])) & PAGE_MASK_4K);

        processedAddress <<= 9;

        if(MapIn2MegaPages && crtLevel == 1)
        {
            break;
        }
    }

    // in the PTE page, add new memory area
    index = processedAddress;
    index &= INDEX_MASK; 
    index >>= 55;
    if(!MapIn2MegaPages)
    {
        crtPagingTable->PagnigTableEntry[index] = (PVOID)((PhysicalAddress & PAGE_MASK_4K)  | (CachingType << 3) | 0x7);
    }
    else
    {
        crtPagingTable->PagnigTableEntry[index] = (PVOID)((PhysicalAddress & (~(2*MEGA - 1)))  | (CachingType << 3) | 0x7 | BIT(7));
    }
    
    return OS_STATUS_SUCCESS;
}


OS_STATUS 
VirtMemMngrUnmapVaToPa(
    PVOID Cr3,
    QWORD VirtualAddress
)

{
    //to be implemented
    return OS_STATUS_SUCCESS;
}