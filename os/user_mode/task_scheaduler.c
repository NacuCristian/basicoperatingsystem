#include "task_scheaduler.h"
#include "../memory/kern_heap.h"
#include "../memory/kern_pagepool.h"
#include "../datastructures/generic/queue.h"
#include "../datastructures/synchronization/mutex.h"
#include "../time/time_tools.h"

OS_STATUS
InitThreadpool(
)
{
    OS_STATUS status;
    status = QueueInit(
        QUEUE_SYNCHRONIZED,
        &gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobaQueue
    );
    if(!SUCCESS(status))
    {
        LOG("Error allocating queue %llX\n", status);
        return status;
    }
    if(gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobaQueue == NULL)
    {
        LOG("Error allocating queue 2 -- %llX\n", status);
        return status;
    }
    
    return OS_STATUS_SUCCESS;
}

OS_STATUS
ContextSave(
    INTERRUPT_REGISTER_STUCTURE *CurrentContext
)
{
    if(gCpuInformation.PerCpuData[_GetCpuId()].SwitchedTask == TRUE)
    {
        return OS_STATUS_CPU_IN_KERNEL;
    }
    _Cli();
    INTERRUPT_REGISTER_STUCTURE *newContext;

    newContext = HeapAlloc(NULL, sizeof(INTERRUPT_REGISTER_STUCTURE));
    if(newContext == NULL)
    {
        LOG("Error allocating\n");
        return OS_STATUS_NULL_POINTER;
    }


    KernMemcpy(
        CurrentContext,
        newContext,
        sizeof(INTERRUPT_REGISTER_STUCTURE)
    );

    AcquireSpinMutex(&gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobalMutex);
    
    QueueEnqueue(
        gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobaQueue,
        (PVOID)newContext
    );
    ReleaseSpinMutex(&gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobalMutex);

    // Mark the CPU as being in kernel
    gCpuInformation.PerCpuData[_GetCpuId()].SwitchedTask = TRUE;
    _Sti();
    return OS_STATUS_SUCCESS;
}

OS_STATUS
ContextLoad(
    INTERRUPT_REGISTER_STUCTURE *NewContext,
    INTERRUPT_REGISTER_STUCTURE *CurrentContext
)
{
    if (gCpuInformation.PerCpuData[_GetCpuId()].SwitchedTask == FALSE)
    {
        LOG("Cannot switch the task if has an unsaved task\n");
        return OS_STATUS_CPU_IN_KERNEL;
    }
    KernMemcpy(
        NewContext,
        CurrentContext,
        sizeof(INTERRUPT_REGISTER_STUCTURE)
    );
    HeapFree(NewContext);
    gCpuInformation.PerCpuData[_GetCpuId()].SwitchedTask = FALSE;
    return OS_STATUS_SUCCESS;
}

OS_STATUS
KernelMainCycle(
    INTERRUPT_REGISTER_STUCTURE *Registers
)
{
    INTERRUPT_REGISTER_STUCTURE *context = NULL;
    _Sti();
    while(1==1)
    {   
        // Although the queue is locked we might need to lock some operations regarding the queue like is the queue empty 
        AcquireSpinMutex(&gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobalMutex);
        if(!QueueIsEmpty(gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobaQueue))
        {
            context = (INTERRUPT_REGISTER_STUCTURE *)QueueDequeue(gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobaQueue);
            if(context == NULL)
            {
                LOG("FAILED CONTEXT!\n");
                ReleaseSpinMutex(&gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobalMutex);
                continue;
            }
            ReleaseSpinMutex(&gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobalMutex);

            
            LOG("[CPU %llX] LOADS TASK PID - %llX\n", _GetCpuId(), *(QWORD *)((context->Rsp&PAGE_MASK_4K) + PAGE_SIZE_4K - sizeof(QWORD)));
            ContextLoad(context, Registers);
            gCpuInformation.PerCpuData[_GetCpuId()].InKernel = FALSE; 
            TimeLapic(ONE_SECOND_IN_MILI_SECONDS * 120, ONE_SHOT);
            return OS_STATUS_SUCCESS;
        }
        else
        {
            ReleaseSpinMutex(&gCpuInformation.SHARED_CPU_INFORMATION.ThreadpoolGlobalMutex);
        }
    }
}
