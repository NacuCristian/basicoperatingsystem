#ifndef _QUEUE_H_
#define _QUEUE_H_

#include "../../utils/os_common.h"
#include "sll.h"

#define QUEUE_HEADER SLL_HEADER

#define QUEUE_TYPE               BYTE
#define QUEUE_SYNCHRONIZED       ((QUEUE_TYPE) 1)
#define QUEUE_NOT_SYNCHRONIZED   ((QUEUE_TYPE) 2)

OS_STATUS
QueueInit(
    QUEUE_TYPE QueueType,
    QUEUE_HEADER **Queue
);

OS_STATUS
QueueEnqueue(
    QUEUE_HEADER *Queue,
    PVOID *Element
);

PVOID
QueueDequeue(
    QUEUE_HEADER *Queue
);

BOOL
QueueIsEmpty(
    QUEUE_HEADER *Queue
);

#endif