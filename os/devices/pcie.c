#include "pcie.h"
#include "../acpi/acpi.h"
#include "../memory/virtual_memory_manager.h"
#include "../memory/kern_pagepool.h"

#pragma pack(1)
typedef struct _ACPI_PCIE_CONFIG_SPACE_DESCRIPTOR
{
    QWORD BaseAddress;
    WORD PciSegmentGroupNumber;
    BYTE StartBusNumber;
    BYTE EndBusNumber;
    DWORD Reserved;
} ACPI_PCIE_CONFIG_SPACE_DESCRIPTOR;
#pragma pack()

#pragma pack(1)
typedef struct _PCIE_CONFIG_SPACE
{
    WORD VendorId;
    WORD DeviceId;
    WORD Command;
    WORD Status;
    BYTE RevId;
    BYTE ProgIf;
    BYTE Subclass;
    BYTE Class;
    BYTE CacheLineS;
    BYTE LatencyTimer;
    BYTE HeaderType;
    BYTE BIST;
} PCIE_CONFIG_SPACE;
#pragma pack()

#pragma pack(1)
typedef struct _PCIE_CONFIG_SPACE_TYPE_0
{
    PCIE_CONFIG_SPACE Header;
    DWORD Bar1;
    DWORD Bar2;
    DWORD Bar3;
    DWORD Bar4;
    DWORD Bar5;
    DWORD CardbusCisPointer;
    WORD SubsystemVendorId;
    WORD SubsystemId;
    DWORD ExpansionRomBaseAddr;
    BYTE CapabilitiesPtr;
    BYTE Reserved[3];
    DWORD Reserved2;
    BYTE InterruptLine;
    BYTE InterruptPin;
    BYTE MinGrant;
    BYTE MaxLatency;
} PCIE_CONFIG_SPACE_TYPE_0;
#pragma pack()

#pragma pack(1)
typedef struct _PCI_CAPABILITY_HEADER{
    BYTE PciExpressCapId;
    BYTE NextCapPointer;
    WORD PciExpressCapRegister;
    DWORD DeviceCapabilities;
    WORD DeviceControl;
    WORD DeviceStatus;
} PCI_CAPABILITY_HEADER;
#pragma pack()

extern QWORD gLowMem;
OS_STATUS
ConfigureDevice(
    PCIE_CONFIG_SPACE *DevConfigSpace
)
{
    PCIE_CONFIG_SPACE_TYPE_0 *device;
    if(DevConfigSpace->HeaderType != 0)
    {
        LOG("Handling for this kind of device headers was not implemented yet\n");
        return OS_STATUS_NOT_IMPLEMENTED;
    }

    device = DevConfigSpace;
    if(device->CapabilitiesPtr & BIT(4) == 0)
    {
        LOG("Device doesn't have any capabilities\n");
        return OS_STATUS_NOT_FOUND;
    }
    PCI_CAPABILITY_HEADER *capability = ((QWORD)device) + (device->CapabilitiesPtr
    );

    LOG("> BAR1 = %X, BAR2 = %X,\n> BAR3 = %X, BAR4 = %X, BAR5 = %X\n", 
        device->Bar1,
        device->Bar2,
        device->Bar3,
        device->Bar4,
        device->Bar5);

    if(device->Header.DeviceId == 0x8168)
    {
        LOG("TRY TO READ MAC ADDR VIA IOPORTS\n");
        if(device->Bar1 & 1 == 0)
        {
            LOG("Error, IO not supportedd on bar1\n");
            goto failedConfig;
        }

        DWORD basePort = device->Bar1 & 0xFFFFFFF8;
        _OutByte(basePort + 0x52, 0x0);
        _OutByte(basePort + 0x37, 0x10);
        while( (_InByte(basePort + 0x37) & 0x10) != 0) {
            _CpuPause();
         }
        LOG("Base IO address = %X\nRead MAC address is:\n", basePort);

        BYTE macAddr = 0;
        for(int i=0;i<6;i++)
        {
            macAddr = _InByte(basePort + i);
            LOG("%X ", macAddr);
        }
        LOG("\n^^^^^^^^^^^^^^^^^\n");
 
        QWORD barSpace = PagePoolAlloc(NULL, PAGE_SIZE_4K);
        barSpace &= 0xFFFFFF00;
        LOG("NEW BAR SPACE = %llX\n", barSpace);
        LOG("Writing BAR\n");
        device->Bar2 = barSpace;
        WORD value = device->Header.Command;
        value &= (~BIT(0));
        value &= (~BIT(1));
        value |= BIT(2);
        device->Header.Command = value;
        LOG("Written BAR OK\n");
        while(_InByte(0x52) & BIT(3) == 0)
        {
            _CpuPause();
        }
        LOG("Bit set\n");

        VirtMemMngrMapVaToPa(
            (PVOID)&gCpuInformation.SHARED_CPU_INFORMATION.KernelCr3,
            0xF7904000,
            0xF7904000,
            PAGE_SIZE_4K,
            STRONGLY_UNCACHABLE,
            FALSE
        );

        for(QWORD adr = DevConfigSpace; adr < ((QWORD)DevConfigSpace+PAGE_SIZE_4K); adr++)
        {
            BYTE *mc = adr;
            if(mc[0] == 0x54 && mc[1] == 0xA0 && mc[2] == 0x50 && mc[3] == 0x3A && mc[4] == 0x0B && mc[5] == 0x6C )
            {
                LOG("FOUND IN PCI MEMORY!!!\n");
                BREAKPOINT;
            }
        }
        for(QWORD adr = 0xF7904000; adr < ((QWORD)0xF7904000+PAGE_SIZE_4K); adr++)
        {
            BYTE *mc = adr;
            if(mc[0] == 0x54 && mc[1] == 0xA0 && mc[2] == 0x50 && mc[3] == 0x3A && mc[4] == 0x0B && mc[5] == 0x6C )
            {
                LOG("FOUND IN BAR MEMORY!!!\n");
                BREAKPOINT;
            }
        }

        BYTE *macAddrMem = barSpace;
        LOG("Memory mapped mac addr = %x, %x, %x, %x, %x, %x\n",macAddrMem[0],macAddrMem[1], macAddrMem[2], macAddrMem[3], macAddrMem[4], macAddrMem[5], macAddrMem[6]);
    }
    
failedConfig:
    while(1 == 1)
    {
    LOG("Capability id = %d, next cap = %x, PCIECR = %x, DEV CAP REG %X,\nDev Control %x, Device status %x\n",
            capability->PciExpressCapId,
            capability->NextCapPointer,
            capability->PciExpressCapRegister,
            capability->DeviceCapabilities,
            capability->DeviceControl,
            capability->DeviceStatus
        );
    if(capability->NextCapPointer == 0)
    {
        break;
    }
    capability = ((QWORD)device) + (capability->NextCapPointer);
    }

}

OS_STATUS
HandleConfigSpace(
    QWORD BaseAddress,
    BYTE BusStart,
    BYTE BusEnd
)
{
    // LOG("Mapping %llX size %llX at cr3 %llX\n", BaseAddress, (BusEnd - BusStart + 1) * PAGE_SIZE_4K, _GetCr3());
    OS_STATUS status;

    status = VirtMemMngrMapVaToPa(
        (PVOID)&gCpuInformation.SHARED_CPU_INFORMATION.KernelCr3,
        BaseAddress,
        BaseAddress,
        (64*32*8 + 2) * PAGE_SIZE_4K,
        STRONGLY_UNCACHABLE,
        TRUE
    );
   if(!SUCCESS(status))
   {
       LOG("ERROR\n");
       BREAKPOINT;
   }
    LOG("Mapped?\n");

    QWORD header = (QWORD)BaseAddress;
    for(QWORD i = 0; i < 63*31*8 - 1; i++, header += PAGE_SIZE_4K)
    {
       PCIE_CONFIG_SPACE *deviceConfigSpace = header;
       if(deviceConfigSpace->VendorId != 0xFFFF && deviceConfigSpace->VendorId != 0)
       {
        
            DEVICE_DESCRIPTOR *newDevice = HeapAlloc(NULL, sizeof(DEVICE_DESCRIPTOR));
            newDevice->Bus = ((i>>8) & 0x3F);
            newDevice->Device = ((i >> 3) & 0x1F);
            newDevice->Function = (i & 0x3);
            newDevice->DeviceClass = deviceConfigSpace->Class;
            newDevice->DeviceSubclass = deviceConfigSpace->Subclass;
            newDevice->DeviceVendor = deviceConfigSpace->VendorId;
            newDevice->DeviceConfigSpaceAddress = deviceConfigSpace;
            newDevice->NextDevice = gCpuInformation.SHARED_CPU_INFORMATION.SystemDevices;
            gCpuInformation.SHARED_CPU_INFORMATION.SystemDevices = newDevice;

            if(deviceConfigSpace->Class == 0x2)
            {
                LOG("---------------------------------------------\nOn BUS %d DEVICE %d FUNCTION %d\nWe have: class = %X, sublclass = %X, progif = %X\n vend Id = %X, device id = %x, header type = %x\n",
                    ((i>>8) & 0x3F),
                    ((i >> 3) & 0x1F),
                    (i & 0x3),
                    deviceConfigSpace->Class,
                    deviceConfigSpace->Subclass,
                    deviceConfigSpace->ProgIf,
                    deviceConfigSpace->VendorId,
                    deviceConfigSpace->DeviceId,
                    deviceConfigSpace->HeaderType);
                LOG("Found network adapter on Bus %d Device %d Function %d, check capabilities...\n",
                    newDevice->Bus,
                    newDevice->Device,
                    newDevice->Function
                );
                ConfigureDevice(deviceConfigSpace);
                LOG("Finished with device\n");
            }

        }
   }
    LOG("Finished?\n");
}


OS_STATUS
InitPcieDevices(

)
{
    ACPI_STATUS acpiStatus = AE_OK;
    ACPI_TABLE_MCFG *mcfg;
    
    gCpuInformation.SHARED_CPU_INFORMATION.SystemDevices = NULL;

    acpiStatus = AcpiGetTable(
        ACPI_SIG_MCFG,
        0,
        (ACPI_TABLE_HEADER **)&mcfg
    );
    if(acpiStatus != AE_OK)
    {
        const char * statusString = AcpiFormatException(acpiStatus);
        LOG("Error AcpiInitializeTables %s\n", statusString);
    }
    
    LOG("OK mcfg length = %d config = \n", ((DWORD)(mcfg->Header.Length)));


    QWORD config = (((QWORD)mcfg) + sizeof(ACPI_TABLE_MCFG));
    for(config; ((QWORD)config) < (((QWORD)(mcfg->Header.Length)) + ((QWORD)mcfg)); config += sizeof(ACPI_PCIE_CONFIG_SPACE_DESCRIPTOR))
    {
        ACPI_PCIE_CONFIG_SPACE_DESCRIPTOR *cfg = (ACPI_PCIE_CONFIG_SPACE_DESCRIPTOR *)config;
        LOG("DESCRIPTOR FOR SEG. %d, START BUS %d, END BUS %d, PCI CONFIG SPACE %llX\n",
            cfg->PciSegmentGroupNumber,
            cfg->StartBusNumber,
            cfg->EndBusNumber,
            cfg->BaseAddress);

        HandleConfigSpace(
            cfg->BaseAddress,
            cfg->StartBusNumber,
            cfg->EndBusNumber
        );

    }
    LOG("END\n");

}