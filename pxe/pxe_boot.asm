[bits 16]
[org 0x7C00]

SCREEN_HEIGHT 	equ 	25
SCREEN_WIDTH 	equ 	80

;;
;;	Resources Used:
;;	http://www.pix.net/software/pxeboot/archive/pxespec.pdf
;;				and
;;	https://wiki.osdev.org/PXE#PXENV.2B
;;

;;
;;	TODO:
;;	
;;	0. Create Git for this
;;	1. Should check PXENV and !PXE structure versions
;;	2. Should add PXENV as a backup method of obtaining PXE API addr. in case !PXE checks fail
;;	3. Should check checksums for both structures
;;	4. Print screen method
;;

KILO    equ     (1024)
MEGA    equ     (1024*1024)
GIGA    equ     (1024*1024*1024)

RELOC_ADDRESS	equ 0x500
LOAD_ADDRESS 	equ 0x5000
%define RELOC(addr) (addr - copyCodeStart + RELOC_ADDRESS)

struc PXENV
	.Signature 			resb 	6
	.Version 			resw	1
	.Length				resb	1
	.Checksum			resb	1
	.RMEntry			resd	1
	.PMOffset			resd	1

	.PMSelector			resw	1
	.StackSeg			resw	1
	.StackSize			resw	1
	.BCCodeSeg			resw	1
	.BCCodeSize			resw	1
	.BCDataSeg			resw	1
	.BCDataSize			resw	1

	.UNDIDataSeg		resw	1
	.UNDIDataSize		resw	1
	.UNDICodeSeg		resw	1
	.UNDICodeSize		resw	1

	.PXEPtr				resd	1
endstruc

struc PXE
	.Signature 			resb 	4
	.StructLength 		resb	1
	.StructCksum		resb 	1
	.StructRev			resb	1
	.Reserved1			resb	1
	
	.UNDIROMID			resd	1
	.BaseROMID			resd	1
	.EntryPointSP		resd	1
	.EntryPointESP		resd	1
	.StatusCallout		resd	1
	
	.Reserved2			resb	1
	.SegDescCnt			resb	1
	.FirstSelector		resw	1

	.Stack				resb	8
	.UNDIData			resb	8
	.UNDICode			resb	8
	.UNDICodeWrite		resb	8
	.BC_Data			resb	8
	.BC_Code			resb	8
	.BC_CodeWrite		resb	8
endstruc

PXENV_PACKET_TYPE_DHCP_DISCOVER 	equ 	1
PXENV_PACKET_TYPE_DHCP_ACK			equ 	2
PXENV_PACKET_TYPE_CACHED_REPLY		equ 	3

struc PXENV_GET_CACHED_INFO
	.Status				resw 	1
	.PacketType			resw 	1
	.BufferSize			resw 	1
	.BufferOff			resw 	1
	.BufferSeg			resw 	1	
	.BufferLimit		resw 	1
endstruc

BOOTP_REQ 	        equ	1
BOOTP_REP 	        equ	2
BOOTP_BCAST         equ	0x8000

struc BOOTPH
	.Opcode				resb 	1
	.Hardware			resb	1
	.Hardlen			resb 	1
	.Gatehops			resb 	1
	.Ident				resd	1
	.Seconds			resw	1
	.Flags				resw	1
	.cip				resb	4
	.yip				resb	4
	.sip				resb	4
	.gip				resb	4
	.CAddr				resb	16
	.Sname				resb 	64
	.Bootfile			resb	128
	.VendorData			resb	64
endstruc

struc PXENV_TFTP_OPEN
    .Status             resw    1
    .ServerIPAddress    resd    1
    .GatewayIPAddress   resd    1
    .FileName           resb    128
    .TFTPPort           resw    1
    .PacketSize         resw    1
endstruc

struc PXENV_TFTP_READ
    .Status             resw  1
    .PacketNumber       resw  1
    .BufferSize         resw  1
    .BufferOff          resw  1
    .BufferSeg          resw  1
endstruc

struc PXENV_TFTP_CLOSE
    .Status             resw  1
endstruc

struc UNDI_SHUTDOWN
    .Status             resw  1
endstruc

FILE_SIZE equ (0x0007FFFF - LOAD_ADDRESS)

_start:
		jmp 0:_start1		; Skip data area
		
		;;	PXENV+ Structure Pointers
		pxenvStructureBase 						dw 	0
		pxenvStructureSegment 					dw 	0

		;;	!PXE Structure
		pxeStructureBasae						dw	0
		pxeStructureSegment						dw	0

		;;	PXE API Calling Address Protected Mode
		pxeApiCallingAddressProtected			dd	0

		;;	Bootph Structure Address
		bootphStructureBase						dw	0
		bootphStructureSegment					dw	0


		cachedInfoRequest:
			istruc PXENV_GET_CACHED_INFO
				at	PXENV_GET_CACHED_INFO.PacketType,	dw 	2
			iend

		tftpOpenRequest:
            istruc  PXENV_TFTP_OPEN
                at  PXENV_TFTP_OPEN.Status,             dw      0
                at  PXENV_TFTP_OPEN.ServerIPAddress,    dd      0
                at  PXENV_TFTP_OPEN.GatewayIPAddress,   dd      0
                at  PXENV_TFTP_OPEN.FileName,           db      'kernel.bin',0
                at  PXENV_TFTP_OPEN.TFTPPort,           dw      0x0045 ;Wink wink
                at  PXENV_TFTP_OPEN.PacketSize,         dw      512
            iend

		error_message 							db " Error"
		error_message_length 					equ $-error_message


		;;	Maybe we should jump to protected 32 bit mode?
		;;	Might simplify the work
		;;	Or just create 1 single space to hold the address?
		;;	^ Fix

_start1:
cli

found:

xor		eax, eax
mov		ds, ax

;;	Get the PXENV Structure
mov		si, bx
;;	At this point ES:SI points to PXENV+ structure

;;	Check the validity of PXENV+ structure by checking the PENV+ signature
push 	ax
mov		ax, 1
cmp		byte [es:si + PXENV.Signature + 0], 'P'
jne		Error
cmp		byte [es:si + PXENV.Signature + 1], 'X'
jne		Error
cmp		byte [es:si + PXENV.Signature + 2], 'E'
jne		Error
cmp		byte [es:si + PXENV.Signature + 3], 'N'
jne		Error
cmp		byte [es:si + PXENV.Signature + 4], 'V'
jne		Error
cmp		byte [es:si + PXENV.Signature + 5], '+'
jne		Error
pop 	ax
;;	Save the PXENV+ selector and base
mov		[pxenvStructureSegment], ax
mov		[pxenvStructureBase], si

;;	Get the address of !PXE structure from the last field of the PENV+ structure
mov		ax, [es:si + PXENV.PXEPtr + 2]
mov		si, [es:si + PXENV.PXEPtr]

mov		es, ax	
;;	At this point ES:SI points to !PXE structure
push 	ax
mov 	ax, 2
cmp		byte [es:si + PXE.Signature + 0], '!'
jne		Error
cmp		byte [es:si + PXE.Signature + 1], 'P'
jne		Error
cmp		byte [es:si + PXE.Signature + 2], 'X'
jne		Error
cmp		byte [es:si + PXE.Signature + 3], 'E'
jne		Error
pop 	ax

;;	Save the !PXE structure selector and base
mov		[pxeStructureSegment], ax
mov		[pxeStructureBasae], si

;;	Get the PXE API calling address
mov		ax, [es:si + PXE.EntryPointSP + 2]
mov		[pxeApiCallingAddressRealSegment],ax
mov		ax, [es:si + PXE.EntryPointSP]
mov		[pxeApiCallingAddressRealBase],ax

mov		eax, [es:si + PXE.EntryPointESP]
mov		[pxeApiCallingAddressProtected], eax

;;	Call the api
xor 	ax, ax
mov		es, ax

mov		di, cachedInfoRequest
mov		bx,	0x0071				;; Ocpode for the  PXENV_GET_CACHED_INFO

call	CallPxeApi

;;	Check for errors
or		ax, [cachedInfoRequest]
test 	ax, ax
jnz		Error

;;	Check if data was written
mov		ax, [cachedInfoRequest + PXENV_GET_CACHED_INFO.BufferSize]
cmp		ax, 0
jz		Error

;;  Move into DS:SI the bootph structure
mov		si, [cachedInfoRequest + PXENV_GET_CACHED_INFO.BufferOff]
mov		[bootphStructureBase], si
mov		ax, [cachedInfoRequest + PXENV_GET_CACHED_INFO.BufferSeg]
mov		[bootphStructureSegment], ax
mov     ds, ax

;;  Move into ES:DI tftpOpenRequest
xor     ax, ax
mov     es, ax
mov     di, tftpOpenRequest

;;  Fill in the tftpOpenRequest Server IP Address
add     si, BOOTPH.sip
add     di, PXENV_TFTP_OPEN.ServerIPAddress
mov     cx, 4
rep     movsb

mov     di, tftpOpenRequest
mov     ax, es
mov     ds, ax
mov     bx, 0x20
call    CallPxeApi

;;	Check for errors
or		ax, [tftpOpenRequest]
test 	ax, ax
jnz		Error

mov     ax, 0xB800
mov     fs, ax
mov     [fs:0], dword 0x30AB

mov		ecx, 0
mov     ax, 0xB800
mov     fs, ax
mov     [fs:2], dword 0xAB30

mov 	si, 0
mov 	ds, si
mov		es, si

mov		di, 0x500
mov		si, copyCodeStart
mov		cx, copyCodeEnd - copyCodeStart

rep 	movsb

mov		esp, 0x4900
jmp		0x500

copyCodeStart:

.RequestReading:

inc		byte [fs:2]
inc		byte [fs:3]
inc		byte [fs:4]
mov     di, RELOC(tftpReadRequest)
mov     bx, 0x22
push	ds
push	di
push	bx

call	far [RELOC(pxeApiCallingAddressRealBase)]
add 	sp, 6

;;	Check for errors
or		ax, [RELOC(tftpReadRequest)]
test 	ax, ax
jnz		Error

add		ecx, 512
cmp		ecx, FILE_SIZE
jae		.FinishReading

mov		ax, [RELOC(tftpReadRequest + PXENV_TFTP_READ.BufferSize)]
cmp		ax, 512
jne		.FinishReading

add		[RELOC(tftpReadRequest + PXENV_TFTP_READ.BufferSeg)], word (512 >> 4)

.tryAgain:
mov		[RELOC(tftpReadRequest + PXENV_TFTP_READ.BufferSize)], word 512
mov		[RELOC(tftpReadRequest + PXENV_TFTP_READ.Status)], word 0
mov		[RELOC(tftpReadRequest + PXENV_TFTP_READ.PacketNumber)], word 0

jmp		.RequestReading

.FinishReading:

mov     di, RELOC(tftpCloseRequest)
mov     bx, 0x21
push	ds
push	di
push	bx

call	far [RELOC(pxeApiCallingAddressRealBase)]

;;	Check for errors
or		ax, [RELOC(undiShutdown)]
test 	ax, ax
jnz		Error

mov     di, RELOC(undiShutdown)
mov     bx, 0x5
push	ds
push	di
push	bx

call	far [RELOC(pxeApiCallingAddressRealBase)]

;;	Check for errors
or		ax, [RELOC(undiShutdown)]
test 	ax, ax
jnz		Error

mov		esp, 0x4900
jmp     0:0x5000
cli
hlt

tftpReadRequest:
    istruc  PXENV_TFTP_READ
        at  PXENV_TFTP_READ.Status,             dw      0
        at  PXENV_TFTP_READ.PacketNumber,       dw      0
        at  PXENV_TFTP_READ.BufferSize,         dw      512
        at  PXENV_TFTP_READ.BufferOff,          dw      0x0
        at  PXENV_TFTP_READ.BufferSeg,          dw      0x500
    iend
db 0x0
tftpCloseRequest:
    istruc PXENV_TFTP_CLOSE
        at PXENV_TFTP_CLOSE.Status,             dw      0
    iend

undiShutdown:
    istruc UNDI_SHUTDOWN
        at UNDI_SHUTDOWN.Status,             	dw      0
    iend

;;	PXE API Calling Address Real Mode
pxeApiCallingAddressRealBase			dw	0
pxeApiCallingAddressRealSegment		    dw	0


copyCodeEnd:



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 	SCREEN STUFF  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;	We're quite ready to call the API, but let's set the video mode just to give the user some feedback
;mov		cx, SCREEN_WIDTH * SCREEN_HEIGHT
;mov		bx,	0xB800
;mov		es, bx
;xor		bx, bx
;
;.clearScreen:
;mov		[es:bx], byte 0
;mov		[es:bx + 1], byte 0x3F
;add		bx, 2
;loop .clearScreen
;
;;;	Set writing positon back to start
;mov 	ah, 0x02
;mov		bx, 0x0058
;int		0x10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 	END SCREEN STUFF  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;   IN:
;   CX      =   Size of dump
;   DS:SI   =   Dump Start
;   DL      =   0 as hex, 1 as ASCII 
MemoryDump:
push    ax
push    bx

mov     ah, 0x0e
mov     bh, 0x00
mov     bl, 0x3F
.dumpStruct:

lodsb
cmp     dl, 1
je      .finishF
;add     al, '0'
push    ax
shr     al, 4
cmp     al, 9
ja      .addLetterS
add     al, '0'
jmp     .finishS
.addLetterS:
add     al, 'A' - 10
.finishS:
int     0x10

pop     ax
and     al, 0x0F
cmp     al, 9
ja      .addLetterF
add     al, '0'
jmp     .finishF
.addLetterF:
add     al, 'A' - 10
.finishF:
int     0x10
loop .dumpStruct

pop     ax
pop     bx
ret




;	IN:
;	DS:DI 	= 	Address of the input/output structure
;	BX		=	Opcode 
;
;	OUT:
;	AX 		=	Status
CallPxeApi:

;;	The calling convention is __cdecl, so:
;;	parameters are on the stack
;;	we have to clear the stack after call
push	ds
push	di
push	bx

call	far [pxeApiCallingAddressRealBase]

add 	sp, 6

;;	Return execution
ret


;  IN:  
;   AX   =  Value to be printed
;   CL   =  Number of digits to print
;   CH   =  Base
;   DL   =  Line
;   DH   =  Column
;  OUT:
;   Screen printed
;PrintValueOnScreen:
;push    bx
;push    es
;
;mov     bx, 0xB800
;mov     es, bx
;
;push    ax
;xor     ax, ax
;mov     al, dl
;mov     bl, SCREEN_WIDTH
;mul     bl
;adc     ah, 0
;add     al, dh
;adc     ah, 0
;shl     ax, 2
;mov     bx, ax
;
;xor     dx, dx
;mov     dl, ch
;
;xor     ch, ch
;
;pop     ax
;.printValueLoop:
;
;div     dl
;
;cmp     ah, 9
;ja      .makeHex
;add     ah, '0'
;jmp     .printDigit
;.makeHex:
;add     ah, 'A' - 10
;.printDigit:
;
;mov     [es:bx], ah
;mov     [es:bx + 1], byte 0x3F
;
;xor     ah, ah
;add     bx, 2
;
;loop .printValueLoop
;
;pop     es
;pop     bx
;ret


Error:

;;	Set writing position at 0
mov 	ah, 0x02
mov		bx, 0x0058
int		0x10

mov		bl, al
and		al, 0xF0
shr		al, 4
cmp 	al, 9
ja		.addLetter1
add		al, '0'
jmp		.afterAddition1
.addLetter1:
add		al, 'A' - 10
.afterAddition1:
mov		ah, 0xe
mov		bx,	0x0058
int     0x10

mov		al, bl
and		al, 0x0F
cmp 	al, 9
ja		.addLetter2
add		al, '0'
jmp		.afterAddition2
.addLetter2:
add		al, 'A' - 10
.afterAddition2:
mov		ah, 0xe
mov		bx,	0x0058
int     0x10

xor		ax, ax
mov		ds, ax
mov 	si, error_message
;;	Write "Error"

mov		cx, error_message_length
mov		ah, 0xe
mov		bx,	0x0058

.print_error_loop:
lodsb
int 0x10
loop	.print_error_loop

cli
hlt
