struc ASM_PER_CPU_INFORMATION
    .CpuId              resb 1
    .StackAddress       resq 1
    .TssAddress         resq 1
    .TssSelector        resw 1
    .ContextSwitchSpace resq 1
    .ApicBase           resq 1
    .LapicFrequency     resq 1
    .InKernel           resb 1
    .SwitchedTask       resb 1
endstruc

struc ASM_SHARED_CPU_INFORMATION
    
    .ExecutionStatus                resb 1
    .Gdt                            resq 1
    
    .DataSegmentDescriptor64Ring0   resw 1
    .CodeSegmentDescriptor64Ring0   resw 1
    .DataSegmentDescriptor64Ring3   resw 1
    .CodeSegmentDescriptor64Ring3   resw 1

    .ThreadpoolGlobaQueue           resq 1
    .ThreadpoolGlobalMutex          resb 1

    .TotalCpuNumber                 resd 1
    .InitiatedCpus                  resd 1

    .KernSpace                      resq 1

    .Idt                            resq 1
    .KernelCr3                      resq 1
    .SystemDevices                  resq 1

    .PerCpuData                     resq 1

endstruc

global _Init16Ap
extern Cr3Base
extern Gdt
extern gCpuInformation
extern main64Ap  



KILO                equ (1024)
MEGA                equ (1024*1024)
GIGA                equ (1024*1024*1024)
TERA                equ (1024*1024*1024*1024)

%define BIT(x)  (1 << x)

CR0.PG              equ BIT(31)
CR0.PE              equ BIT(0)
CR4.PAE             equ BIT(5)
IA32_EFER_MSR.LME   equ BIT(8)
IA32_EFER_MSR       equ 0xC0000080

;; This code has to be page-alligned in order to be sent via SIPI vector
align 4096

[bits 16]
_Init16Ap:
    cli
    lgdt    [Gdt]

    mov     eax, cr0
    or      eax, CR0.PE
    mov     cr0, eax

    mov     ax, 0x20
    mov     ds, ax
    mov     es, ax
    mov     fs, ax
    mov     gs, ax
    mov     ss, ax

    jmp     0x18:_Init32Ap

[bits 32]
_Init32Ap:
    mov     eax, [Cr3Base]
    mov     cr3, eax

    mov     eax, cr4
    or      eax, CR4.PAE
    mov     cr4, eax

    mov     ecx, IA32_EFER_MSR
    rdmsr   
    or      eax, IA32_EFER_MSR.LME
    wrmsr

    mov     eax, cr0
    or      eax, CR0.PG
    mov     cr0, eax

    mov     ax, 0x30
    mov     ds, ax
    mov     es, ax
    mov     fs, ax
    mov     gs, ax
    mov     ss, ax

    jmp     0x28:_Init64Ap

[bits 64]
_Init64Ap:
    mov     rax, cr4
    and     eax, (~(1<<2))
    mov     cr4, rax

    ;; Get processor ID
    ;; Call extern _GetCpuId??
    mov     rbx, 0
    mov     rax, 1
    cpuid
    shr     rbx, 24
    and     rbx, 0xFF
    ;; Find the offset of the cpu's PER_CPU_INFORMATION
    mov     rax, rbx
    mov     cx, ASM_PER_CPU_INFORMATION_size
    mul     cx

    ;; Set the stack to the one allocated by the bsp
    mov     rcx, [gCpuInformation + ASM_SHARED_CPU_INFORMATION.PerCpuData]
    mov     rcx, [rcx + rax + ASM_PER_CPU_INFORMATION.StackAddress]
    mov     rsp, rcx

    ;; Call the AP main function
    mov     rcx, rbx
    call    main64Ap
    cli 
    hlt
