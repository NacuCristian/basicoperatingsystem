@echo off
@echo BUILD STARTED
C:\nasm\nasm.exe -f bin -o %~dp0\bin\pxe_boot.bin %~dp0\pxe\pxe_boot.asm
@echo --- END PXE_BOOT BUILD ---
cd os
py -3 build.py
cd ..
@echo BUILD ENDED