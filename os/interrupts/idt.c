#include "idt.h"
#include "../memory/kern_pagepool.h"
#include "../memory/kern_heap.h"
#include "../devices/lapic.h"
#include "../devices/pic.h"
#include "interrupt_manager.h"
#include "../driver/vga_driver.h"

#define ADDRESS_OF(label) ((QWORD)&label)
#define LEAST_SIGNIFICANT_WORD(value) (value & 0x0ffff)
#define MOST_SIGNIFICANT_WORD(value) ((value >> 16) & 0x0ffff)   
#define LEAST_SIGNIFICANT_DWORD(value) (value & 0x0ffffffff)   
#define MOST_SIGNIFICANT_DWORD(value) ((value >> 32) & 0x0ffffffff)   
extern void _Handle0();
extern void _Handle1();

#define HANDLE_SIZE (((QWORD)&_Handle1) - ((QWORD)&_Handle0))
QWORD handle0Address = (QWORD)&_Handle0;

char * ExceptionNames[] = {
    "DIVIDE ERROR",
    "DEBUG EXCEPTION",
    "NON MASKABLE INTERRUPT",
    "BREAKPOINT",
    "OVERFOW",
    "BOUND RANGE EXCEPTION",
    "INVALID OPCODE #UD",
    "NO MATH COPROCESSOR",
    "DOUBLE FAULT",
    "COPROCESSOR SEGMENT OVERRUN",
    "INVALID TSS",
    "SEGMENT NOT PRESENT",
    "STACK SEGMENT FAULT",
    "GENERAL PROTECTION",
    "PAGE FAULT",
    "",
    "MATH FAULT",
    "ALIGNMENT CHECK",
    "MACHINE CHECK",
    "SIMD FPU EXCEPTION",
    "VIRTUALIZATION EXCEPTION"
};

typedef enum _EXCEPTION_VECTOR
{
    VECTOR_NR_DE = 0,
    VECTOR_NR_DB = 1,
    VECTOR_NR_NMI = 2,
    VECTOR_NR_BP = 3,
    VECTOR_NR_OF = 4,
    VECTOR_NR_UD = 6,
    VECTOR_NR_NM = 7,
    VECTOR_NR_DF = 8,
    VECTOR_NR_TSS = 10,
    VECTOR_NR_NP = 11,
    VECTOR_NR_SS = 12,
    VECTOR_NR_GP = 13,
    VECTOR_NR_PF = 14,
    VECTOR_NR_MF = 16,
    VECTOR_NR_AC = 17,
    VECTOR_NR_MC = 18,
    VECTOR_NR_XM = 19,
    VECTOR_NR_VE = 20,
    RESERVED_START = 21,
    RESERVED_END = 31,
    USER_DEFINED_START = 32,
    USER_DEFINED_END = 255
} EXCEPTION_VECTOR;

#pragma pack(1)
typedef struct _IDT_ATTRIBUTES{
    union 
    {
        struct 
        {
            BYTE Type : 4;
            BYTE Zero : 1;
            BYTE Dpl : 2;
            BYTE P : 1;
        } Explicit;

        BYTE Raw;
    };
} IDT_ATTRIBUTES;
#pragma pack()

#pragma pack(1)
typedef struct _IDT_DESCRIPTOR
{
    WORD Offset1;
    WORD Selector;
    BYTE Ist;
    IDT_ATTRIBUTES Attributes;
    WORD Offset2;
    DWORD Offset3;
    DWORD Zero;
} IDT_DESCRIPTOR;
#pragma pack()

#pragma pack(1)
typedef struct _IDT
{
    IDT_DESCRIPTOR Table[256];
} IDT;
#pragma pack()

#pragma pack(1)
typedef struct _IDT_ADDRESS_STRUCTURE
{
    WORD Limit;
    QWORD IdtAddress;
} IDT_ADDRESS_STRUCTURE;
#pragma pack()

OS_STATUS
InitIdt()
{
    IDT * idt;
    PVOID nimic = 0;
    sizeof(IDT_DESCRIPTOR);
    idt = PagePoolAlloc(
        NULL,
        PAGE_SIZE_4K
    );
    if(idt == NULL)
    {
        LOG("Error PagePoolAlloc failed\n");
    }
    nimic = PagePoolAlloc(
        NULL,
        PAGE_SIZE_4K
    );
    if(nimic == NULL)
    {
        LOG("Error PagePoolAlloc failed\n");
    }


    for (DWORD vector = 0; vector <= 256; vector++)
    {
        QWORD isrStartAddr = 0;
        idt->Table[vector].Selector = 0x28; // Trust me 
        idt->Table[vector].Ist = 0; // Don't use IST mechanism right now (https://wiki.osdev.org/Task_State_Segment)
        idt->Table[vector].Zero = 0;
        idt->Table[vector].Attributes.Explicit.Type = 14;
        idt->Table[vector].Attributes.Explicit.Zero = 0;
        idt->Table[vector].Attributes.Explicit.P = 1; // Present
        idt->Table[vector].Attributes.Explicit.Dpl = vector == 33 ? 3 : 0; // Ring 0 ISR

        isrStartAddr = (handle0Address + (vector * HANDLE_SIZE)); // Compute the offset of the ISR handle
        idt->Table[vector].Offset1 = LEAST_SIGNIFICANT_WORD(isrStartAddr); 
        idt->Table[vector].Offset2 = MOST_SIGNIFICANT_WORD(isrStartAddr); 
        idt->Table[vector].Offset3 = MOST_SIGNIFICANT_DWORD(isrStartAddr);  
    }

    IDT_ADDRESS_STRUCTURE *idtAddr = HeapAlloc(
        NULL, 
        sizeof(IDT_ADDRESS_STRUCTURE)
    );

    idtAddr->Limit = sizeof(IDT);
    idtAddr->IdtAddress = (QWORD)idt;    

    _Lidt(idtAddr);
    gCpuInformation.SHARED_CPU_INFORMATION.Idt = idtAddr;
    LOG("Idt allocated at address %llX containing 255 interrupt descriptors\n", idt);

    return OS_STATUS_SUCCESS;
}

void
GeneralExceptionHandler(
    COMMON_REGISTER_STRUCTURE *Registers
)
{
    DWORD cpuid = (DWORD)_GetCpuId();
    BOOL requiresEOI = TRUE;
    BOOL requiresPicEOI = FALSE;
    BYTE irqNumber = 0;    
    
    gCpuInformation.PerCpuData[_GetCpuId()].InKernel = TRUE;

    if(Registers->ExceptionVector < 21)
    {
        EXCEPTION_REGISTER_STUCTURE *exceptionRegs;
        exceptionRegs = Registers;

        LOG("Exception %s (vector %X) occured on CPU %d\n\
|Rsp = %18llX | Cr2 = %18llX | Cr3 = %18llX|\
|Rax = %18llX | Rbx = %18llX | Rcx = %18llX|\
|Rdx = %18llX | Rsi = %18llX | Rdi = %18llX|\
|SS  = %18llX | CS  = %18llX | Rip = %18llX|\
|ES  = %18llX | FL  = %18llX | ER = %18llX|\
|FS  = %18llX | GS  = %18llX | DS = %18llX|\
",
        ExceptionNames[exceptionRegs->COMMON_REGS.ExceptionVector],
        exceptionRegs->COMMON_REGS.ExceptionVector, _GetCpuId(), exceptionRegs->Rsp,
        exceptionRegs->COMMON_REGS.Cr2, exceptionRegs->COMMON_REGS.Cr3,
        exceptionRegs->COMMON_REGS.Rax, exceptionRegs->COMMON_REGS.Rbx,
        exceptionRegs->COMMON_REGS.Rcx, exceptionRegs->COMMON_REGS.Rdx,
        exceptionRegs->COMMON_REGS.Rsi, exceptionRegs->COMMON_REGS.Rdi,
        exceptionRegs->Ss, exceptionRegs->Cs, exceptionRegs->Rip,
        exceptionRegs->COMMON_REGS.Es, exceptionRegs->Rflags, exceptionRegs->ErrCode,
        exceptionRegs->COMMON_REGS.Fs, exceptionRegs->COMMON_REGS.Gs, exceptionRegs->COMMON_REGS.Ds
        );
        
    }
    else if(Registers->ExceptionVector >= USER_DEFINED_START)
    {
        INTERRUPT_REGISTER_STUCTURE *interruptRegs;
        INTERRUPT_SERVICE_ROUTINE_DATA *isrd;
        BYTE irqNumber = INVALID_IRQ;
        OS_STATUS status;

        interruptRegs = Registers;

        isrd = HeapAlloc(NULL, sizeof(INTERRUPT_SERVICE_ROUTINE_DATA));
        isrd->InterruptVector = Registers->ExceptionVector;
        isrd->Context = Registers;

        HandleUserInterrupt(
            isrd
        );
        HeapFree(isrd);
        if(GetIrqFromInterruptVector(Registers->ExceptionVector, &irqNumber) == OS_STATUS_SUCCESS)
        {
            requiresPicEOI = TRUE;
        }
    }

    // Signal the LAPIC that the ISR has finished
    if(requiresEOI && Registers->ExceptionVector != 0x46)
    {
        SendEoiForLapic();
    }

    // If handled an IRQ signal the PIC that the ISR has finished
    if(requiresPicEOI)
    {
        SendEoiForPic(irqNumber);
    }
    return;
}