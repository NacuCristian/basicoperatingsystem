#ifndef _SYSCALL_DEFS_
#define _SYSCALL_DEFS_

#include "../utils/os_defs.h"

typedef enum _SYSCALL_CODES{
    READ_REQ = 0,
    WRITE_REQ,
    FORK_REQ,
    GETPID_REQ,
    EXIT_REQ,
    MAX_REQ
} SYSCALL_CODES;

typedef struct _KERNEL_BUFFER_COMMAND{

    BYTE * Buffer;
    DWORD Count;

} KERNEL_BUFFER_COMMAND;


#endif