#ifndef _MEMORY_MANAGEMENT_DEFS_H_
#define _MEMORY_MANAGEMENT_DEFS_H_

#include "../utils/os_defs.h"

#pragma pack(1)
typedef struct _E820_MAP_ENTRY
{
    QWORD BaseAddress;
    QWORD Length;
    DWORD Type;
    DWORD ExtendedAttributes;
} E820_MAP_ENTRY;

typedef struct _E820_MAP_STRUCTURE
{
    DWORD MapSize;
    E820_MAP_ENTRY Map[1]; //We don't know exactly how many entries are in the map so
                           //we'll just put 1, but this allows for the reading of the whole map
} E820_MAP_STRUCTURE;
#pragma pack()
#endif
