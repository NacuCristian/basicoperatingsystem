#ifndef _DATASTRUCTURES_DEFS_H_
#define _DATASTRUCTURES_DEFS_H_

#include "../utils/os_defs.h"

// This method has to return:
// COMPARE_FUNCTION_STATUS_EQUAL if the elements are equal, 
// < COMPARE_FUNCTION_STATUS_LESS if ELEM1 < ELEM2 or
// > COMPARE_FUNCTION_STATUS_GREATER if ELEM1 > ELEM2
// The last 2 are not currently uesd but for future development might
// be good to implement it this way
typedef enum 
{
    COMPARE_FUNCTION_STATUS_EQUAL = 0,
    COMPARE_FUNCTION_STATUS_LESS = -1,
    COMPARE_FUNCTION_STATUS_GREATER = 1
} COMPARE_FUNCTION_STATUS;

typedef COMPARE_FUNCTION_STATUS (*CompareElements)(PVOID Elem1, PVOID Elem2);

typedef struct GENERIC_SINGLE_LINKED_NODE
{
    struct GENERIC_SINGLE_LINKED_NODE *Next;
    PVOID Data;
} GENERIC_SINGLE_LINKED_NODE;

#endif