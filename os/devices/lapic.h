#ifndef _LAPIC_MANAGEMENT_H_
#define _LAPIC_MANAGEMENT_H_

#include "../utils/os_common.h"

typedef enum _DELIVERY_MODE
{
    FIXED = 0,
    LOW_PRIORITY = 1,
    SMI = 2,
    NMI = 4,
    INIT = 5,
    START_UP = 6
} DELIVERY_MODE;

typedef enum _DESTINATION_SHORTHAND
{
    NO_SHORTHAND = 0,
    SELF = 1,
    ALL_INCLUDING_SELF = 2,
    ALL_EXCLUDING_SELF = 3
} DESTINATION_SHORTHAND;

typedef enum _DESTINATION_MODE
{
    PHYSICAL = 0,
    LOGICAL = 1,
} DESTINATION_MODE;

typedef enum _TIMER_MODE
{
    ONE_SHOT = 0,
    PERIODIC = 1,
    TSC_DEADLINE = 2
} TIMER_MODE;

typedef enum _TIMER_MASK_OPTIONS
{
    NOT_MASKED = 0,
    MASKED = 1
} TIMER_MASK_OPTIONS;

typedef enum _DIVIDE_VALUE_OPTIONS
{
    DIVIDE_2 = 0b00000,
    DIVIDE_4 = 0b01000,
    DIVIDE_8 = 0b00010,
    DIVIDE_16 = 0b01010,
    DIVIDE_32 = 0b00001,
    DIVIDE_64 = 0b01001,
    DIVIDE_128 = 0b01010,
    DIVIDE_1 = 0b01011,

} DIVIDE_VALUE_OPTIONS;

OS_STATUS
InitApicSystem(
    PVOID RemapAddress
);

OS_STATUS
SendIpi(
    BYTE Vector,
    DELIVERY_MODE IpiType,
    DESTINATION_MODE DestinationMode,
    DESTINATION_SHORTHAND DestinationShorthand,
    BYTE DestinationId,
    BOOL Synchronize
);

OS_STATUS
SetApicTimer(
    DWORD Time,
    TIMER_MODE TimerMode,
    BYTE Vector,
    DIVIDE_VALUE_OPTIONS DivideValue,
    BOOL Masked,
    BOOL Synchronize
);

OS_STATUS
StopApicTimer(
);

DWORD
ReadApicTimer(
);

OS_STATUS
SendEoiForLapic(
);

DWORD
ReadApicError(
);

#endif