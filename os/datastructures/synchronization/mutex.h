#ifndef _OS_MUTEX_
#define _OS_MUTEX_

#include "../../utils/os_defs.h"

typedef struct _MUTEX
{
    volatile BYTE Lock;
} MUTEX;

extern void InitSpinMutex(MUTEX *Mutex);
extern void AcquireSpinMutex(MUTEX *Mutex);
extern void ReleaseSpinMutex(MUTEX *Mutex);

#endif
