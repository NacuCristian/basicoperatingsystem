import os 
import subprocess
from os import listdir
from os.path import isfile, join, isdir

asmCompileCommand = 'C:\\nasm\\nasm.exe -f elf64'
cCompileCommand = 'gcc -c -Os -fcompare-debug-second -nostdlib -nostartfiles -nodefaultlibs -fno-builtin'
asmBuildDir = '..\\bin\\compiled\\'
cBuildDir = '..\\bin\\compiled\\'

#os_init has to be the first linked file
specialOrderFiles = ["os_init.asm", "initAp.asm", "idt.asm", "intrinsics.asm", "mutex.asm", "syscall.asm",  "um_dll.asm"]
specialOrderFileBuildDir =  '..\\bin\\'

linkCommand = 'ld -Ttext 0x5000 ..\\bin\\os_init.o  ..\\bin\\initAp.o ..\\bin\\idt.o \
               ..\\bin\\intrinsics.o ..\\bin\\mutex.o ..\\bin\\syscall.o \
               ..\\bin\\compiled\\*.o -o ..\\bin\\kernel.out'
makeBinaryCommand = 'objcopy -S -O binary ..\\bin\\kernel.out ..\\bin\\kernel.bin'

cleanCommand = '@del /Q /S ..\\bin\\*.o 1>nul'
cleanCommand2 = '@del /Q /S ..\\bin\\*.out 1>nul'

def loopFiles(crtpath): 
    files = [f for f in listdir(crtpath) if isfile(join(crtpath, f))]
    dirs = [f for f in listdir(crtpath) if isdir(join(crtpath, f))]
    for file in files:
        if ".c" in file:
            pre, ext = os.path.splitext(file)
            ext = ".o"
            if file not in specialOrderFiles:
                os.system(cCompileCommand + " " + join(crtpath, file) + " -o " + join(cBuildDir, (pre + ext)))
            else:
                os.system(cCompileCommand + " " + join(crtpath, file) + " -o " + join(specialOrderFileBuildDir, (pre+ext)))
        elif ".asm" in file:
            pre, ext = os.path.splitext(file)
            ext = ".o"
            if file not in specialOrderFiles:
                os.system(asmCompileCommand + " " + join(crtpath, file) + " -o " + join(asmBuildDir, (pre+ext)))
            else:
                os.system(asmCompileCommand + " " + join(crtpath, file) + " -o " + join(specialOrderFileBuildDir, (pre+ext)))
    for dir in dirs:
        loopFiles(join(crtpath, dir))

loopFiles(crtpath = os.path.dirname(os.path.abspath(__file__)))
os.system(linkCommand)
os.system(makeBinaryCommand)
os.system(cleanCommand)
os.system(cleanCommand2)