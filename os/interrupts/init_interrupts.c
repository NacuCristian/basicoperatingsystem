#include "init_interrupts.h"
#include "../time/time_tools.h"
#include "../devices/keyboard.h"
#include "../devices/pic.h"
OS_STATUS
KeyboardDriver(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
);

OS_STATUS
InitInterruptSystem()
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    QWORD regNr;

    status = InitIdt();
    if(!SUCCESS(status))
    {
        LOG("Error initializing IDT\n");
        return status;
    }
    
    status = InitPic(0x28, 0x30);
    if(!SUCCESS(status))
    {
        LOG("Error while remapping PIC\n");
        return status;
    }
    LOG(" \nRemapped Programmable Interrupt Controller (PIC)\n IRQs to interrupt vectors 0x28 - 0x30\nAs a result:\n PIT timer interrupts have vector 0x28\n Keyboard interrupts have vector 0x29\n");
    // Logically this should have been initialize during device initialization
    // But, because of the need of keyboard in debugging this is done as early as it can be donw
    status = RegisterHandleForIrq(
        1,
        KeyboardDriver,
        &regNr
    );
    if(!SUCCESS(status))
    {
        LOG("Error registering initial keyboard driver\n");
        return status;
    }
    
    LOG(" \nRegistered keyboard driver, should have keyboard callbacks.\n");
    
    status = InitApicSystem(NULL);
    if(!SUCCESS(status))
    {
        LOG("Error configuring APIC system\n");
        return status;
    }
    LOG("Acpi initialized in X2 apic mode\n");

    status = InitTimer(0x46);
    if(!SUCCESS(status))
    {
        LOG("Error configuring APIC system\n");
        return status;
    }
    _Sti();

    return status;
}