#ifndef _OS_INTRINSICS_
#define _OS_INTRINSICS_

#include "os_defs.h"

extern  PVOID   _GetRip();

extern  void    _OutByte(BYTE Data, WORD Port);
extern  void    _OutWord(WORD Data, WORD Port);
extern  BYTE    _InByte(WORD Port);
extern  WORD    _InWord(WORD Port);

extern  void    _InvalidateTlb();

extern  void    _Break();

extern  QWORD   _ReadMsr(QWORD MsrAddress);
extern  void    _WriteMsr(QWORD MsrAddress, QWORD Value);

extern  void    _Lidt(PVOID IdtAddress);
extern  void    _Lgdt(PVOID GdtAddress);
extern  void    _Ltr(WORD SegmentOffset);

extern  QWORD   _GetCr0();
extern  QWORD   _GetCs();
extern  QWORD   _GetSs();
extern  QWORD   _GetCr3();
extern  void    _SetCr3(QWORD Cr3);
extern  QWORD   _GetCpuId();
extern  QWORD   _GetRsp();
extern  void    _SetRsp(QWORD Rsp);

extern void     _SetCodeSelector(WORD Selector);

extern void     _Cli();
extern void     _Sti();
extern void     _CpuPause();


extern void     _SyscallInt();
extern QWORD    _Rdtsc();

extern void     _IoWait();
extern QWORD    _GetRflags();
#endif