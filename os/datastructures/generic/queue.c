#include "queue.h"

#define QUEUE_TYPE                   BYTE
#define QUEUE_SYNCHRONIZED          ((BYTE) 1)
#define QUEUE_NOT_SYNCHRONIZED      ((BYTE) 2)

// Todo sterge PVOID * de la argumentul Element??
// TODO fa todo cu "schimba coada sll in circulara dll" 

OS_STATUS
QueueInit(
    QUEUE_TYPE QueueType,
    QUEUE_HEADER **Queue
)
{
    return SllInit(
        QueueType,
        NULL,
        Queue
    );
}

OS_STATUS
QueueEnqueue(
    QUEUE_HEADER *Queue,
    PVOID *Element
)
{
    return SllInsertElementEnd(
        Queue,
        Element
    );
}

PVOID
QueueDequeue(
    QUEUE_HEADER *Queue
)
{
    return SllDeleteFirst(
        Queue
    );
}

BOOL
QueueIsEmpty(
    QUEUE_HEADER *Queue
)
{
    return SllIsEmpty(
        Queue
    );
}