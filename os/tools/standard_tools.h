#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "../utils/os_defs.h"
#include "../utils/os_status.h"

OS_STATUS
KernMemcpy(
    PVOID * Source,
    PVOID * Destination,
    QWORD Length
);

OS_STATUS
KernFill(
    BYTE Value,
    PVOID * Destination,
    QWORD Length
);

// Returns TRUE if buffers are equal
BOOL
KernMemCompare(
    BYTE *Buffer1,
    BYTE *Buffer2,
    QWORD Size
);

#endif