#include "keyboard.h"
#include "../memory/kern_heap.h"

BYTE KeyboardBuffer[65];
BYTE idx = 0;
BYTE lastChar = 0;

BYTE
DecodeKey(
    BYTE Code
)
{   
    BYTE uppercase = abs(('a'-'A'));
    BYTE c = Code & 0x7F;

    if(c > 0x1 && c < 0xC)
    {
        return '0' + abs(((int)c - 0xB));
    }
    switch (c)
    {
        case 0x1:
            return ESCAPE;
        case 0xE:
            return BACKSPACE;
        case 0x10:
            return 'q'-uppercase;
        case 0x11:
            return 'w'-uppercase;
        case 0x12:
            return 'e'-uppercase;
        case 0x13:
            return 'r'-uppercase;
        case 0x14:
            return 't'-uppercase;
        case 0x15:
            return 'y'-uppercase;
        case 0x16:
            return 'u'-uppercase;
        case 0x17:
            return 'i'-uppercase;
        case 0x18:
            return 'o'-uppercase;
        case 0x19:
            return 'p'-uppercase;
        case 0x1C:
            return ENTER;
        case 0x1E:
            return 'a'-uppercase;
        case 0x1F:
            return 's'-uppercase;
        case 0x20:
            return 'd'-uppercase;
        case 0x21:
            return 'f'-uppercase;
        case 0x22:
            return 'g'-uppercase;
        case 0x23:
            return 'h'-uppercase;
        case 0x24:
            return 'j'-uppercase;
        case 0x25:
            return 'k'-uppercase;
        case 0x26:
            return 'l'-uppercase;
        case 0x2C:
            return 'z'-uppercase;
        case 0x2D:
            return 'x'-uppercase; 
        case 0x2E:
            return 'c'-uppercase; 
        case 0x2F:
            return 'v'-uppercase; 
        case 0x30:
            return 'b'-uppercase; 
        case 0x31:
            return 'n'-uppercase; 
        case 0x32:
            return 'm'-uppercase; 
       
        case 0x48:
            return UP; 
        case 0x50:
            return DOWN; 
    }

    return UNKNOWN;

}

MUTEX gKeyboardMutex = {0};

// Should make a structure out of these, to accept multiple requests
BYTE *WriteBuffer = NULL;
DWORD BufferSize = 0;
BYTE *Signal = NULL;

OS_STATUS
KeyboardDriver(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
)
{
    BYTE command = _InByte(0x60);

    BYTE c = DecodeKey(command);
    if (c == lastChar)
    {
        return OS_STATUS_SUCCESS;
    }

    lastChar = c;

    if(c == BACKSPACE)
    {
        KeyboardDeleteCharacter(BLACK, BLACK);
        return OS_STATUS_SUCCESS;
    }
    if(c == ESCAPE)
    {
        _OutByte(0xFE, 0x64);
        return OS_STATUS_SUCCESS;
    }
    if(c == ENTER)
    {
        KeyboardBuffer[64] = 0;
        if(WriteBuffer != NULL)
        {
            for(int i=0; i<BufferSize && i < idx; i++)
            {
                WriteBuffer[i] = KeyboardBuffer[i];
            }
           *Signal = 0xFF;
            Signal = NULL; // It is the register's job to free the signal byte
            WriteBuffer = NULL;
            BufferSize = 0;
            ReleaseSpinMutex(&gKeyboardMutex); // Accept new requests
        }
        
        else if(Signal != NULL)
        {
            *Signal = 0xFF;
            Signal = NULL; // It is the register's job to free the signal byte
            ReleaseSpinMutex(&gKeyboardMutex);
        }

        LOG("Keyboard buffer = %s\n", KeyboardBuffer);


        for(int i=0;i<64;i++)
            KeyboardBuffer[i] = 0;
        idx = 0;
        KeyboardReset(BLACK, BLACK);
        return OS_STATUS_SUCCESS;
    }
    if(c == UP)
    {
        VgaMoveWindow(1);
        return OS_STATUS_SUCCESS;
    }
    if(c == DOWN)
    {
        VgaMoveWindow(2);
        return OS_STATUS_SUCCESS;
    }
    if(c ==  LEFT || c == RIGHT || c == UNKNOWN)
    {
        return OS_STATUS_SUCCESS;
    }
    idx &= 0x3F;
    KeyboardBuffer[idx++] = c;
    KeyboardWriteCharacter(c, BLACK, LIGHT_GRAY);
    return OS_STATUS_SUCCESS;
}

OS_STATUS
KeyboardRegisterBuffer(
    BYTE *Buffer,
    DWORD Size,
    BYTE **FinishSignal
)
{
    OS_STATUS status = OS_STATUS_SUCCESS;
    _Cli();
    AcquireSpinMutex(&gKeyboardMutex);
    WriteBuffer = Buffer;
    BufferSize = Size;
    Signal = HeapAlloc(NULL, sizeof(BYTE));
    if(Signal == NULL)
    {
        status = OS_STATUS_NULL_POINTER;
    }
    *Signal = 0;
    *FinishSignal = Signal;
    _Sti();
    if(!SUCCESS(status))
    {
        ReleaseSpinMutex(&gKeyboardMutex);
    }
    return status;
}