#ifndef _PIC_MANAGEMENT_H_
#define _PIC_MANAGEMENT_H_

#include "../utils/os_common.h"
#define INVALID_IRQ 0xFF

OS_STATUS
InitPic(
    DWORD OffsetInt_0_7,
    DWORD OffsetInt_8_15
);

OS_STATUS
SendEoiForPic(
    BYTE Irq
);

OS_STATUS
GetIrqFromInterruptVector(
    BYTE InterruptVector,
    BYTE *Irq
);

OS_STATUS
GetInterruptVectorFromIrq(
    BYTE Irq,
    BYTE *InterruptVector
);

#endif