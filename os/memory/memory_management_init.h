#ifndef _MEMORY_MANAGEMENT_INIT_
#define _MEMORY_MANAGEMENT_INIT_
#include "memory_management_def.h"

#include "../utils/os_common.h"

OS_STATUS
InitMemoryManagement(
     E820_MAP_STRUCTURE *E820Map
);

#endif