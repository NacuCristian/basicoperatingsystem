#include "../utils/os_common.h"
#include "../devices/vga.h"
#include "../datastructures/synchronization/mutex.h"
#include "../user_mode/syscall.h"
#include "../time/time_tools.h"
#include "../memory/kern_pagepool.h"
#include "../memory/virtual_memory_manager.h"

MUTEX Mtx = {0};

OS_STATUS Td (PVOID *Data)
{
    LOG("OKEEEE\n");
    BREAKPOINT;
    return OS_STATUS_SUCCESS;
}

extern OS_STATUS
TimerDriver(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
);


MUTEX mtx = {0};
OS_STATUS
main64Ap(
    QWORD Id
)
{    
    OS_STATUS ststus;
    _Lidt (gCpuInformation.SHARED_CPU_INFORMATION.Idt);
    _Lgdt (gCpuInformation.SHARED_CPU_INFORMATION.Gdt);
    _Ltr (gCpuInformation.PerCpuData[_GetCpuId()].TssSelector);
    InitSyscall(
        gCpuInformation.PerCpuData[_GetCpuId()].StackAddress
    );
    InitApicSystem(NULL);
    InitTimer(0x46);
    
    AcquireSpinMutex(&mtx);
    LOG("AP %d finished initialization, waiting for signal from BSP to start processing\n", _GetCpuId());
    gCpuInformation.SHARED_CPU_INFORMATION.InitializedCpus++;
    ReleaseSpinMutex(&mtx);

    while(gCpuInformation.SHARED_CPU_INFORMATION.ExecutionStatus != USERMODE_STARTED)
    {
        _CpuPause();
    }
    TimeLapic( ONE_SECOND_IN_MILI_SECONDS, ONE_SHOT);
    BREAKPOINT;
    _Break();
    return OS_STATUS_SUCCESS;
}