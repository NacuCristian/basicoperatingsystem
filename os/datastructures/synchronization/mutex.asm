[bits 64]

global InitSpinMutex
global AcquireSpinMutex
global ReleaseSpinMutex

InitSpinMutex:
    mov [rcx], byte 0
    ret

AcquireSpinMutex:
    push rbx
    .tryLock:
    mov al, 0
    mov bl, 1
    lock cmpxchg [rcx], bl
    jz .locked
    pause
    jmp .tryLock
    .locked:
    pop rbx
    ret

ReleaseSpinMutex:
    mov [rcx], byte 0
    ret