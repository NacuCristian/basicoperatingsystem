#ifndef _HEYBOARD_H_
#define _KEYBOARD_H_

#include "../utils/os_common.h"
#include "../interrupts/interrupt_manager.h"
#include "vga_driver.h"

#define ESCAPE 1
#define BACKSPACE 2
#define ENTER 3
#define LEFT 4
#define RIGHT 5
#define UP 6
#define DOWN 7
#define UNKNOWN 0xFF

OS_STATUS
KeyboardDriver(
    INTERRUPT_SERVICE_ROUTINE_DATA *Data
);

OS_STATUS
KeyboardRegisterBuffer(
    BYTE *Buffer,
    DWORD Size,
    BYTE **FinishSignal
);

#endif